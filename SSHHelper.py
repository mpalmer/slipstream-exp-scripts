import threading
import sys
from queue import Queue
from paramiko import SSHClient
from paramiko import SSHException


def execute(connection: SSHClient, command: str, log=sys.stderr):
    try:
        stdin, stdout, stderr = connection.exec_command(command)
    except SSHException as e:
        print('Failed to execute command "' + command + '": ' + str(e), file=log)
        return -1
    return stdout.channel.recv_exit_status()


class ThreadExecutor(threading.Thread):
    def __init__(self, connection: SSHClient, command: str, pipe: Queue, log=sys.stderr):
        threading.Thread.__init__(self)
        self.connection = connection
        self.command = command
        self.pipe = pipe
        self.log = log

    def run(self):
        try:
            stdin, stdout, stderr = self.connection.exec_command(self.command)
        except SSHException as e:
            print('Failed to execute command "' + self.command + '": ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        try:
            pid = int(stdout.readline())
        except ValueError as e:
            print('Got something from server that was not the PID: ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        self.pipe.put(pid)
        exit_code = stdout.channel.recv_exit_status()
        self.pipe.put(exit_code)
