import threading
from queue import Queue
from queue import Empty
from paramiko import SSHClient
from paramiko import AutoAddPolicy
from paramiko import BadHostKeyException
from paramiko import AuthenticationException
from paramiko import SSHException
import socket
import sys
import SSHHelper as ssh
import time


class TCPDumpThread(threading.Thread):
    def __init__(self, connection: SSHClient, interface: str, binary_dir: str, working_dir: str, pipe: Queue,
                 log=sys.stderr):
        threading.Thread.__init__(self)
        self.connection = connection
        self.interface = interface
        self.binary_dir = binary_dir.rstrip('/')
        self.working_dir = working_dir.rstrip('/')
        self.pipe = pipe
        self.log = log

    def run(self):
        command = self.binary_dir + '/run-tcpdump.sh ' + self.interface + ' ' + self.working_dir
        print(command, file=self.log)
        try:
            stdin, stdout, stderr = self.connection.exec_command(command)
        except SSHException as e:
            print('Error: Failed to execute command "' + command + '": ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        try:
            pid_bash = int(stdout.readline())
            pid_tcpdump = int(stdout.readline())
        except ValueError as e:
            print('Error: Got something from server that was not the PID: ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        self.pipe.put(pid_bash)
        self.pipe.put(pid_tcpdump)
        exit_code = stdout.channel.recv_exit_status()
        self.pipe.put(exit_code)


class MonitorTcThread(threading.Thread):
    def __init__(self, connection: SSHClient, srv_if: str, cli_if: str, binary_dir: str, working_dir: str, pipe: Queue,
                 log=sys.stderr):
        threading.Thread.__init__(self)
        self.connection = connection
        self.srv_if = srv_if
        self.cli_if = cli_if
        self.binary_dir = binary_dir.rstrip('/')
        self.working_dir = working_dir.rstrip('/')
        self.pipe = pipe
        self.log = log

    def run(self):
        command = self.binary_dir + '/run-monitor-tc.sh ' + self.srv_if + ' ' + self.cli_if + ' ' + self.binary_dir \
                  + ' ' + self.working_dir
        print(command, file=self.log)
        try:
            stdin, stdout, stderr = self.connection.exec_command(command)
        except SSHException as e:
            print('Error: Failed to execute command "' + command + '": ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        try:
            pid_bash = int(stdout.readline())
            pid_monitor_tc = int(stdout.readline())
        except ValueError as e:
            print('Error: Got something from server that was not the PID: ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        self.pipe.put(pid_bash)
        self.pipe.put(pid_monitor_tc)
        exit_code = stdout.channel.recv_exit_status()
        self.pipe.put(exit_code)


class BwSwitchThread(threading.Thread):
    def __init__(self, connection: SSHClient, srv_if: str, cli_if: str, binary_dir: str, working_dir: str, trace: str,
                 offset: float, shift: int, pipe: Queue, log=sys.stderr):
        threading.Thread.__init__(self)
        self.connection = connection
        self.srv_if = srv_if
        self.cli_if = cli_if
        self.binary_dir = binary_dir.rstrip('/')
        self.working_dir = working_dir.rstrip('/')
        self.trace = trace
        self.offset = offset
        self.shift = shift
        self.pipe = pipe
        self.log = log

    def run(self):
        command = self.binary_dir + '/run-bw-switch.sh ' + self.binary_dir + '/traces/' + self.trace + ' ' \
                  + str(self.offset) + ' ' + self.srv_if + ' ' + self.cli_if + ' ' + self.working_dir + ' ' \
                  + str(self.shift)
        print(command, file=self.log)
        try:
            stdin, stdout, stderr = self.connection.exec_command(command)
        except SSHException as e:
            print('Error: Failed to execute command "' + command + '": ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        try:
            pid_bash = int(stdout.readline())
        except ValueError as e:
            print('Error: Got something from server that was not the PID: ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        self.pipe.put(pid_bash)
        exit_code = stdout.channel.recv_exit_status()
        self.pipe.put(exit_code)


class BridgeThread(threading.Thread):
    def __init__(self, host: str, srv_if: str, cli_if: str, trace: str, offset: float, shift: int, pipe: Queue,
                 log=sys.stderr, bw_switch_flag=True):
        threading.Thread.__init__(self)
        self.pipe_timeout = 30
        self.kill_wait = 10
        self.host = host
        self.srv_if = srv_if
        self.cli_if = cli_if
        self.trace = trace
        self.offset = offset
        self.shift = shift
        self.pipe = pipe
        self.connection = SSHClient()
        self.connection.set_missing_host_key_policy(AutoAddPolicy)
        self.connection.load_system_host_keys()
        self.working_dir = None
        self.binary_dir = None
        self.storage_dir = None
        self.log = log
        self.bw_switch_flag = bw_switch_flag

        self.monitoring_stopped = True

        self.tcpdump_srv_if_pipe = Queue()
        self.tcpdump_srv_if = None
        self.tcpdump_srv_if_bash_pid = -1
        self.tcpdump_srv_if_pid = -1

        self.tcpdump_cli_if_pipe = Queue()
        self.tcpdump_cli_if = None
        self.tcpdump_cli_if_bash_pid = -1
        self.tcpdump_cli_if_pid = -1

        self.monitor_tc_pipe = Queue()
        self.monitor_tc = None
        self.monitor_tc_bash_pid = -1
        self.monitor_tc_pid = -1

        self.bw_switch_pipe = Queue()
        self.bw_switch = None
        self.bw_switch_pid = -1
        self.bw_switch_stopped = True

    def __del__(self):
        self.connection.close()

    def connect(self):
        try:
            self.connection.connect(hostname=self.host, port=222, username='root', key_filename='ssh/slipstream')
        except (BadHostKeyException, AuthenticationException, SSHException, socket.error) as e:
            print('Error: Failed to connect to bridge: ' + str(e), file=self.log)
            return False
        return True

    def set_working_dir(self, path: str):
        self.working_dir = path.rstrip('/')

    def set_binary_dir(self, path: str):
        self.binary_dir = path.rstrip('/')

    def set_storage_dir(self, path: str):
        self.storage_dir = path.rstrip('/')

    def start_monitoring(self):
        if self.binary_dir is None or self.working_dir is None:
            print('Error: Binary dir or working dir not set.', file=self.log)
            return False
        if not self.__start_tcpdump_srv_if():
            return False
        if not self.__start_tcpdump_cli_if():
            return False
        if not self.__start_monitor_tc():
            return False
        self.monitoring_stopped = False
        return True

    def __start_tcpdump_srv_if(self):
        self.tcpdump_srv_if = TCPDumpThread(self.connection, self.srv_if, self.binary_dir, self.working_dir,
                                            self.tcpdump_srv_if_pipe, self.log)
        print('Starting tcpdump_srv_if', file=self.log)
        self.tcpdump_srv_if.start()
        self.tcpdump_srv_if_bash_pid = self.tcpdump_srv_if_pipe.get()
        if self.tcpdump_srv_if_bash_pid == -1:
            return False
        try:
            self.tcpdump_srv_if_pid = self.tcpdump_srv_if_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for tcpdump_srv_if_pid: ' + str(e), file=self.log)
            return False
        print('tcpdump_srv_if started with pid: ' + str(self.tcpdump_srv_if_pid) + ' bash: '
              + str(self.tcpdump_srv_if_bash_pid), file=self.log)
        return True

    def __start_tcpdump_cli_if(self):
        self.tcpdump_cli_if = TCPDumpThread(self.connection, self.cli_if, self.binary_dir, self.working_dir,
                                            self.tcpdump_cli_if_pipe, self.log)
        print('Starting tcpdump_cli_if', file=self.log)
        self.tcpdump_cli_if.start()
        self.tcpdump_cli_if_bash_pid = self.tcpdump_cli_if_pipe.get()
        if self.tcpdump_cli_if_bash_pid == -1:
            return False
        try:
            self.tcpdump_cli_if_pid = self.tcpdump_cli_if_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for tcpdump_cli_if_pid: ' + str(e), file=self.log)
            return False
        print('tcpdump_cli_if started with pid: ' + str(self.tcpdump_cli_if_pid)
              + ' bash: ' + str(self.tcpdump_cli_if_bash_pid), file=self.log)
        return True

    def __start_monitor_tc(self):
        self.monitor_tc = MonitorTcThread(self.connection, self.srv_if, self.cli_if, self.binary_dir, self.working_dir,
                                          self.monitor_tc_pipe, self.log)
        print('Starting monitor_tc', file=self.log)
        self.monitor_tc.start()
        self.monitor_tc_bash_pid = self.monitor_tc_pipe.get()
        if self.monitor_tc_bash_pid == -1:
            return False
        try:
            self.monitor_tc_pid = self.monitor_tc_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for monitor_tc_pid: ' + str(e), file=self.log)
            return False
        print('monitor_tc started with pid: ' + str(self.monitor_tc_pid) + ' bash: ' + str(self.monitor_tc_bash_pid),
              file=self.log)
        return True

    def __start_bw_switch(self):
        self.bw_switch = BwSwitchThread(self.connection, self.srv_if, self.cli_if, self.binary_dir, self.working_dir,
                                        self.trace, self.offset, self.shift, self.bw_switch_pipe, self.log)
        print('Starting bw_switch', file=self.log)
        self.bw_switch.start()
        self.bw_switch_pid = self.bw_switch_pipe.get()
        if self.bw_switch_pid == -1:
            return False
        print('bw_switch started with pid: ' + str(self.bw_switch_pid), file=self.log)
        self.bw_switch_stopped = False
        return True

    def stop_monitoring(self):
        success = True
        self.monitoring_stopped = True
        if not self.__stop_tcpdump_srv_if():
            success = False
        if not self.__stop_tcpdump_cli_if():
            success = False
        if not self.__stop_monitor_tc():
            success = False
        return success

    def __stop_tcpdump_srv_if(self):
        if self.tcpdump_srv_if is None:
            print('tcpdump_srv_if was never created.', file=self.log)
            return False
        if self.tcpdump_srv_if_pid == -1 or self.tcpdump_srv_if_bash_pid == -1:
            print('Error: tcpdump_srv_if pids were never set. Can not stop!', file=self.log)
            return False
        if not self.tcpdump_srv_if.is_alive():
            print('Error: tcpdump_srv_if was already stopped. Trying to kill PIDs just in case.', file=self.log)
            ssh.execute(self.connection,
                        'kill -9 ' + str(self.tcpdump_srv_if_pid) + '; kill -9 ' + str(self.tcpdump_srv_if_bash_pid),
                        self.log)
            return False
        print('Stopping tcpdump_srv_if', file=self.log)
        ssh.execute(self.connection, 'kill ' + str(self.tcpdump_srv_if_pid), self.log)
        tries = 0
        while self.tcpdump_srv_if.is_alive():
            time.sleep(1)
            tries += 1
            if tries == self.kill_wait:
                print('CriticalError: tcpdump_srv_if does not respond to kill command. Trying to force.', file=self.log)
                ssh.execute(self.connection,
                            'kill -9 ' + str(self.tcpdump_srv_if_pid) + '; kill -9 '
                            + str(self.tcpdump_srv_if_bash_pid),
                            self.log)
                return False
        try:
            exit_code = self.tcpdump_srv_if_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for tcpdump_srv_if exit code: ' + str(e), file=self.log)
            return False
        print('tcpdump_srv_if stopped with exit code: ' + str(exit_code), file=self.log)
        return True

    def __stop_tcpdump_cli_if(self):
        if self.tcpdump_cli_if is None:
            print('tcpdump_cli_if was never created.', file=self.log)
            return False
        if self.tcpdump_cli_if_pid == -1 or self.tcpdump_cli_if_bash_pid == -1:
            print('Error: tcpdump_cli_if pids were never set. Can not stop!', file=self.log)
            return False
        if not self.tcpdump_cli_if.is_alive():
            print('Error: tcpdump_cli_if was already stopped. Trying to kill PIDs just in case.', file=self.log)
            ssh.execute(self.connection,
                        'kill -9 ' + str(self.tcpdump_cli_if_pid) + '; kill -9 ' + str(self.tcpdump_cli_if_bash_pid),
                        self.log)
            return False
        print('Stopping tcpdump_cli_if', file=self.log)
        ssh.execute(self.connection, 'kill ' + str(self.tcpdump_cli_if_pid), self.log)
        tries = 0
        while self.tcpdump_cli_if.is_alive():
            time.sleep(1)
            tries += 1
            if tries == self.kill_wait:
                print('CriticalError: tcpdump_cli_if does not respond to kill command. Trying to force.',
                      file=self.log)
                ssh.execute(self.connection, 'kill -9 ' + str(self.tcpdump_cli_if_pid) + '; kill -9 ' + str(
                    self.tcpdump_cli_if_bash_pid), self.log)
                return False
        try:
            exit_code = self.tcpdump_cli_if_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for tcpdump_cli_if exit code: ' + str(e), file=self.log)
            return False
        print('tcpdump_cli_if stopped with exit code: ' + str(exit_code), file=self.log)
        return True

    def __stop_monitor_tc(self):
        if self.monitor_tc is None:
            print('monitor_tc was never created.', file=self.log)
            return False
        if self.monitor_tc_pid == -1 or self.monitor_tc_bash_pid == -1:
            print('Error: monitor_tc pids were never set. Can not stop!', file=self.log)
            return False
        if not self.monitor_tc.is_alive():
            print('Error: monitor_tc was already stopped. Trying to kill PIDs just in case.', file=self.log)
            ssh.execute(self.connection,
                        'kill -9 ' + str(self.monitor_tc_pid) + '; kill -9 ' + str(self.monitor_tc_bash_pid), self.log)
            return False
        print('Stopping monitor_tc', file=self.log)
        ssh.execute(self.connection, 'kill ' + str(self.monitor_tc_pid), self.log)
        tries = 0
        while self.monitor_tc.is_alive():
            time.sleep(1)
            tries += 1
            if tries == self.kill_wait:
                print('CriticalError: monitor_tc does not respond to kill command. Trying to force.', file=self.log)
                ssh.execute(self.connection,
                            'kill -9 ' + str(self.monitor_tc_pid) + '; kill -9 ' + str(self.monitor_tc_bash_pid),
                            self.log)
                return False
        try:
            exit_code = self.monitor_tc_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for monitor_tc exit code: ' + str(e), file=self.log)
            return False
        print('monitor_tc stopped with exit code: ' + str(exit_code), file=self.log)
        return True

    def stop_bw_switch(self):
        self.bw_switch_stopped = True
        if self.bw_switch is None:
            print('bw_switch was never created.', file=self.log)
            return False
        if self.bw_switch_pid == -1:
            print('Error: bw_switch pid was never set. Can not stop!', file=self.log)
            return False
        if not self.bw_switch.is_alive():
            print('Error: bw_switch was already stopped. Trying to kill PID just in case.', file=self.log)
            ssh.execute(self.connection, 'kill -9 ' + str(self.bw_switch_pid), self.log)
            return False
        print('Stopping bw_switch', file=self.log)
        ssh.execute(self.connection, 'kill ' + str(self.bw_switch_pid), self.log)
        tries = 0
        while self.bw_switch.is_alive():
            time.sleep(1)
            if tries == self.kill_wait:
                print('CriticalError: bw_switch does not respond to kill command. Trying to force.', file=self.log)
                ssh.execute(self.connection, 'kill -9 ' + str(self.bw_switch_pid), self.log)
                return False
        return True

    def teardown(self, target_name: str):
        if self.working_dir is None or self.storage_dir is None:
            print('Working dir or storage dir not set!', file=self.log)
            return False
        mv_bridge_bw_switch = 'mv --backup=numbered ' + self.working_dir + '/bw-switch.log ' + self.storage_dir + '/' + target_name + '.bw-switch.log'
        mv_bridge_tcpdump_log_srv = 'mv --backup=numbered ' + self.working_dir + '/' + self.srv_if + '.log ' + self.storage_dir + '/' + target_name + '.' + self.srv_if + '.log'
        mv_bridge_tcpdump_pcap_srv = 'mv --backup=numbered ' + self.working_dir + '/' + self.srv_if + '.pcap ' + self.storage_dir + '/' + target_name + '.' + self.srv_if + '.pcap'
        mv_bridge_tcpdump_log_cli = 'mv --backup=numbered ' + self.working_dir + '/' + self.cli_if + '.log ' + self.storage_dir + '/' + target_name + '.' + self.cli_if + '.log'
        mv_bridge_tcpdump_pcap_cli = 'mv --backup=numbered ' + self.working_dir + '/' + self.cli_if + '.pcap ' + self.storage_dir + '/' + target_name + '.' + self.cli_if + '.pcap'
        mv_bridge_tc_log = 'mv --backup=numbered ' + self.working_dir + '/tc.log ' + self.storage_dir + '/' + target_name + '.tc.log'

        no_error = True
        print(mv_bridge_tcpdump_log_srv, file=self.log)
        if ssh.execute(self.connection, mv_bridge_tcpdump_log_srv, self.log) != 0:
            print('Error: Failed to move ' + self.srv_if + '.log.', file=self.log)
            no_error = False
        print(mv_bridge_tcpdump_pcap_srv, file=self.log)
        if ssh.execute(self.connection, mv_bridge_tcpdump_pcap_srv, self.log) != 0:
            print('Error: Failed to move ' + self.srv_if + '.pcap.', file=self.log)
            no_error = False
        print(mv_bridge_tcpdump_log_cli, file=self.log)
        if ssh.execute(self.connection, mv_bridge_tcpdump_log_cli, self.log) != 0:
            print('Error: Failed to move ' + self.cli_if + '.log.', file=self.log)
            no_error = False
        print(mv_bridge_tcpdump_pcap_cli, file=self.log)
        if ssh.execute(self.connection, mv_bridge_tcpdump_pcap_cli, self.log) != 0:
            print('Error: Failed to move ' + self.cli_if + '.pcap.', file=self.log)
            no_error = False
        print(mv_bridge_tc_log, file=self.log)
        if ssh.execute(self.connection, mv_bridge_tc_log, self.log) != 0:
            print('Error: Failed to move tc.log,', file=self.log)
            no_error = False
        if self.bw_switch_flag:
            print(mv_bridge_bw_switch, file=self.log)
            if ssh.execute(self.connection, mv_bridge_bw_switch, self.log) != 0:
                print('Error: Failed to move bw-switch.log.', file=self.log)
                no_error = False
        return no_error

    def run(self):
        if self.binary_dir is None or self.working_dir is None:
            print('Binary dir or working dir not set!', file=self.log)
            self.pipe.put(-1)
            return
        if self.bw_switch_flag:
            if not self.__start_bw_switch():
                self.pipe.put(-1)
                return
            self.pipe.put(self.bw_switch_pid)
            while self.bw_switch.is_alive():
                if (
                        not self.tcpdump_srv_if.is_alive() or not self.tcpdump_cli_if.is_alive() or not self.monitor_tc.is_alive()) \
                        and not self.monitoring_stopped:
                    print('Error: tcpdump or monitor-tc stopped unexpectedly.', file=self.log)
                    self.stop_bw_switch()
                    self.pipe.put(-1)
                    return
                time.sleep(1)
            try:
                exit_code = self.bw_switch_pipe.get(timeout=self.pipe_timeout)
                print('bw_switch stopped with exit code: ' + str(exit_code), file=self.log)
                exit_code = 0
            except Empty as e:
                print('Error: Timed out while waiting for bw_switch exit code: ' + str(e), file=self.log)
                exit_code = -1
            if not self.bw_switch_stopped:
                print('Error: bw_switch stopped unexpectedly.', file=self.log)
                exit_code = -1
        else:
            while self.tcpdump_srv_if.is_alive() and self.tcpdump_cli_if.is_alive() and self.monitor_tc.is_alive():
                time.sleep(1)
            exit_code = 0
            if not self.monitoring_stopped:
                print('Error: tcpdump or monitor-tc stopped unexpectedly.', file=self.log)
                exit_code = -1
        self.pipe.put(exit_code)
