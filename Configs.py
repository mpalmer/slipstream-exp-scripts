class RunConfig:
    def __init__(self, config_string: str):
        self.trace = None
        self.trace_long = None
        self.trace_offset = None
        self.trace_shift = None
        self.harpoon_bandwidth = None
        self.parallel_video = None
        config_split = config_string.split(',')
        if len(config_split) < 11:
            raise ValueError('Invalid run config: ' + config_string)
        self.__parse_shared_values(config_split)
        if config_split[4] == 'trace':
            self.__parse_trace_config(config_split)
        elif config_split[4] == 'harpoon':
            self.__parse_harpoon_config(config_split)
        elif config_split[4] == 'parallel':
            self.__parse_parallel_config(config_split)
        else:
            raise ValueError('Invalid run config: ' + config_string)
        self.number = -1

    def __parse_shared_values(self, config_split: list):
        self.ts = config_split[0]
        self.video = config_split[1]
        self.abr = config_split[2]
        self.buffer = config_split[3]
        self.traffic_simulation = config_split[4]
        self.qlen = config_split[5]
        self.delay = config_split[6]
        self.default_bandwidth = config_split[7]
        self.fine = False
        if config_split[8] == 'true':
            self.fine = True
        self.feature = config_split[9]

    def __parse_parallel_config(self, config_split: list):
        self.parallel_video = config_split[10]

    def __parse_harpoon_config(self, config_split: list):
        self.harpoon_bandwidth = config_split[10]

    def __parse_trace_config(self, config_split: list):
        self.trace = config_split[10]
        self.trace_shift = config_split[11]
        self.__translate_trace()

    def __translate_trace(self):
        if self.trace == 'att':
            self.trace_long = 'ATT-LTE-driving.down.sum'
            self.trace_offset = 0
        elif self.trace == 'tmobile':
            self.trace_long = 'TMobile-LTE-driving.down.sum'
            self.trace_offset = 0
        elif self.trace == 'verizon':
            self.trace_long = 'Verizon-LTE-driving.down.sum'
            self.trace_offset = 0
        elif self.trace == 'constant':
            self.trace_long = 'constant.down.sum'
            self.trace_offset = 0
        elif self.trace == 'fcc':
            self.trace_long = 'FCC-trace_32424_samknows1.lax1.level3.net.sum'
            self.trace_offset = 0
        elif self.trace == 'fcc_sanity':
            self.trace_long = 'FCC-sanity-trace_28869_ispmon.samknows.mlab1v4.lga03.measurement-lab.org.sum'
            self.trace_offset = 0
        elif self.trace == '3g':
            self.trace_long = '3G-report.2010-09-29_1827CEST.log.sum'
            self.trace_offset = 0
        else:
            self.trace_long = self.trace
            self.trace_offset = 0
            print('Warning: Using unconfigured trace:', self.trace)
            print('         Make sure that it exists.')
            #raise ValueError('Invalid trace: ' + self.trace)

    def __str__(self):
        line = [self.ts]
        line.append(self.video)
        line.append(self.abr)
        line.append(self.buffer)
        line.append(self.traffic_simulation)
        line.append(self.qlen)
        line.append(self.delay)
        line.append(self.default_bandwidth)
        if self.fine:
            line.append('true')
        else:
            line.append('false')
        line.append(self.feature)
        if self.traffic_simulation == 'harpoon':
            line.append(self.harpoon_bandwidth)
        elif self.traffic_simulation == 'parallel':
            line.append(self.parallel_video)
        else:
            line.append(self.trace)
            line.append(self.trace_shift)
        return ','.join(line)

class SetupConfig:
    def __init__(self, path: str = None):
        self.keys = {'SRV_IP': None,
                     'SRV_HOST': None,
                     'SRV_PORT': None,
                     'CLI_IP': None,
                     'CLI_HOST': None,
                     'BRIDGE_IP': None,
                     'BRIDGE_HOST': None,
                     'BRIDGE_SRV_IF': None,
                     'BRIDGE_CLI_IF': None}
        if path is not None:
            self.parse(path)

    def parse(self, path: str):
        with open(path, 'r') as f:
            for line in f:
                line_split = line.split('=')
                if len(line_split) == 2 and line_split[0] in self.keys:
                    self.keys[line_split[0]] = line_split[1].strip().strip('"')
        for key in self.keys:
            if self.keys[key] is None:
                raise ValueError('Incomplete setup configuration. Missing key: ' + key)

    def get_srv_ip(self):
        return self.keys['SRV_IP']

    def get_srv_host(self):
        return self.keys['SRV_HOST']

    def get_srv_port(self):
        return self.keys['SRV_PORT']

    def get_cli_ip(self):
        return self.keys['CLI_IP']

    def get_cli_host(self):
        return self.keys['CLI_HOST']

    def get_bridge_ip(self):
        return self.keys['BRIDGE_IP']

    def get_bridge_host(self):
        return self.keys['BRIDGE_HOST']

    def get_bridge_srv_if(self):
        return self.keys['BRIDGE_SRV_IF']

    def get_bridge_cli_if(self):
        return self.keys['BRIDGE_CLI_IF']
