from Configs import SetupConfig
from Configs import RunConfig
from ServerThread import ServerThread
from ClientThread import ClientThread
from Harpoon import HarpoonThread
from Bridge import BridgeThread
from queue import Queue
from queue import Empty
import time
import SSHHelper as ssh
import threading
import subprocess as sp
import os
import Defines


class Run(threading.Thread):
    def __init__(self, setup_config: SetupConfig, run_config: RunConfig, pipe: Queue):
        threading.Thread.__init__(self)
        self.pipe_timeout = 30
        self.heartbeat = 10
        self.die_timeout = 10
        self.thread_join_timeout = 10
        self.ssh_command = 'ssh -F ssh/config'  # Used for rsync
        self.binary_dir = Defines.BINARY_DIR
        log_dir = Defines.LOG_DIR

        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        self.log = open(log_dir + '/' + run_config.ts + '-' + str(run_config.number) + '.log', 'w')

        self.setup_config = setup_config
        self.run_config = run_config

        self.working_dir = Defines.WORKING_DIR_PREFIX + '/' + run_config.ts + '-' + str(run_config.number)
        self.storage_dir = self.__generate_storage_dir_path(Defines.STORAGE_DIR_PREFIX)
        self.nfs_storage_dir = self.__generate_storage_dir_path(Defines.NFS_STORAGE_DIR_PREFIX)
        self.pipe = pipe
        self.server_pipe = Queue()
        self.server = ServerThread(setup_config.get_srv_host(), setup_config.get_srv_port(), run_config.video
                                   , self.server_pipe, self.log)
        self.server.set_binary_dir(self.binary_dir)
        self.server.set_working_dir(self.working_dir)
        self.server.set_storage_dir(self.storage_dir)
        self.client_pipe = Queue()
        self.client = ClientThread(setup_config, run_config, run_config.video, run_config.fine, self.client_pipe, self.log)
        self.client.set_binary_dir(self.binary_dir)
        self.client.set_working_dir(self.working_dir)
        self.client.set_storage_dir(self.storage_dir)
        if run_config.traffic_simulation == 'harpoon':
            bw_switch_flag = False
            self.harpoon_pipe = Queue()
            self.harpoon = HarpoonThread(self.run_config, self.setup_config, self.harpoon_pipe, self.log)
            self.harpoon.set_binary_dir(self.binary_dir)
            self.harpoon.set_working_dir(self.working_dir)
            self.harpoon.set_storage_dir(self.storage_dir)
        elif run_config.traffic_simulation == 'parallel':
            self.working_dir_parallel_client = self.working_dir + '-parallel'
            self.storage_dir_parallel_client = self.storage_dir.replace('/s-', '/r-')
            self.nfs_storage_dir_parallel_client = self.nfs_storage_dir.replace('/s-', '/r-')
            bw_switch_flag = False
            self.parallel_client_pipe = Queue()
            self.parallel_client = ClientThread(setup_config, run_config, run_config.parallel_video, False,
                                                self.parallel_client_pipe, self.log)
            self.parallel_client.set_binary_dir(self.binary_dir)
            self.parallel_client.set_working_dir(self.working_dir_parallel_client)
            self.parallel_client.set_storage_dir(self.storage_dir_parallel_client)
        else:
            bw_switch_flag = True
        self.bridge_pipe = Queue()
        self.bridge = BridgeThread(setup_config.get_bridge_host(), setup_config.get_bridge_srv_if(),
                                   setup_config.get_bridge_cli_if(), run_config.trace_long, run_config.trace_offset,
                                   run_config.trace_shift, self.bridge_pipe, self.log, bw_switch_flag)
        self.bridge.set_binary_dir(self.binary_dir)
        self.bridge.set_working_dir(self.working_dir)
        self.bridge.set_storage_dir(self.storage_dir)

    def __del__(self):
        try:
            self.server.join(self.thread_join_timeout)
            if self.server.is_alive():
                print('Error: Timed out while waiting for server thread to die.', file=self.log)
        except RuntimeError as e:
            print('Error: Server thread join: ' + str(e), file=self.log)
        try:
            self.client.join(self.thread_join_timeout)
            if self.client.is_alive():
                print('Error: Timed out while waiting for client thread to die.', file=self.log)
        except RuntimeError as e:
            print('Error: Client thread join: ' + str(e), file=self.log)
        try:
            self.bridge.join(self.thread_join_timeout)
            if self.bridge.is_alive():
                print('Error: Timed out while waiting for bridge thread to die.', file=self.log)
        except RuntimeError as e:
            print('Error: Bridge thread join: ' + str(e), file=self.log)
        if self.run_config.traffic_simulation == 'harpoon':
            try:
                self.harpoon.join(self.thread_join_timeout)
                if self.harpoon.is_alive():
                    print('Error: Timed out while waiting for harpoon thread to die.', file=self.log)
            except RuntimeError as e:
                print('Error: Harpoon thread join: ' + str(e), file=self.log)
        elif self.run_config.traffic_simulation == 'parallel':
            try:
                self.parallel_client.join(self.thread_join_timeout)
                if self.parallel_client.is_alive():
                    print('Error: Timed out while waiting for parallel client thread to die.', file=self.log)
            except RuntimeError as e:
                print('Error: Parallel client thread join: ' + str(e), file=self.log)
        self.log.close()

    def __generate_storage_dir_path(self, prefix: str):
        buffer_seconds = int(int(self.run_config.buffer) / 1000)
        if self.run_config.traffic_simulation == 'harpoon':
            layer1 = 'harpoon' + self.run_config.harpoon_bandwidth
        elif self.run_config.traffic_simulation == 'parallel':
            layer1 = 'parallel'
        else:
            layer1 = self.run_config.trace
        layer1 += '-' + self.run_config.abr + '-' + str(buffer_seconds) + 's'
        layer2 = self.run_config.video.replace("reliable", "r").replace("slipstream", "s")
        return prefix + '/' + self.run_config.ts + '/meas/' + layer1 + '/' + layer2

    def setup(self):
        print('Setting up run on config: server: ' + self.setup_config.get_srv_host() + ' client: ' + self.setup_config.get_cli_host() + ' bridge: ' + self.setup_config.get_bridge_host(), file=self.log)
        if not self.server.connect() or not self.client.connect() or not self.bridge.connect():
            return False
        if self.run_config.traffic_simulation == 'harpoon':
            print('Running with harpoon')
            if not self.harpoon.connect():
                return False
        elif self.run_config.traffic_simulation == 'parallel':
            print('Running with concurrent stream')
            if not self.parallel_client.connect():
                return False
        if not self.__test_connectivity():
            return False
        if not self.__configure_bridge():
            return False
        if not self.__prepare_hosts():
            return False
        return True

    def __test_connectivity(self):
        print('Pinging server -> bridge...', file=self.log)
        if ssh.execute(self.server.connection, 'ping -c 1 ' + self.setup_config.get_bridge_ip(), self.log) != 0:
            return False
        print('Pinging server -> client...', file=self.log)
        if ssh.execute(self.server.connection, 'ping -c 1 ' + self.setup_config.get_cli_ip(), self.log) != 0:
            return False
        print('Pinging client -> bridge...', file=self.log)
        if ssh.execute(self.client.connection, 'ping -c 1 ' + self.setup_config.get_bridge_ip(), self.log) != 0:
            return False
        print('Pinging client -> server...', file=self.log)
        if ssh.execute(self.client.connection, 'ping -c 1 ' + self.setup_config.get_srv_ip(), self.log) != 0:
            return False
        return True

    def __configure_bridge(self):
        print('Configuring bridge...', file=self.log)
        default_rate = self.run_config.default_bandwidth + 'Mbit'
        command = '/sbin/tc qdisc del dev ' + self.setup_config.get_bridge_srv_if() + ' root; ' \
                  + '/sbin/tc qdisc del dev ' + self.setup_config.get_bridge_cli_if() + ' root; ' \
                  + '/sbin/ip link set ' + self.setup_config.get_bridge_cli_if() + ' qlen ' + self.run_config.qlen \
                  + ' && /sbin/ip link set ' + self.setup_config.get_bridge_srv_if() + ' qlen ' + self.run_config.qlen \
                  + ' && /sbin/tc qdisc add dev ' + self.setup_config.get_bridge_srv_if() + ' root handle 1: htb default 1' \
                  + ' && /sbin/tc qdisc add dev ' + self.setup_config.get_bridge_cli_if() + ' root handle 1: htb default 1' \
                  + ' && /sbin/tc class add dev ' + self.setup_config.get_bridge_srv_if() + ' parent 1: classid 0:1 htb rate ' + default_rate \
                  + ' && /sbin/tc class add dev ' + self.setup_config.get_bridge_cli_if() + ' parent 1: classid 0:1 htb rate ' + default_rate \
                  + ' && /sbin/tc qdisc add dev ' + self.setup_config.get_bridge_srv_if() + ' parent 1:1 handle 10: netem delay ' + self.run_config.delay + 'ms limit ' + self.run_config.qlen \
                  + ' && /sbin/tc qdisc add dev ' + self.setup_config.get_bridge_cli_if() + ' parent 1:1 handle 10: netem delay ' + self.run_config.delay + 'ms limit ' + self.run_config.qlen
        print(command, file=self.log)
        if ssh.execute(self.bridge.connection, command, self.log) != 0:
            return False
        return True

    def __prepare_hosts(self):
        print('Creating backup directory (via Rechenknecht) ' + self.nfs_storage_dir, file=self.log)
        os.makedirs(self.nfs_storage_dir, exist_ok=True)
        if self.run_config.traffic_simulation == 'parallel':
            print('Creating parallel client backup directory (via Rechenknecht) ' + self.nfs_storage_dir_parallel_client, file=self.log)
            os.makedirs(self.nfs_storage_dir_parallel_client, exist_ok=True)
        dir_create_command = 'mkdir -p ' + self.working_dir + '; mkdir -p ' + self.storage_dir

        print('Directory create command: ' + dir_create_command, file=self.log)
        print('Creating directories on server...', file=self.log)
        if ssh.execute(self.server.connection, dir_create_command, self.log) != 0:
            print('Error: Failed to create directories on server.', file=self.log)
            return False
        print('Creating directories on client...', file=self.log)
        if ssh.execute(self.client.connection, dir_create_command, self.log) != 0:
            print('Error: Failed to create directories on client.', file=self.log)
            return False
        if self.run_config.traffic_simulation == 'parallel':
            dir_create_command_parallel_client = 'mkdir -p ' + self.working_dir_parallel_client + '; mkdir -p ' + self.storage_dir_parallel_client
            print('Creating directories for parallel client...', file=self.log)
            print(dir_create_command_parallel_client, file=self.log)
            if ssh.execute(self.parallel_client.connection, dir_create_command_parallel_client, self.log) != 0:
                print('Error: Failed to create directories for parallel client.', file=self.log)
                return False

        print('Creating directories on bridge...', file=self.log)
        if ssh.execute(self.bridge.connection, dir_create_command, self.log) != 0:
            print('Error: Failed to create directories on bridge.', file=self.log)
            return False
        nfs_info_command = self.__generate_write_info_command(self.nfs_storage_dir)
        local_info_command = self.__generate_write_info_command(self.storage_dir)
        print('Creating README.txt in backup directory (via Rechenknecht)', file=self.log)
        print(nfs_info_command, file=self.log)
        os.system(nfs_info_command)
        print('Creating README.txt command:', file=self.log)
        print(local_info_command, file=self.log)
        print('Creating README.txt on server...', file=self.log)
        if ssh.execute(self.server.connection, local_info_command, self.log) != 0:
            print('Error: Failed to create README.txt on server.', file=self.log)
            return False
        print('Creating README.txt on client...', file=self.log)
        if ssh.execute(self.client.connection, local_info_command, self.log) != 0:
            print('Error: Failed to create README.txt on client.', file=self.log)
            return False
        print('Creating README.txt on bridge...', file=self.log)
        if ssh.execute(self.bridge.connection, local_info_command, self.log) != 0:
            print('Error: Failed to create README.txt on bridge.', file=self.log)
            return False
        rsync_server = 'rsync -ruv -e "' + self.ssh_command + '" bin extra-data/* quic-stuff/leaf_cert.* run-scripts/run-harpoon.sh harpoon-configs ' + self.setup_config.get_srv_host() + ':' + self.binary_dir
        rsync_client = 'rsync -ruv -e "' + self.ssh_command + '" bin quic-stuff/2048-sha256-root.pem run-scripts/run-harpoon.sh harpoon-configs ' + self.setup_config.get_cli_host() + ':' + self.binary_dir
        rsync_bridge = 'rsync -ruv -e "' + self.ssh_command + '" run-scripts/* extra-data/traces ' + self.setup_config.get_bridge_host() + ':' + self.binary_dir
        print('Rsync with server...', file=self.log)
        print(rsync_server, file=self.log)
        self.log.flush()
        if sp.call(rsync_server, stdout=self.log, stderr=sp.STDOUT, shell=True) != 0:
            print('Error: Rsync with server failed.', file=self.log)
            return False
        print('Rsync with client...', file=self.log)
        print(rsync_client, file=self.log)
        self.log.flush()
        if sp.call(rsync_client, stdout=self.log, stderr=sp.STDOUT, shell=True) != 0:
            print('Error: Rsync with client failed.', file=self.log)
            return False
        print('Rsync with bridge...', file=self.log)
        print(rsync_bridge, file=self.log)
        self.log.flush()
        if sp.call(rsync_bridge, stdout=self.log, stderr=sp.STDOUT, shell=True) != 0:
            print('Error: Rsync with bridge failed.', file=self.log)
            return False
        return True

    def __generate_write_info_command(self, path: str):
        path = path.rstrip('/')
        info = 'server: ' + self.setup_config.get_srv_host() + '\n'
        info += 'client: ' + self.setup_config.get_cli_host() + '\n'
        info += 'bridge: ' + self.setup_config.get_bridge_host() + '\n'
        if self.run_config.traffic_simulation == 'trace':
            info += 'shift: ' + self.run_config.trace_shift + ' s\n'
        info += 'qlen: ' + self.run_config.qlen + '\n'
        info += 'delay: ' + self.run_config.delay + ' ms\n'
        info += 'abr: ' + self.run_config.abr + '\n'
        info += 'default-bw: ' + self.run_config.default_bandwidth + ' Mbps\n'
        res = 'echo "' + info + '" > ' + path + '/' + str(self.run_config.number) + '.README.txt'
        return res

    def teardown(self):
        run_number = str(self.run_config.number)

        backup_command = 'sudo -u mappel cp --backup=numbered ' + self.storage_dir + '/' + run_number + '.* ' + self.nfs_storage_dir + '/'

        server_teardown = self.server.teardown(run_number)
        client_teardown = self.client.teardown(run_number)
        bridge_teardown = self.bridge.teardown(run_number)
        if self.run_config.traffic_simulation == 'harpoon':
            harpoon_teardown = self.harpoon.teardown(run_number)
            # If bridge teardown failed, do not overwrite the value
            if bridge_teardown:
                bridge_teardown = harpoon_teardown
        elif self.run_config.traffic_simulation == 'parallel':
            parallel_client_teardown = self.parallel_client.teardown(run_number + '.parallel')

        print('Backup command: ' + backup_command, file=self.log)
        print('Backing up server data...', file=self.log)
        if ssh.execute(self.server.connection, backup_command, self.log) != 0:
            print('Error: Failed to backup server data to NFS storage.', file=self.log)
        print('Backing up client data...', file=self.log)
        if ssh.execute(self.client.connection, backup_command, self.log) != 0:
            print('Error: Failed to backup client data to NFS storage.', file=self.log)
        if self.run_config.traffic_simulation == 'parallel':
            backup_command_parallel_client = 'sudo -u mappel cp --backup=numbered ' + self.storage_dir_parallel_client\
                                             + '/' + run_number + '.* ' + self.nfs_storage_dir_parallel_client + '/'
            print('Backing up parallel client data...', file=self.log)
            print(backup_command_parallel_client, file=self.log)
            if ssh.execute(self.parallel_client.connection, backup_command_parallel_client, self.log) != 0:
                print('Error: Failed to backup parallel client data to NFS storage.', file=self.log)
        print('Backing up bridge data...', file=self.log)
        if ssh.execute(self.bridge.connection, backup_command, self.log) != 0:
            print('Error: Failed to backup bridge data to NFS storage.', file=self.log)

        rmdir_command = 'rm -r ' + self.working_dir
        print('Directory remove command: ' + rmdir_command, file=self.log)
        if server_teardown:
            print('Removing working directory on server...', file=self.log)
            if ssh.execute(self.server.connection, rmdir_command, self.log) != 0:
                print('Error: Failed to remove working directory on server.', file=self.log)
        if client_teardown:
            print('Removing working directory on client...', file=self.log)
            if ssh.execute(self.client.connection, rmdir_command, self.log) != 0:
                print('Error: Failed to remove working directory on client.', file=self.log)
        if self.run_config.traffic_simulation == 'parallel' and parallel_client_teardown:
            rmdir_command_parallel_client = 'rm -r ' + self.working_dir_parallel_client
            print('Removing working directory for parallel client...', file=self.log)
            print(rmdir_command_parallel_client, file=self.log)
            if ssh.execute(self.parallel_client.connection, rmdir_command_parallel_client, self.log) != 0:
                print('Error: Failed to remove working directory for parallel client.', file=self.log)
        if bridge_teardown:
            print('Removing working directory on bridge...', file=self.log)
            if ssh.execute(self.bridge.connection, rmdir_command, self.log) != 0:
                print('Error: Failed to remove working directory on bridge.', file=self.log)

    def __kill_client(self, client, client_name: str):
        wait = 0
        while client.is_alive():
            print('Waiting for ' + client_name + ' to die...', file=self.log)
            wait += 1
            time.sleep(1)
            if wait == self.die_timeout:
                print('CriticalError: ' + client_name + ' does not want to die!', file=self.log)
                self.pipe.put(-2)
                return -2
        self.pipe.put(-1)
        return -1

    def __single_client_loop(self):
        self.client.start()
        heartbeat = 0
        while self.client.is_alive():
            if self.run_config.traffic_simulation == 'harpoon':
                if not self.server.is_alive() or not self.bridge.is_alive() or not self.harpoon.is_alive():
                    print('Error: Server, bridge or harpoon crashed during experiment.', file=self.log)
                    self.harpoon.stop_harpoon()
                    self.bridge.stop_monitoring()
                    self.server.stop()
                    if self.__kill_client(self.client, 'client') == -2:
                        return False
            else:
                if not self.server.is_alive() or not self.bridge.is_alive():
                    print('Error: Server or bridge crashed during experiment.', file=self.log)
                    self.bridge.stop_bw_switch()
                    self.bridge.stop_monitoring()
                    self.server.stop()
                    if self.__kill_client(self.client, 'client') == -2:
                        return False
            time.sleep(1)
            heartbeat += 1
            if heartbeat % self.heartbeat == 0:
                print('Experiment running since ' + str(heartbeat) + ' seconds...', file=self.log)
                self.log.flush()
        try:
            client_exit = self.client_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for client exit code: ' + str(e), file=self.log)
            if self.run_config.traffic_simulation == 'harpoon':
                self.harpoon.stop_harpoon()
            else:
                self.bridge.stop_bw_switch()
            self.bridge.stop_monitoring()
            self.server.stop()
            self.pipe.put(-1)
            return False
        if client_exit != 0:
            print('Error: Client returned non-zero exit code.', file=self.log)
            if self.run_config.traffic_simulation == 'harpoon':
                self.harpoon.stop_harpoon()
            else:
                self.bridge.stop_bw_switch()
            self.bridge.stop_monitoring()
            self.server.stop()
            self.pipe.put(-1)
            return False
        return True

    def __multi_client_loop(self):
        self.client.start()
        self.parallel_client.start()
        heartbeat = 0
        while self.client.is_alive() or self.parallel_client.is_alive():
            if not self.server.is_alive() or not self.bridge.is_alive():
                print('Error: Server or bridge crashed during experiment.', file=self.log)
                self.bridge.stop_bw_switch()
                self.bridge.stop_monitoring()
                self.server.stop()
                ret_code_client = self.__kill_client(self.client, 'client')
                ret_code_parallel_client = self.__kill_client(self.parallel_client, 'parallel client')
                if ret_code_client == -2 or ret_code_parallel_client == -2:
                    return False
            time.sleep(1)
            heartbeat += 1
            if heartbeat % self.heartbeat == 0:
                print('Experiment running since ' + str(heartbeat) + ' seconds...', file=self.log)
                self.log.flush()
        try:
            client_exit = self.client_pipe.get(timeout=self.pipe_timeout)
            parallel_client_exit = self.parallel_client_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for client or parallel client exit code: ' + str(e), file=self.log)
            self.bridge.stop_monitoring()
            self.server.stop()
            self.pipe.put(-1)
            return False
        if client_exit != 0 or parallel_client_exit != 0:
            print('Error: Client or parallel client returned non-zero exit code.', file=self.log)
            self.bridge.stop_monitoring()
            self.server.stop()
            self.pipe.put(-1)
            return False
        return True

    def run(self):
        self.server.start()
        try:
            server_pid = self.server_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for server pid: ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        if server_pid == -1:
            self.server.stop()
            self.pipe.put(-1)
            return
        print('Waiting for version', file=self.log)
        try:
            version = self.server_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for server version: ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        print('Waiting for server ready', file=self.log)
        try:
            self.server_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for server ready: ' + str(e), file=self.log)
        print('Server is ready!', file=self.log)
        if not self.bridge.start_monitoring():
            self.bridge.stop_monitoring()
            self.server.stop()
            self.pipe.put(-1)
            return
        self.bridge.start()
        if self.run_config.traffic_simulation == 'harpoon':
            self.harpoon.start()
            try:
                harpoon_srv_pid = self.harpoon_pipe.get(timeout=self.pipe_timeout)
                if harpoon_srv_pid == -1:
                    self.harpoon.stop_harpoon()
                    self.bridge.stop_monitoring()
                    self.server.stop()
                    self.pipe.put(-1)
                    return
                harpoon_cli_pid = self.harpoon_pipe.get(timeout=self.pipe_timeout)
                if harpoon_cli_pid == -1:
                    self.harpoon.stop_harpoon()
                    self.bridge.stop_monitoring()
                    self.server.stop()
                    self.pipe.put(-1)
                    return
            except Empty as e:
                print('Error: Timed out while waiting for harpon_srv or harpoon_cli pid: ' + str(e), file=self.log)
                self.harpoon.stop_harpoon()
                self.bridge.stop_monitoring()
                self.server.stop()
                self.pipe.put(-1)
        elif self.run_config.traffic_simulation == 'trace':
            try:
                bw_switch_pid = self.bridge_pipe.get(timeout=self.pipe_timeout)
            except Empty as e:
                print('Error: Timed out while waiting for bw_switch pid: ' + str(e), file=self.log)
                self.bridge.stop_bw_switch()
                self.bridge.stop_monitoring()
                self.server.stop()
                self.pipe.put(-1)
                return
            if bw_switch_pid == -1:
                self.bridge.stop_bw_switch()
                self.bridge.stop_monitoring()
                self.server.stop()
                self.pipe.put(-1)
                return
        if self.run_config.traffic_simulation == 'parallel':
            if not self.__multi_client_loop():
                return
        else:
            if not self.__single_client_loop():
                return
        stop_error = False
        if self.run_config.traffic_simulation == 'harpoon' and not self.harpoon.stop_harpoon():
            stop_error = True
        elif self.run_config.traffic_simulation == 'trace' and not self.bridge.stop_bw_switch():
            stop_error = True
        if not self.bridge.stop_monitoring():
            stop_error = True
        if not self.server.stop():
            stop_error = True
        wait = 0
        while self.bridge.is_alive():
            print('Waiting for bridge to die...', file=self.log)
            wait += 1
            time.sleep(1)
            if wait == self.die_timeout:
                print('CriticalError: Bridge does not want to die!', file=self.log)
                self.pipe.put(-2)
                return
        if stop_error:
            self.pipe.put(-1)
        else:
            self.pipe.put(0)
