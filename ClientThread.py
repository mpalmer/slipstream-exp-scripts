import threading
from Configs import SetupConfig
from Configs import RunConfig
from queue import Queue
from paramiko import SSHClient
from paramiko import AutoAddPolicy
from paramiko import BadHostKeyException
from paramiko import AuthenticationException
from paramiko import SSHException
import socket
import sys
import SSHHelper as ssh


class ClientThread(threading.Thread):
    def __init__(self, setup_config: SetupConfig, run_config: RunConfig, video: str, fine: bool, pipe: Queue, log=sys.stderr):
        threading.Thread.__init__(self)
        self.setup_config = setup_config
        self.run_config = run_config
        self.host = setup_config.get_cli_host()
        self.srv_ip = setup_config.get_srv_ip()
        self.srv_port = setup_config.get_srv_port()
        self.video = video
        self.fine = fine
        self.abr = run_config.abr
        self.buffer = run_config.buffer
        self.pipe = pipe
        self.connection = SSHClient()
        self.connection.set_missing_host_key_policy(AutoAddPolicy)
        self.connection.load_system_host_keys()
        self.binary_dir = None
        self.working_dir = None
        self.storage_dir = None
        self.log = log

    def __del__(self):
        self.connection.close()

    def connect(self):
        try:
            self.connection.connect(hostname=self.host, port=222, username='root', key_filename='ssh/slipstream')
        except (BadHostKeyException, AuthenticationException, SSHException, socket.error) as e:
            print('Failed to connect to client: ' + str(e), file=self.log)
            return False
        return True

    def set_binary_dir(self, path: str):
        self.binary_dir = path.rstrip('/')

    def set_working_dir(self, path: str):
        self.working_dir = path.rstrip('/')

    def set_storage_dir(self, path: str):
        self.storage_dir = path.rstrip('/')

    def teardown(self, target_name: str):
        if self.working_dir is None or self.storage_dir is None:
            print('Working dir or storage dir not set!', file=self.log)
            return False
        mv_client_out = 'mv --backup=numbered ' + self.working_dir + '/cli.out.log ' + self.storage_dir + '/' \
                        + target_name + '.cli.out.log'
        print(mv_client_out, file=self.log)
        if ssh.execute(self.connection, mv_client_out, self.log) != 0:
            print('Error: Failed to move cli.out.log.', file=self.log)
            return False
        return True

    def run(self):
        if self.binary_dir is None or self.working_dir is None:
            print('Binary dir or working dir not set!', file=self.log)
            self.pipe.put(-1)
            return
        print('Starting QUIC client', file=self.log)
        command = 'ulimit -c unlimited; cd ' + self.working_dir + '; ' + self.binary_dir \
                  + '/bin/quic_client -q --host=' + self.srv_ip + ' --port=' + self.srv_port \
                  + ' --abr_buf=' + self.buffer + ' --abr=' + self.abr
        if self.fine:
            command += ' --fine'
        if self.run_config.feature != '':
            command += ' --feature=' + self.run_config.feature
        command += ' https://www.example.org/' + self.video + '.mpd 2> ' \
                   + self.working_dir + '/cli.out.log 1> ' + self.working_dir + '/cli.mp4'
        print(command, file=self.log)
        exit_code = ssh.execute(self.connection, command, self.log)
        self.pipe.put(exit_code)
