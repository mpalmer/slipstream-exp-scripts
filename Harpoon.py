import threading
from queue import Queue
from queue import Empty
from paramiko import SSHClient
from paramiko import AutoAddPolicy
from paramiko import BadHostKeyException
from paramiko import AuthenticationException
from paramiko import SSHException
from Configs import RunConfig
from Configs import SetupConfig
import socket
import sys
import SSHHelper as ssh
import time


class HarpoonRunnerThread(threading.Thread):
    def __init__(self, connection: SSHClient, srv_ip: str, cli_ip: str, srv_port: str, working_dir: str,
                 binary_dir: str, cfg: str, pipe: Queue, log=sys.stderr):
        threading.Thread.__init__(self)
        self.connection = connection
        self.srv_ip = srv_ip
        self.cli_ip = cli_ip
        self.srv_port = srv_port
        self.working_dir = working_dir.rstrip('/')
        self.binary_dir = binary_dir.rstrip('/')
        self.cfg = cfg
        self.pipe = pipe
        self.log = log

    def run(self):
        command = [self.binary_dir + '/run-harpoon.sh']
        command.append(self.cfg)
        command.append(self.srv_ip)
        command.append(self.cli_ip)
        command.append(self.srv_port)
        command.append(self.working_dir)
        command.append(self.binary_dir + '/harpoon-configs')
        command = ' '.join(command)
        print(command, file=self.log)
        try:
            stdin, stdout, stderr = self.connection.exec_command(command)
        except SSHException as e:
            print('Error: Failed to execute command "' + command + '": ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        try:
            pid_bash = int(stdout.readline())
            pid_harpoon = int(stdout.readline())
        except ValueError as e:
            print('Error: Got something from server that was not the PID: ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        self.pipe.put(pid_bash)
        self.pipe.put(pid_harpoon)
        exit_code = stdout.channel.recv_exit_status()
        self.pipe.put(exit_code)


class HarpoonThread(threading.Thread):
    def __init__(self, run_config: RunConfig, setup_config: SetupConfig, pipe: Queue, log=sys.stderr):
        threading.Thread.__init__(self)
        self.pipe_timeout = 30
        self.kill_wait = 10
        self.srv_host = setup_config.get_srv_host()
        self.cli_host = setup_config.get_cli_host()
        self.srv_ip = setup_config.get_srv_ip()
        self.cli_ip = setup_config.get_cli_ip()
        self.srv_port = setup_config.get_srv_port()
        self.bandwidth = run_config.harpoon_bandwidth
        self.pipe = pipe
        self.srv_connection = SSHClient()
        self.srv_connection.set_missing_host_key_policy(AutoAddPolicy)
        self.srv_connection.load_system_host_keys()
        self.cli_connection = SSHClient()
        self.cli_connection.set_missing_host_key_policy(AutoAddPolicy)
        self.cli_connection.load_system_host_keys()
        self.working_dir = None
        self.binary_dir = None
        self.storage_dir = None
        self.log = log

        self.harpoon_stopped = False

        self.harpoon_srv_pipe = Queue()
        self.harpoon_srv = None
        self.harpoon_srv_bash_pid = -1
        self.harpoon_srv_pid = -1

        self.harpoon_cli_pipe = Queue()
        self.harpoon_cli = None
        self.harpoon_cli_bash_pid = -1
        self.harpoon_cli_pid = -1

    def __del__(self):
        self.srv_connection.close()
        self.cli_connection.close()

    def connect(self):
        try:
            self.srv_connection.connect(hostname=self.srv_host, port=222, username='root', key_filename='ssh/slipstream')
            self.cli_connection.connect(hostname=self.cli_host, port=222, username='root', key_filename='ssh/slipstream')
        except (BadHostKeyException, AuthenticationException, SSHException, socket.error) as e:
            print('Error: Failed to connect to harpoon client or server: ' + str(e), file=self.log)
            return False
        return True

    def set_working_dir(self, path: str):
        self.working_dir = path.rstrip('/')

    def set_binary_dir(self, path: str):
        self.binary_dir = path.rstrip('/')

    def set_storage_dir(self, path: str):
        self.storage_dir = path.rstrip('/')

    def __start_harpoon_srv(self):
        server_cfg = 'tcp_server_' + self.bandwidth + '.xml'
        self.harpoon_srv = HarpoonRunnerThread(self.srv_connection, self.srv_ip, self.cli_ip, self.srv_port,
                                               self.working_dir, self.binary_dir, server_cfg, self.harpoon_srv_pipe,
                                               self.log)
        print('Starting harpoon_srv', file=self.log)
        self.harpoon_srv.start()
        self.harpoon_srv_bash_pid = self.harpoon_srv_pipe.get()
        if self.harpoon_srv_bash_pid == -1:
            return False
        try:
            self.harpoon_srv_pid = self.harpoon_srv_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for harpoon_srv_pid: ' + str(e), file=self.log)
            return False
        print('harpoon_srv started with pid: ' + str(self.harpoon_srv_pid) + ' bash: ' + str(self.harpoon_srv_bash_pid),
              file=self.log)
        return True

    def __start_harpoon_cli(self):
        client_cfg = 'tcp_client.xml'
        self.harpoon_cli = HarpoonRunnerThread(self.cli_connection, self.srv_ip, self.cli_ip, self.srv_port,
                                               self.working_dir, self.binary_dir, client_cfg, self.harpoon_cli_pipe,
                                               self.log)
        print('Starting harpoon_cli', file=self.log)
        self.harpoon_cli.start()
        self.harpoon_cli_bash_pid = self.harpoon_cli_pipe.get()
        if self.harpoon_cli_bash_pid == -1:
            return False
        try:
            self.harpoon_cli_pid = self.harpoon_cli_pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for harpoon_cli_pid: ' + str(e), file=self.log)
            return False
        print('harpoon_cli started with pid: ' + str(self.harpoon_cli_pid) + ' bash: ' + str(self.harpoon_cli_bash_pid),
              file=self.log)
        return True

    def stop_harpoon(self):
        success = True
        self.harpoon_stopped = True
        if not self.__stop_thread(self.srv_connection, 'harpoon_srv', self.harpoon_srv, self.harpoon_srv_pid,
                                  self.harpoon_srv_bash_pid, self.harpoon_srv_pipe):
            success = False
        if not self.__stop_thread(self.cli_connection, 'harpoon_cli', self.harpoon_cli, self.harpoon_cli_pid,
                                  self.harpoon_cli_bash_pid, self.harpoon_cli_pipe):
            success = False
        return success

    def __stop_thread(self, connection, thread_name: str, thread: threading.Thread, pid: int, bash_pid: int, pipe: Queue):
        if thread is None:
            print(thread_name + ' was never created.', file=self.log)
            return False
        if pid == -1 or bash_pid == -1:
            print('Error: ' + thread_name + ' pids were never set. Can not stop!', file=self.log)
            return False
        if not thread.is_alive():
            print('Error: ' + thread_name + ' was already stopped. Trying to kill PIDs just in case.', file=self.log)
            ssh.execute(connection, 'kill -9 ' + str(pid) + '; kill -9 ' + str(bash_pid), self.log)
            return False
        print('Stopping ' + thread_name, file=self.log)
        ssh.execute(connection, 'kill -9 ' + str(pid), self.log)
        tries = 0
        while thread.is_alive():
            time.sleep(1)
            tries += 1
            if tries == self.kill_wait:
                print('CriticalError: ' + thread_name + ' does not respond to kill command. Trying to force.',
                      file=self.log)
                ssh.execute(connection, 'kill -9 ' + str(pid) + '; kill -9 ' + str(bash_pid), self.log)
                return False
        try:
            exit_code = pipe.get(timeout=self.pipe_timeout)
        except Empty as e:
            print('Error: Timed out while waiting for ' + thread_name + ' exit code: ' + str(e), file=self.log)
            return False
        print(thread_name + ' stopped with exit code: ' + str(exit_code), file=self.log)
        return True

    def teardown(self, target_name: str):
        if self.working_dir is None or self.storage_dir is None:
            print('Working dir or storage dir not set!', file=self.log)
            return
        mv_harpoon_srv_log = 'mv --backup=numbered ' + self.working_dir + '/harpoon.log ' + self.storage_dir + '/' + target_name + '.harpoon.srv.log'
        mv_harpoon_cli_log = 'mv --backup=numbered ' + self.working_dir + '/harpoon.log ' + self.storage_dir + '/' + target_name + '.harpoon.cli.log'

        print(mv_harpoon_srv_log, file=self.log)
        if ssh.execute(self.srv_connection, mv_harpoon_srv_log, self.log) != 0:
            print('Error: Failed to move harpoon.srv.log.', file=self.log)
        print(mv_harpoon_cli_log, file=self.log)
        if ssh.execute(self.cli_connection, mv_harpoon_cli_log, self.log) != 0:
            print('Error: Failed to move harpoon.cli.log.', file=self.log)

    def run(self):
        if self.binary_dir is None or self.working_dir is None:
            print('Binary dir or working dir not set!', file=self.log)
            self.pipe.put(-1)
            return
        if not self.__start_harpoon_srv():
            self.pipe.put(-1)
            return
        self.pipe.put(self.harpoon_srv_pid)
        if not self.__start_harpoon_cli():
            self.pipe.put(-1)
            return
        self.pipe.put(self.harpoon_cli_pid)
        self.harpoon_stopped = False
        while self.harpoon_srv.is_alive() and self.harpoon_cli.is_alive():
            time.sleep(1)
        exit_code = 0
        if not self.harpoon_stopped:
            print('Error: harpoon_srv or harpoon_cli stopped unexpectedly.', file=self.log)
            self.stop_harpoon()
            exit_code = -1
        self.pipe.put(exit_code)
