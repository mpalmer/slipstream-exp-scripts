#!/usr/bin/python3
import sys
import os
import time
import subprocess
from Run import Run
from Configs import SetupConfig
from Configs import RunConfig
from queue import Queue
from queue import Empty
import Defines

idle_check_wait = 5
workers = dict()
runs = dict()
run_numbers = dict()
run_queue = Queue()


def load_workers(worker_signature_path: str):
    print('Looking for worker configurations...')
    global workers
    global runs
    if not worker_signature_path.endswith('/'):
        worker_signature_path += '/'
    worker_file_it = os.scandir(worker_signature_path)
    for entry in worker_file_it:
        try:
            config = SetupConfig(worker_signature_path + entry.name)
        except ValueError as e:
            print('Error: Skipping invalid configuration ' + entry.name, file=sys.stderr)
            print(str(e), file=sys.stderr)
            continue
        print('Added configuration ' + entry.name)
        workers[entry.name] = config
        runs[entry.name] = None


def log_failed_run(run_config: RunConfig):
    global run_queue
    print('Error: Run failed: ' + str(run_config), file=sys.stderr)
    run_queue.put(str(run_config))


def start_run(worker: str, run_config: RunConfig):
    global runs
    pipe = Queue()
    run = Run(workers[worker], run_config, pipe)
    if not run.setup():
        print('Error: Setup ' + worker + ' seems to be broken.')
        runs[worker] = None
        return False
    run.start()
    runs[worker] = (run, pipe, run_config)
    return True


def cleanup_run(worker: str):
    global runs
    # Assumes run is not alive anymore
    run, pipe, run_config = runs[worker]
    try:
        exit_code = pipe.get(timeout=30)
    except Empty as e:
        print('Error: Timed out while waiting for exit code during cleanup: ' + str(e), file=sys.stderr)
        exit_code = -1
    if exit_code != 0:
        log_failed_run(run_config)
    else:
        run.teardown()
        with open(Defines.FINISHED_RUNS_FILE_NAME, 'a') as f:
            f.write(str(run_config) + '\n')
    runs[worker] = None


def locate_idle_worker(ignored_workers: list):
    for worker in runs:
        if worker in ignored_workers:
            continue
        if runs[worker] is None:
            return worker
        run, pipe, run_config = runs[worker]
        if not run.is_alive():
            cleanup_run(worker)
            return worker
    # No idle worker found
    return None


def wait_for_termination():
    all_workers_idle = False
    while not all_workers_idle:
        all_workers_idle = True
        for worker in runs:
            if runs[worker] is None:
                continue
            run, pipe, run_config = runs[worker]
            if not run.is_alive():
                cleanup_run(worker)
            else:
                all_workers_idle = False
        time.sleep(1)


def consume_run_queue(run_queue_file: str):
    global run_queue
    if not os.path.exists(run_queue_file):
        return False
    with open(run_queue_file, 'r') as f:
        for run in f:
            if run.startswith('#'):
                continue
            run_queue.put(run.strip())
    os.remove(run_queue_file)
    return True


def get_existing_run_number(ts: str):
    path = Defines.NFS_STORAGE_DIR_PREFIX + '/' + ts
    if not os.path.exists(path):
        return 1
    get_filenames = ['find', path, '-name', '*.README.txt', '-printf', '%f ']
    filenames = str(subprocess.check_output(get_filenames)).strip("b'")
    file_run_numbers = list()
    for filename in filenames.split():
        file_run_numbers.append(int(filename.split('.')[0]))
    if len(file_run_numbers) == 0:
        return 1
    file_run_numbers.sort(reverse=True)
    next_run_number = file_run_numbers[0] + 1
    print('Found existing runs for timestamp ' + ts + '. Assigning new number ' + str(next_run_number))
    return next_run_number


def load_run(run_string: str):
    global run_numbers
    try:
        run_config = RunConfig(run_string)
    except ValueError as e:
        print('Error: Skipping invalid run configuration: ' + run_string, file=sys.stderr)
        print(str(e), file=sys.stderr)
        return None
    if run_config.ts not in run_numbers:
        run_numbers[run_config.ts] = get_existing_run_number(run_config.ts)
    else:
        run_numbers[run_config.ts] += 1
    run_config.number = run_numbers[run_config.ts]
    return run_config


def consume_and_run(run_file: str):
    global run_queue
    while consume_run_queue(run_file) or not run_queue.empty():
        print('Found new runs.')
        while not run_queue.empty():
            run = run_queue.get()
            print('Trying to start run: ' + run)
            run_config = load_run(run)
            if run_config is None:
                continue
            print('Locating idle worker...')
            run_started = False
            ignored_workers = list()
            while not run_started:
                idle_worker = locate_idle_worker(ignored_workers)
                while idle_worker is None:
                    print('Waiting for idle worker...')
                    time.sleep(idle_check_wait)
                    idle_worker = locate_idle_worker(ignored_workers)
                run_started = start_run(idle_worker, run_config)
                if not run_started:
                    ignored_workers.append(idle_worker)
            print('Started run: ' + str(run_config))
        print('All runs started. Looking for new runs...')


def main(worker_signature_path: str, run_queue_file: str):
    if Defines.BINARY_DIR == '':
        print('BINARY_DIR in Defines.py is unset!')
        exit(1)
    load_workers(worker_signature_path)
    print('Looking for new runs...')
    while True:
        consume_and_run(run_queue_file)
        print('No new runs found. Waiting for termination of current runs')
        wait_for_termination()
        if run_queue.empty():
            break
    print('Done')


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <path/to/configs> <run queue file>')
        exit(1)
    main(sys.argv[1], sys.argv[2])
