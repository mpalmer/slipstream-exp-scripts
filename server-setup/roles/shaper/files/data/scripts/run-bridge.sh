#!/bin/bash
source global.sh
source config.sh
#source $1
#PREFIX=test
#mkdir -p $(dirname $PREFIX)
mkdir -p $PREFIX

tc qdisc del dev ${BRIDGE_SRV_IF} root
tc qdisc del dev ${BRIDGE_CLI_IF} root
ip link set ${BRIDGE_CLI_IF} qlen ${QLEN}
ip link set ${BRIDGE_SRV_IF} qlen ${QLEN}
tc qdisc add dev ${BRIDGE_SRV_IF} root handle 1: htb default 1
tc qdisc add dev ${BRIDGE_CLI_IF} root handle 1: htb default 1
tc class add dev ${BRIDGE_SRV_IF} parent 1: classid 0:1 htb rate ${RATE}
tc class add dev ${BRIDGE_CLI_IF} parent 1: classid 0:1 htb rate ${RATE}
tc qdisc add dev ${BRIDGE_SRV_IF} parent 1:1 handle 10: netem delay ${DELAY}ms loss ${LOSS}% ${BURST}% reorder ${REORDER}% ${REORDERBURST}%
tc qdisc add dev ${BRIDGE_CLI_IF} parent 1:1 handle 10: netem delay ${DELAY}ms loss ${LOSS}% ${BURST}% reorder ${REORDER}% ${REORDERBURST}%


tcpdump -i $BRIDGE_SRV_IF -w $PREFIX.$BRIDGE_SRV_IF.pcap & echo "$!" >> $PIDFILE
tcpdump -i $BRIDGE_CLI_IF -w $PREFIX.$BRIDGE_CLI_IF.pcap & echo "$!" >> $PIDFILE
#./monitor-tc.sh $BRIDGE_SRV_IF $BRIDGE_CLI_IF  > $PREFIX.tc-log & echo "$!" >> $PIDFILE
wait
