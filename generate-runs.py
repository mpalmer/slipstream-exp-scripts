#!/usr/bin/python3
import sys
import datetime

if len(sys.argv) != 3:
    print('usage: ' + sys.argv[0] + ' <config> <output>')
    exit(1)
config_file = sys.argv[1]
out_file = sys.argv[2]
config = dict()
required_keys = ['traffic-simulation', 'runs', 'buffers', 'videos', 'abrs', 'qlen', 'delay', 'default-bandwidth', 'fine']
allowed_abrs = ['bola', 'mpc', 'tput', 'bpp']
trace_lengths = {'att': 787, 'tmobile': 475, 'verizon': 1364, 'constant': 1, 'fcc': 1235, 'fcc_sanity': 350, '3g': 551}
allowed_traffic_simulations = ['trace', 'harpoon', 'parallel']
allowed_harpoon_bandwidths = ['5', '10', '15', '20']
with open(config_file, 'r') as f:
    for line in f:
        if line.startswith('#'):
            continue
        line_split = line.split(maxsplit=1)
        if len(line_split) == 1:
            continue
        config[line_split[0].rstrip(':')] = line_split[1].strip()
if 'timestamp' not in config:
    config['timestamp'] = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
for key in required_keys:
    if key not in config:
        print('Error: Missing key in config: ' + key, file=sys.stderr)
        exit(1)
buffers = config['buffers'].split(',')
videos = config['videos'].split(',')
runs = int(config['runs'])
abrs = config['abrs'].split(',')
for abr in abrs:
    if abr not in allowed_abrs:
        print('Error: Invalid abr specified: ' + abr, file=sys.stderr)
        exit(1)
if config['traffic-simulation'] not in allowed_traffic_simulations:
    print('Error: Invalid traffic simulation specified: ' + config['traffic-simulation'], file=sys.stderr)
    exit(1)
if config['traffic-simulation'] == 'trace':
    if 'traces' not in config:
        print('Error: \'trace\' traffic simulation specified but \'traces\' parameter is missing.', file=sys.stderr)
        exit(1)
    if 'shift' not in config:
        print('Error: \'trace\' traffic simulation specified but \'shift\' parameter is missing.', file=sys.stderr)
        exit(1)
    traces = config['traces'].split(',')
elif config['traffic-simulation'] == 'harpoon':
    if 'harpoon-bandwidths' not in config:
        print('Error: \'harpoon\' traffic simulation specified but \'harpoon-bandwidths\' parameter is missing.', file=sys.stderr)
        exit(1)
    harpoon_bandwidths = config['harpoon-bandwidths'].split(',')
    for bandwidth in harpoon_bandwidths:
        if bandwidth not in allowed_harpoon_bandwidths:
            print('Error: Invalid harpoon bandwidth specified: ' + bandwidth, file=sys.stderr)
            exit(1)
elif config['traffic-simulation'] == 'parallel':
    parallel_videos = list()
    for video in videos:
        if video.find('slipstream') == -1:
            print('Error: \'parallel\' traffic simulation specified but non-slipstream video was specified.', file=sys.stderr)
            exit(1)
        parallel_videos.append(video.replace('slipstream', 'reliable'))
feature = ''
if 'feature' in config:
    feature = config['feature']
# Values not needed, but sanity check
int(config['qlen'])
int(config['delay'])
float(config['default-bandwidth'])
for buffer in buffers:
    int(buffer)

with open(out_file, 'a') as out:
    if config['traffic-simulation'] == 'trace':
        out.write('# timestamp,video,abr,buffer(ms),traffic-simulation,qlen,delay(ms),default-bandwidth(Mbps),fine,feature,trace,shift(s)\n')
        for abr in abrs:
            for trace in traces:
                if config['shift'] == 'true':
                    if trace in trace_lengths:
                        shift = int(trace_lengths[trace] / runs)
                    else:
                        shift = 0
                else:
                    shift = 0
                for buffer in buffers:
                    for video in videos:
                        for run in range(0, runs):
                            line = [config['timestamp'], video, abr, buffer, config['traffic-simulation'],
                                    config['qlen'], config['delay'], config['default-bandwidth'], config['fine'],
                                    feature, trace, str(run * shift)]
                            out.write(','.join(line) + '\n')
    elif config['traffic-simulation'] == 'harpoon':
        out.write('# timestamp,video,abr,buffer(ms),traffic-simulation,qlen,delay(ms),default-bandwidth(Mbps),fine,feature,harpoon-bandwidth(Mbit)\n')
        for abr in abrs:
            for bandwidth in harpoon_bandwidths:
                for buffer in buffers:
                    for video in videos:
                        for run in range(0, runs):
                            line = [config['timestamp'], video, abr, buffer, config['traffic-simulation'],
                                    config['qlen'], config['delay'], config['default-bandwidth'], config['fine'],
                                    feature, bandwidth]
                            out.write(','.join(line) + '\n')
    elif config['traffic-simulation'] == 'parallel':
        out.write('# timestamp,video,abr,buffer(ms),traffic-simulation,qlen,delay(ms),default-bandwidth(Mbps),fine,feature,parallel-video\n')
        for abr in abrs:
            for buffer in buffers:
                for video in range(0, len(videos)):
                    for run in range(0, runs):
                        line = [config['timestamp'], videos[video], abr, buffer, config['traffic-simulation'],
                                config['qlen'], config['delay'], config['default-bandwidth'], config['fine'], feature,
                                parallel_videos[video]]
                        out.write(','.join(line) + '\n')

