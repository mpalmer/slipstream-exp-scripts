#TRACES=(constant-0.30 constant-1.35 constant-2.40 constant-3.45 constant-4.50 constant-5.55 constant-6.60 constant-7.65 constant-8.70 constant-9.75 constant-10.80 constant-11.85 constant-12.90 constant-13.95 constant-15.00)
#QLENS=(5 5 8 11 15 18 21 24 28 31 34 38 41 44 47)
TRACES=(constant-1.35 constant-3.45 constant-5.55 constant-7.65 constant-9.75 constant-11.85 constant-13.95 constant-15.00)
QLENS=(5 11 18 24 31 38 44 47)
for idx in ${!TRACES[*]}
do
    echo ${TRACES[$idx]}
    echo ${QLENS[$idx]}
    sed "s/<TRACE>/${TRACES[$idx]}/;s/<QLEN>/${QLENS[$idx]}/" constant-template.cfg >> run_configs/"${TRACES[$idx]}"
done
