#!/bin/bash
# usage: ./run-bw-switch <trace> <offset> <server if> <client if> <working dir> <shift>
# Starts an endless loop that changes the bandwidth of <server if> and
# <client if> once per second, according to the given trace file. <trace> needs
# to be an absolute path to the trace file. The values of the trace file are
# modified by <offset>. Note that <offset> should be a Mbit value. The start of
# the loop can be modified by <shift>.
# Creates a log of the applied switches at <working dir>/bw-switch.log.
# Outputs its own PID to stdout, which needs to be used to exit the endless
# loop.
set -euo pipefail
NUM_ARGS=6
if [ ! $# -eq ${NUM_ARGS} ]
then
  echo "Error: Invalid number of arguments specified: $# (expected ${NUM_ARGS})"
  exit 1
fi
TRACE=$1
OFFSET=$2
SRV_IF=$3
CLI_IF=$4
# Remove trailing slash from path if it exists.
WORKING_DIR=${5%/}
SHIFT=$6
# Apply a switch once every DUR seconds. Probably not necessary as a variable,
# but prevents magic numbers.
DUR=1
# Bash PID
echo $$
# Calculate the rates. Note that the $2 in the command refers to the second
# column of the trace file and _not_ the bash parameter $2.
# The command is a fancy, safer way to write the output into an array, see
# https://github.com/koalaman/shellcheck/wiki/SC2207 for more info.
mapfile -t RATES < <(awk '{print ((($2/1000000.0)*8)+'"${OFFSET}"')}' "${TRACE}")
if [ "$SHIFT" -gt ${#RATES[*]} ] || [ "$SHIFT" -lt 0 ]
then
  echo "Error: Specified shift is greater than trace length or less than zero."
  exit 1
fi
while true
do
    # Iterate from shift to end of the array. ${#RATES[*]} refers to the size of
    # the array.
    for((i = SHIFT; i < ${#RATES[*]}; i++))
    do
        CURR_RATE=${RATES[i]}
        echo "$(date +%s.%N) ${CURR_RATE} Mbit" >> "${WORKING_DIR}"/bw-switch.log
        tc class change dev "${SRV_IF}" parent 1: classid 0:1 htb rate "${CURR_RATE}"Mbit
        tc class change dev "${CLI_IF}" parent 1: classid 0:1 htb rate "${CURR_RATE}"Mbit
        sleep ${DUR}
    done
    # Iterate from start of the array to shift.
    for((i = 0; i < SHIFT; i++))
    do
        CURR_RATE=${RATES[i]}
        echo "$(date +%s.%N) ${CURR_RATE} Mbit" >> "${WORKING_DIR}"/bw-switch.log
        tc class change dev "${SRV_IF}" parent 1: classid 0:1 htb rate "${CURR_RATE}"Mbit
        tc class change dev "${CLI_IF}" parent 1: classid 0:1 htb rate "${CURR_RATE}"Mbit
        sleep ${DUR}
    done
done
