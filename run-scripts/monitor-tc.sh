#!/bin/bash
# (c) 2017 Theresa Enghardt
#
# Endless loop that queries tc stats and logs them to a file
# every $SLEEPTIME seconds
# Note: Needs right to invoke tc

IFACES=("$1" "$2")
TC_COMPONENTS=("qdisc" "class") # "filter")

TC=tc
IPTABLES=iptables
if [ "$USER" != "root" ] 
	then
	TC="/sbin/tc"
	#IPTABLES="sudo /sbin/iptables"
fi

#rm log/*.log

while true;
do
	for iface in ${IFACES[@]};
	do
		timestamp=$(date +%s)
		for tccomp in ${TC_COMPONENTS[@]};
		do
			echo "[$timestamp] [$iface]" # >> log/tc_${tccomp}_${iface}.log
			$TC -s ${tccomp} show dev $iface #>> log/tc_${tccomp}_${iface}.log
		done
	done
	#echo "[$timestamp]" >> log/tc_iptables.log
	#$IPTABLES -w -L FORWARD -t mangle -v >> log/tc_iptables.log
	sleep 1
done

