#!/bin/bash
# usage: ./run-tcpdump.sh <interface> <working dir>
# Starts tcpdump on the given interface and dumps into a pcap file located at
# <working dir>/<interface>.pcap. Also redirects stdout und stderr of tcpdump to
# a file located at <working dir>/<interface>.log.
# Writes two PIDs to stdout, first the PID of the script's bash instance, then
# the PID of the tcpdump process. It waits for the tcpdump process to finish and
# returns its exit code.
set -euo pipefail
NUM_ARGS=2
if [ ! $# -eq ${NUM_ARGS} ]
then
  echo "Error: Invalid number of arguments specified: $# (expected ${NUM_ARGS})"
  exit 1
fi
IF=$1
# Remove trailing slash from path if it exists.
DIR=${2%/}
# Bash PID
echo $$
/usr/sbin/tcpdump -i "${IF}" -s 100 -w "${DIR}"/"${IF}".pcap > "${DIR}"/"${IF}".log 2>&1 &
# tcpdump PID
echo $!
# Waiting for a specific PID saves the exit code of that process into $?
wait $!
exit $?