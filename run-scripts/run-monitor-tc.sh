#!/bin/bash
# usage: ./run-monitor-tc.sh <server if> <client if> <binary dir> <working dir>
# Starts the monitor-tc.sh script, which should be located at
# <binary dir>/monitor-tc.sh with the parameters <server if> and <client if>.
# Redirects stdout and stderr of the monitor-tc.sh into a file located at
# <working dir>/tc.log.
# Writes two PIDs to stdout, first the PID of the script's bash instance, then
# the PID of the monitor-tc.sh process. It waits for the monitor-tc.sh process
# to finish and returns its exit code.
set -euo pipefail
NUM_ARGS=4
if [ ! $# -eq ${NUM_ARGS} ]
then
  echo "Error: Invalid number of arguments specified: $# (expected ${NUM_ARGS})"
  exit 1
fi
SRV_IF=$1
CLI_IF=$2
# Remove trailing slash from paths if it exists.
BINARY_DIR=${3%/}
WORKING_DIR=${4%/}
# Bash PID
echo $$
"${BINARY_DIR}"/monitor-tc.sh "${SRV_IF}" "${CLI_IF}" > "${WORKING_DIR}"/tc.log 2>&1 &
# monitor-tc.sh PID
echo $!
# Waiting for a specific PID saves the exit code of that process into $?
wait $!
exit $?
