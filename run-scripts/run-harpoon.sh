#!/bin/bash
set -euo pipefail
NUM_ARGS=6
if [ ! $# -eq ${NUM_ARGS} ]
then
  echo "Error: Invalid number of arguments specified: $# (expected ${NUM_ARGS})"
  exit 1
fi
CFG=$1
SRV_IP=$2
CLI_IP=$3
SRV_PORT=$4
WORKING_DIR=${5%/}
CFG_DIR=${6%/}

# Bash PID
echo $$
# Change configs according to the sig we run on
sed      "s/<!--SRV_IP-->/${SRV_IP}/g"   "${CFG_DIR}"/"${CFG}" > "${WORKING_DIR}"/"${CFG}"
sed -i'' "s/<!--CLI_IP-->/${CLI_IP}/g"   "${WORKING_DIR}"/"${CFG}"
sed -i'' "s/<!--SRV_PORT-->/${SRV_PORT}/g" "${WORKING_DIR}"/"${CFG}"

export LD_LIBRARY_PATH=/usr/local/harpoon/plugins
# -v10 max verbosity
# -w600 traffic patterns are 600 seconds long
# -c loop after 600 seconds
/usr/local/harpoon/harpoon -v10 -w600 -c -f "${WORKING_DIR}"/"${CFG}" > "${WORKING_DIR}"/harpoon.log 2>&1 &
# harpoon PID
echo $!
# Waiting for a specific PID saves the exit code of that process into $?
wait $!
exit $?
