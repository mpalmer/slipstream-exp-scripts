TRACES=(step-1-1.500 step-1-3.625 step-1-5.750 step-1-7.875 step-1-10.000 step-1.500-1 step-3.625-1 step-5.750-1 step-7.875-1 step-10.000-1)
QLENS=(5 12 18 25 32 5 12 18 25 32)
for SUFFIX in -60s -70s
do
    for idx in ${!TRACES[*]}
    do
        echo ${TRACES[$idx]}
        echo ${QLENS[$idx]}
        sed "s/<TRACE>/${TRACES[$idx]}$SUFFIX/;s/<QLEN>/${QLENS[$idx]}/" step-template.cfg >> run_configs/"${TRACES[$idx]}$SUFFIX"
    done
done
