# Location (on the lab machines) of the log files and video while the experiment is running.
WORKING_DIR_PREFIX = '/local/tmp'
# Location (on the lab machines) of the log files and video after the experiment is finished.
STORAGE_DIR_PREFIX = '/local/SAVE'
NFS_STORAGE_DIR_PREFIX = '/INET/quic-storage/work/data'
# Location (on the lab machines) of everything that does not change between runs:
#  - Server and client binaries
#  - Video files
#  - QUIC files, like the certificates
# CHANGE THIS IF YOU ARE RUNNING MULTIPLE ExperimentRunners FOR DIFFERENT BINARIES.
BINARY_DIR = ''
# File (on rechenknecht) that stores the signatures of finished runs.
FINISHED_RUNS_FILE_NAME = 'completed_runs'
# Folder (on rechenknecht) in which logs are created.
LOG_DIR = 'logs'
