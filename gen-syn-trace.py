#!/usr/bin/python3

import sys

if len(sys.argv) != 7:
    print('usage: ' + sys.argv[0] + ' <duration in S> <drop at S> <start-bw in Mbit/s> <bw-change-to in Mbit/s> <change-shift in S> <output | - >')
    exit(1)

duration = int(sys.argv[1])
drop_at = int(sys.argv[2])
start_bw = float(sys.argv[3])
bw_change = float(sys.argv[4])
shift = float(sys.argv[5])
out = sys.argv[6]
print_bw = (start_bw * 1000000) / 8

with open(1 if out == '-' else out, 'w') as f:
    for n in range(1, duration + 1):
        if n > drop_at + shift :
            print_bw = ((bw_change) * 1000000) / 8
        f.write(str(n) + " " + str(int(print_bw)) + "\n")
        f.flush()
