#!/usr/bin/python3
from collections import namedtuple
import numpy as np
import os
import sys

LOG_SUFFIX = '.cli.out-downloads.csv'
OUTPUT_SUFFIX = '-downloads.csv'
DATA_SEPARATOR = ','
RunStats = namedtuple('RunStats', 'requests_total full_aborted part_total part_first part_retry part_completed part_aborted part_first_aborted part_retry_aborted')


def get_configurations(path: str):
    ret = list()
    it = os.scandir(path)
    for entry in it:
        if entry.is_dir():
            subdir = path + entry.name + '/'
            it_subdir = os.scandir(subdir)
            for entry_subdir in it_subdir:
                if entry_subdir.is_dir():
                    print(entry_subdir.name)
                    ret.append((entry.name + '-' + entry_subdir.name, subdir + entry_subdir.name + '/'))
    return ret


def get_runs(path: str):
    ret = list()
    if not os.path.exists(path + 'csv/'):
        print('Warning: No csv directory for config:', path, file=sys.stderr)
        return ret
    path += 'csv/'
    it = os.scandir(path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_SUFFIX):
            ret.append(path + entry.name)
    return ret


def process_run(run_file: str):
    total_requests = 0
    full_requests_aborted = 0
    part_requests = 0
    part_requests_first = 0
    part_requests_retry = 0
    part_requests_completed = 0
    part_requests_aborted = 0
    part_requests_first_aborted = 0
    part_requests_retry_aborted = 0
    with open(run_file, 'r') as f:
        # Consume headers
        f.readline()
        for line in f:
            line_split = list(map(int, line.split()))
            if len(line_split) != 6:
                print('Error: Invalid line:', line.strip(), file=sys.stderr)
                return None
            total_requests += 1
            # No part request
            if line_split[3] == 0:
                if line_split[5] == 1:
                    full_requests_aborted += 1
                continue
            part_requests += 1
            if line_split[2] == 1:
                part_requests_first += 1
            else:
                part_requests_retry += 1
            if line_split[4] == 1:
                part_requests_completed += 1
            if line_split[5] == 1:
                part_requests_aborted += 1
                if line_split[2] == 1:
                    part_requests_first_aborted += 1
                else:
                    part_requests_retry_aborted += 1
    return RunStats(total_requests, full_requests_aborted, part_requests, part_requests_first, part_requests_retry, part_requests_completed, part_requests_aborted, part_requests_first_aborted, part_requests_retry_aborted)

def write_sep_line(f, line: list):
    f.write(DATA_SEPARATOR.join(map(str, line)) + '\n')

def write_stats(output_file: str, stats: list):
    # 0 -> total_requests
    # 1 -> total_requests_aborted
    # 2 -> full_requests_aborted
    # 3 -> part_requests
    # 4 -> part_requests_first
    # 5 -> part_requests_retry
    # 6 -> part_requests_completed
    # 7 -> part_requests_aborted
    # 8 -> part_requests_first_aborted
    # 9 -> part_requests_retry_aborted
    stats_acc = np.zeros(10)
    for stat in stats:
        stats_acc[0] += stat.requests_total
        stats_acc[1] += stat.full_aborted + stat.part_aborted
        stats_acc[2] += stat.full_aborted
        stats_acc[3] += stat.part_total
        stats_acc[4] += stat.part_first
        stats_acc[5] += stat.part_retry
        stats_acc[6] += stat.part_completed
        stats_acc[7] += stat.part_aborted
        stats_acc[8] += stat.part_first_aborted
        stats_acc[9] += stat.part_retry_aborted
    stats_acc /= len(stats)
    with open(output_file, 'w') as f:
        line = ['category', 'absolute', 'percent_total', 'percent_relative']
        write_sep_line(f, line)
        line = ['total_requests', stats_acc[0], 100.0]
        write_sep_line(f, line)
        aborted_percent = (100 / stats_acc[0]) * stats_acc[1]
        line = ['total_aborted', stats_acc[1], aborted_percent]
        write_sep_line(f, line)
        full_aborted_percent = (100 / stats_acc[0]) * stats_acc[2]
        full_aborted_percent_relative = (100 / stats_acc[1]) * stats_acc[2]
        line = ['full_aborted', stats_acc[2], full_aborted_percent, full_aborted_percent_relative]
        write_sep_line(f, line)
        part_total_percent = (100 / stats_acc[0]) * stats_acc[3]
        line = ['part_total', stats_acc[3], part_total_percent]
        write_sep_line(f, line)
        part_first_percent = (100 / stats_acc[0]) * stats_acc[4]
        part_first_percent_relative = (100 / stats_acc[3]) * stats_acc[4]
        line = ['part_first', stats_acc[4], part_first_percent, part_first_percent_relative]
        write_sep_line(f, line)
        part_retry_percent = (100 / stats_acc[0]) * stats_acc[5]
        part_retry_percent_relative = (100 / stats_acc[3]) * stats_acc[5]
        line = ['part_retry', stats_acc[5], part_retry_percent, part_retry_percent_relative]
        write_sep_line(f, line)
        part_completed_percent = (100 / stats_acc[0]) * stats_acc[6]
        part_completed_percent_relative = (100 / stats_acc[3]) * stats_acc[6]
        line = ['part_completed', stats_acc[6], part_completed_percent, part_completed_percent_relative]
        write_sep_line(f, line)
        part_aborted_percent = (100 / stats_acc[0]) * stats_acc[7]
        part_aborted_percent_relative = (100 / stats_acc[1]) * stats_acc[7]
        line = ['part_aborted', stats_acc[7], part_aborted_percent, part_aborted_percent_relative]
        write_sep_line(f, line)
        part_aborted_first_percent = (100 / stats_acc[0]) * stats_acc[8]
        part_aborted_first_percent_relative = (100 / stats_acc[7]) * stats_acc[8]
        line = ['part_aborted_first', stats_acc[8], part_aborted_first_percent, part_aborted_first_percent_relative]
        write_sep_line(f, line)
        part_aborted_retry_percent = (100 / stats_acc[0]) * stats_acc[9]
        part_aborted_retry_percent_relative = (100 / stats_acc[7]) * stats_acc[9]
        line = ['part_aborted_retry', stats_acc[9], part_aborted_retry_percent, part_aborted_retry_percent_relative]
        write_sep_line(f, line)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment> <path/to/output>')
        print('use parse-downloads.py to generate required input files.')
        sys.exit(1)
    experiment_path = sys.argv[1]
    if not experiment_path.endswith('/'):
        experiment_path += '/'
    if os.path.exists(experiment_path + 'meas/'):
        experiment_path += 'meas/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    configurations = get_configurations(experiment_path)
    for name, config in configurations:
        runs = get_runs(config)
        run_stats = list()
        for run in runs:
            run_stats.append(process_run(run))
            if run_stats[-1] is None:
                break
        if len(run_stats) != len(runs):
            continue
        write_stats(output_path + name + OUTPUT_SUFFIX, run_stats)


