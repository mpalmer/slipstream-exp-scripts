#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import argparse
import sys
import os


def create_window(source_bitrates: list, target_avg_bitrate: float, scale: float, offset: int, size: int):
    if offset + size >= len(source_bitrates):
        window_data = source_bitrates[offset:len(source_bitrates)]
        window_data += source_bitrates[0:offset + size - len(source_bitrates)]
    else:
        window_data = source_bitrates[offset:offset + size]
    data_base = np.array(window_data)
    data_scaled = data_base * scale
    data_mean = np.mean(data_scaled)
    required_offset = target_avg_bitrate - data_mean
    data_offset = data_scaled + required_offset
    if np.min(data_offset) < 0:
        print('Warning: Skipping window with shift ' + str(offset) + ' because it contains negative bitrates.')
        return None
    return data_offset


def write_window_to_file(window: list, filename: str):
    print('Writing: ' + filename)
    count = 1
    with open(filename, 'w') as f:
        for entry in window:
            f.write(str(count) + ' ' + str(int(entry)) + '\n')
            count += 1


def plot_window_to_file(window: list, filename: str):
    print('Plotting: ' + filename)
    window_data = (np.array(window) / 1000000) * 8
    mean = np.mean(window_data)
    label_fontsize = 12
    tick_fontsize = 12
    linewidth = 1
    fig, ax1 = plt.subplots()
    title = os.path.splitext(os.path.basename(filename))[0]
    plt.title(title)
    ax1.set_xlabel('Seconds', fontsize=label_fontsize)
    ax1.set_ylabel('Mbit', fontsize=label_fontsize)
    ax1.tick_params(labelsize=tick_fontsize)
    ax1.axhline(y=mean, color='r', linestyle='--', linewidth=linewidth, label='Mean')
    ax1.axhline(y=5.8, color='g', linestyle='--', linewidth=linewidth, label='5800')
    ax1.axhline(y=4.3, color='pink', linestyle='--', linewidth=linewidth, label='4300')
    br_line, = ax1.step(np.arange(0, len(window_data)), window_data, linewidth=linewidth, label='Bitrate')
    ax1.set_xlim(0, len(window_data))
    ax1.set_ylim(bottom=0)
    ax1.xaxis.grid(linestyle='--')
    plt.legend()
    fig.set_size_inches(7, 5)
    #plt.show()
    plt.savefig(filename, bbox_inches='tight')
    plt.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate scaled traces')
    parser.add_argument('trace')
    parser.add_argument('bitrate', type=float, help='target average bitrate in Mbit/s')
    parser.add_argument('-sf', type=float, default=1.0, help='scale factor for the trace (default: %(default)i)')
    parser.add_argument('-max', type=int, help='limit the output to MAX windows')
    parser.add_argument('-ws', type=int, default=300, help='window size in seconds (default: %(default)is)')
    parser.add_argument('-step', type=int, default=1, help='step size between window beginnings in seconds (default: %(default)is)')
    parser.add_argument('-shift', type=int, default=0, help='shift of first window beginning in seconds (default: %(default)is)')
    parser.add_argument('-plot', action='store_true', help='if specified, generates plots for the created windows')
    args = parser.parse_args()
    bitrates = list()
    with open(args.trace, 'r') as f:
        for line in f:
            bitrates.append(int(line.split()[1]))
    if args.ws > len(bitrates) or args.shift >= len(bitrates) or args.step >= len(bitrates):
        print('Error: One or more of the specified parameters is larger than the source bitrate set.', file=sys.stderr)
        print('       source size: ' + str(len(bitrates)), file=sys.stderr)
        print('       window size: ' + str(args.ws), file=sys.stderr)
        print('             shift: ' + str(args.shift), file=sys.stderr)
        print('              step: ' + str(args.step), file=sys.stderr)
        exit(1)
    target_avg_bitrate_bytes = (args.bitrate * 1000000) / 8
    max_windows = len(bitrates)
    if args.max is not None:
        max_windows = args.max
    windows = list()
    max_offset = 0
    offsets = list()
    for offset in range(args.shift, len(bitrates), args.step):
        if len(windows) >= max_windows:
            break
        window = create_window(bitrates, target_avg_bitrate_bytes, args.sf, offset, args.ws)
        if window is not None:
            windows.append(window)
            offsets.append(offset)
        max_offset = offset
    start_lower = args.step - (len(bitrates) - max_offset)
    for offset in range(start_lower, args.shift, args.step):
        if len(windows) >= max_windows:
            break
        window = create_window(bitrates, target_avg_bitrate_bytes, args.sf, offset, args.ws)
        if window is not None:
            windows.append(window)
            offsets.append(offset)
    outname_prefix = args.trace
    if args.trace.endswith('.csv'):
        outname_prefix = args.trace[:-4]
    count = 0
    for window in windows:
        outname_trace = outname_prefix + '-' + str(offsets[count]) + '.csv'
        write_window_to_file(window, outname_trace)
        if args.plot:
            outname_plot = outname_prefix + '-' + str(offsets[count]) + '.png'
            plot_window_to_file(window, outname_plot)
        count += 1
    exit(0)
