#!/usr/bin/python3
import sys
import os
from collections import namedtuple
import numpy as np

ByteRange = namedtuple('ByteRange', 'start end pos')

def parse_key_value_pair(pair: str):
    pair_split = pair.split('=')
    if len(pair_split) != 2:
        return None, None
    return pair_split[0], pair_split[1].strip('"/>')


def parse_key_value_line(line: str):
    key_value_pairs = dict()
    line_split = line.split()
    for pair in line_split:
        key, value = parse_key_value_pair(pair)
        if key is None:
            continue
        key_value_pairs[key] = value
    return key_value_pairs

def parse_ranges(ranges: str):
    parsed_ranges = list()
    ranges_split = ranges.split(',')
    range_count = 0
    for range_string in ranges_split:
        range_split = range_string.split('-')
        if len(range_split) != 2:
            print('Error: Invalid range:', range_string, file=sys.stderr)
            continue
        parsed_ranges.append(ByteRange(int(range_split[0]), int(range_split[1]), range_count))
        range_count += 1
    parsed_ranges.sort()
    return parsed_ranges


def parse_segmenturl(line: str):
    line_attributes = parse_key_value_line(line)
    if 'unreliable' not in line_attributes:
        print('Error: No "unreliable" attribute in SegmentURL:', line, file=sys.stderr)
        return None
    return parse_ranges(line_attributes['unreliable'])


def parse_mpd(mpd: str):
    repr_count = 0
    representations = list()
    segments = list()
    with open(mpd, 'r') as f:
        for line in f:
            if '<Representation id' in line:
                if repr_count > 0:
                    representations.append(list(segments))
                    segments.clear()
                repr_count += 1
            elif 'SegmentURL' in line:
                byte_ranges = parse_segmenturl(line)
                segments.append(byte_ranges)
        # Append last representation
        if len(segments) > 0:
            representations.append(segments)
            segments.clear()
    return representations


def write_output(output_path: str, q: int, segments: list):
    offset_values = list()
    for segment in segments:
        for idx, byte_range in enumerate(segment):
            offset_values.append(byte_range.pos - idx)
    offset_values.sort()
    p_values = np.arange(len(offset_values)) / (len(offset_values) - 1)
    with open(output_path + str(q) + '.dat', 'w') as f:
        f.write('p offset\n')
        for idx, value in enumerate(offset_values):
            f.write(str(p_values[idx]) + ' ' + str(value) + '\n')


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage:',sys.argv[0],'<MPD> <path/to/output>')
        exit(1)
    mpd = sys.argv[1]
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    representations = parse_mpd(mpd)
    for idx, representation in enumerate(representations):
        write_output(output_path, len(representations) - (idx + 1), representation)

