#!/usr/bin/python3
import argparse
import os
import sys

LOG_SUFFIX = '.log'
OUTPUT_SUFFIX = '-losses.csv'
OUTPUT_DELIMITER = ','


def parse_line(line: str) -> (str, dict):
    if not line.startswith('['):
        return str(), dict()
    line_split = line.strip().split()
    key = line_split[0].strip('[]')
    if len(line_split) == 1:
        return key, dict()
    entries = {kv[0]:kv[1] for kv in [e.split(':') for e in line_split[1:]] if len(kv) == 2}
    return key, entries


def parse_loss_line(line: str) -> int:
    line_split = line.strip().split()
    if not line_split[0] == '[loss]':
        print(f'Error: Invalid loss line: {line.strip()}', file=sys.stderr)
        return -1
    return sum([int(e.split(',')[1]) for e in line_split[1:]])


def main() -> None:
    desc = """Get the remaining losses for unreliable parts of the segments,
              i.e., the loss remaining after hole fill. Excludes losses /
              unfinished sections of optional parts."""
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('log')
    args = parser.parse_args()

    path, base = os.path.split(args.log)
    if not path:
        path = './'
    else:
        path += '/'
    if not base.endswith(LOG_SUFFIX):
        print(f'Error: Log file does not end with {LOG_SUFFIX}', file=sys.stderr)
        sys.exit(1)
    csv_dir = path + 'csv/'
    out_file = csv_dir + base[:-len(LOG_SUFFIX)] + OUTPUT_SUFFIX

    losses = list()
    num_segments = 0
    curr_hole = 0
    curr_loss = 0
    curr_segment_no = 0
    in_optional = False
    with open(args.log, 'r') as f:
        for line in f:
            key, entries = parse_line(line)
            if key == 'loading-optional':
                in_optional = True
            elif not in_optional and key == 'loss':
                curr_loss = parse_loss_line(line)
            elif not in_optional and curr_hole == 0 and key == 'hole-fill':
                    curr_hole = int(entries['fill']) + int(entries['loss'])
            elif key == 'segment':
                if curr_segment_no > 0:
                    size = int(entries['ssu'])
                    # No attempt at filling the hole was made.
                    if curr_hole == 0 and curr_loss > 0:
                        curr_hole = curr_loss
                    hole_percentage = (curr_hole / size) * 100
                    loss_percentage = (curr_loss / size) * 100
                    losses.append((curr_segment_no, size, curr_hole, curr_loss, hole_percentage, loss_percentage))
                curr_segment_no += 1
                curr_hole = 0
                curr_loss = 0
                in_optional = False
    os.makedirs(csv_dir, exist_ok=True)
    with open(out_file, 'w') as f:
        f.writelines('segment_no,unrel_size(bytes),hole(bytes),loss(bytes),hole(percentage),loss(percentage)\n')
        for l in losses:
            if l[2] < l[3]:
                print(f'That can not be right: {l}', file=sys.stderr)
            f.write(OUTPUT_DELIMITER.join(map(str, l)) + '\n')


if __name__ == '__main__':
    main()
    sys.exit(0)

