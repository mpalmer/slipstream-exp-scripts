### Plotting with gnuplot

In general, all `.gp` gnuplot scripts have one or more input files (`IN_FILE`,
or `IN_FILE01`, etc.) and one output file (`OUT_FILE`). To specify the input and
output files, call gnuplot like this:

```
gnuplot -e "IN_FILE='data.csv'" -e "OUT_FILE='plot.pdf'" plot_script.gp
```

### Scripts

The scripts in this folder were never part of our standard set of plots, so
there is no Makefile or batch integration for these. There were supposed to be
manually executed for specific runs.

Some scripts require a SSIM database. These can be found
at `/INET/quic-storage/work/data/ssim-label-data`. For the normal VoD videos,
choose the database `4s-scaled/ssim.db`. For further information, check
the `README.md` over there.

* `get-bitrate-convergence.py`:
    * Takes `*.cli.out-br_switches.csv` as input.
    * Plot results with `plot-bitrate-convergence[-comp].gp`.
* `get-ssim-convergence[-avg].py`:
    * Takes `*.cli.out.csv` as input.
    * Plot results with `plot-ssim-convergence[-avg][-comp]/gp`.
* `get-reliable-ssim-convergence[-avg].py`:
    * Takes a SSIM database (see above), a video (usually one out
      of `bbb|ed|sintel|tos`), and a `*.cli.out.csv` file as input.
    * Plot results with `plot-ssim-convergence[-avg][-comp]/gp`.
