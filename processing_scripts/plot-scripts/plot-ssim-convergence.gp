BASE_FONT='Clear Sans, 22'
XTICS_FONT=', 20'
YTICS_FONT=', 22'
KEY_FONT=',22'

set terminal pdfcairo enhanced font BASE_FONT linewidth 2 rounded dashed

set style line 80 lc rgb "#202020" lt 1 lw 1
# Border: 3 (X & Y axes)
set border 3 back ls 80

set style line 81 lc rgb "#808080" lt 1 lw 0.50 dt 3
set style line 82 lc rgb "#999999" lt 1 lw 0.25 dt 3
set grid ytics mytics xtics mxtics back ls 81, ls 82

# Based on colorbrewer2.org's Set1 palette.
set style line 1 lt 1 lc rgb "#e41a1c" lw 2.0 dt 1
set style line 2 lt 1 lc rgb "#377eb8" lw 2.0 dt 1
set style line 3 lt 1 lc rgb "#4daf4a" lw 2.0 dt 1
set style line 4 lt 1 lc rgb "#984ea3" lw 1.0 dt 1
set style line 5 lt 1 lc rgb "#ff7f00" lw 1.0 dt 1

set style line 99 lt 1 lc rgb "#006d2c" lw 1.0 dt 1
set style line 95 lt 1 lc rgb "#2ca25f" lw 1.0 dt 1

set xtics border in scale 1,0.5 nomirror norotate autojustify
set ytics border in scale 1,0.5 nomirror norotate autojustify


set key top left maxrows 2 font KEY_FONT
set key samplen 1.2 width 1

# Adjust Y-axis range.
#set yrange[0:1]
# Adjust how the Y-axis tics should increment.
#set ytics 0, 0.2 font YTICS_FONT
# Adjust number of breaks between two major tics.
#set mytics 2
set ylabel "SSIM diff" offset 1.5,0

# Adjust X-axis range.
set xrange[0:100]
# Adjust how the X-axis tics should increment.
set xtics 20 offset 0,0.5 font XTICS_FONT
# Adjust number of breaks between two major tics.
set mxtics 2
set xlabel "% played" offset 0,1.2

#set lmargin 6.5


set parametric

# Dummy variable for curves.
set trange [0:1]

set key autotitle columnheader

set output OUT_FILE

plot IN_FILE u 1:2 notitle w l ls 1 lw 1 dt 1
