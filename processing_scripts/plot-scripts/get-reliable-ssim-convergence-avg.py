#!/usr/bin/python3
import sys
import os
import sqlite3
from collections import namedtuple

QUALITY_MAP = {'bbb'   :{161:0, 236:1, 375:2, 560:3, 749:4, 1048:5, 1745:6, 2347:7, 2995:8, 4290:9, 5785:10, 7378:11, 9978:12},
               'ed'    :{161:0, 236:1, 376:2, 561:3, 751:4, 1051:5, 1753:6, 2354:7, 3005:8, 4311:9, 5817:10, 7421:11, 9980:12},
               'sintel':{161:0, 237:1, 378:2, 561:3, 750:4, 1052:5, 1750:6, 2343:7, 2990:8, 4290:9, 5786:10, 7265:11, 9753:12},
               'tos'   :{161:0, 237:1, 376:2, 561:3, 751:4, 1049:5, 1747:6, 2350:7, 2990:8, 4284:9, 5795:10, 7392:11, 10003:12}}
SEGMENT_COUNT = 75
SEGMENT_LENGTH_IN_S = 4
VIDEO_LENGTH = SEGMENT_COUNT * SEGMENT_LENGTH_IN_S


def get_ssims(run_file: str, video_name: str, db_cursor: sqlite3.Cursor):
    ssim_stmt = 'SELECT ssim FROM data WHERE frame_order = "original" AND video = ? AND quality = ? AND segment_no = ? and frames_dropped = 0'
    # First segment has no SSIM
    ret = {1:0}
    prev_ssim = 0
    with open(run_file, 'r') as f:
        # Consume header
        f.readline()
        # Skip first segment
        f.readline()
        for line in f:
            line_split = line.split(',')
            if len(line_split) < 2:
                print('Error: Failed to get bitrate from line: ' + line.strip(), file=sys.stderr)
                return None
            segment_count = int(line_split[0])
            bitrate = int(line_split[1])
            quality = QUALITY_MAP[video_name][bitrate]
            db_cursor.execute(ssim_stmt, (video_name,  quality, segment_count))
            query_ret = db_cursor.fetchone()
            if query_ret is None:
                print('Error: Could not find SSIM entry for video: ' + video_name + ' q: ' + str(quality) + ' segment_no: ' + str(segment_count), file=sys.stderr)
                return None
            ssim = query_ret[0]
            #ret[segment_count] = abs(prev_ssim - ssim)
            ret[segment_count] = ssim
            prev_ssim = ssim
    return ret


def get_convergence_data(switch_data: dict):
    ret = list()
    for i in range(1, SEGMENT_COUNT + 1):
        if i not in switch_data:
            ret.append(0)
        else:
            ret.append(switch_data[i])
    return ret


def convert_to_percent_played(data: list):
    ret = list()
    switch_acc = 0
    played_acc = 4
    segment_acc = 0
    for magnitude in data[1:]:
        switch_acc += magnitude
        played_acc += 4
        segment_acc += 1
        percent_played = (100 / VIDEO_LENGTH) * played_acc
        switch_rate = switch_acc / segment_acc
        ret.append((percent_played, switch_rate))
    return ret


def write_output(output: str, data: list):
    with open(output, 'w') as f:
        f.write('percent_played acc_ssim_change\n')
        for entry in data:
            f.write(' '.join(map(str, entry)) + '\n')


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print('usage: ' + sys.argv[0] + ' <database> <video> <csv-file> <output>')
        exit(1)
    database = sys.argv[1]
    video = sys.argv[2]
    csv_file = sys.argv[3]
    output = sys.argv[4]
    db_connection = sqlite3.connect(database)
    db_cursor = db_connection.cursor()
    ssims = get_ssims(csv_file, video, db_cursor)
    convergence_data = get_convergence_data(ssims)
    plot_data = convert_to_percent_played(convergence_data)
    write_output(output, plot_data)
