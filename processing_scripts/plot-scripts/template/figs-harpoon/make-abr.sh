#!/bin/bash
if [ ! $# -eq 2 ]
then
    echo "usage: $0 <abr> <harpoon>"
    exit
fi
TEMPLATE="template-abr.tex"

ABR="$1"
HARPOON="$2"
OUT_FILE="$HARPOON"-"$ABR".tex
sed -e "s/%ABR/$ABR/" -e "s/%HARPOON/$HARPOON/" "$TEMPLATE" >> "$OUT_FILE"
pdflatex -interaction nonstopmode "$OUT_FILE"
rm "$OUT_FILE" *.log *.aux
