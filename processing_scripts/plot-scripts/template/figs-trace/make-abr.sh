#!/bin/bash
if [ ! $# -eq 2 ]
then
    echo "usage: $0 <abr> <trace>"
    exit
fi
TEMPLATE="template-abr.tex"

ABR="$1"
TRACE="$2"
OUT_FILE="$TRACE"-"$ABR".tex
sed -e "s/%ABR/$ABR/" -e "s/%TRACE/$TRACE/" "$TEMPLATE" >> "$OUT_FILE"
pdflatex -interaction nonstopmode "$OUT_FILE"
rm "$OUT_FILE" *.log *.aux
