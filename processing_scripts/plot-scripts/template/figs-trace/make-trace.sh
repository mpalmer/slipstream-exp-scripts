#!/bin/bash
if [ ! $# -eq 2 ]
then
    echo "usage: $0 <abr> <experiment>"
    exit
fi
TEMPLATE="template-trace.tex"

ABR="$1"
EXPERIMENT="$2"
OUT_FILE="$ABR"-"$EXPERIMENT".tex
sed -e "s/%ABR/$ABR/" -e "s/%EXPERIMENT/$EXPERIMENT/" "$TEMPLATE" >> "$OUT_FILE"
pdflatex -interaction nonstopmode "$OUT_FILE"
rm "$OUT_FILE" *.log *.aux
