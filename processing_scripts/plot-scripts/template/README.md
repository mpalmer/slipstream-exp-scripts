### Preparation

Before you can plot anything, you first need the get the data from the logs in
to CSV files. Depending on what you want to plot, you have to run more than one
parse script. The mentioned scripts are all in the `processing_scripts` folder.
The minimum required script for most things is `log-parser-vod.py`. These list
entries correspond to make targets in the `stats` folder. Not all stat files
have corresponding plot scripts.

* `log-parser-vod.py`:
    * `avg-bitrates`
    * `badseg-ratios` (no bpp)
    * `br-switches`
    * `losses-p90` (no bpp)
    * `quality-scores-*` *NOTE: I have no idea if this output still makes sense*
    * `rebuf-ratios`
    * `ssim-cdf-*`
    * `total-sizes`
    
All stats that include losses in some way do not make sense for bpp runs, since
the losses are reported wrong. There is a `get-losses.sh` script, which reports
the correct losses but is not integrated into this process.

* `measure-tput-accuracy.py`:
    * `avg-act-throughputs`
    * `avg-throughput-diffs`

* `log-parser-bpp-discards.py`:
    * `avg-discards`
    * `discard-rates`

* `get-request-sizes.py`:
    * `necessary-sizes`
    * `request-distribution`
    * `saved-percent`

### How to plot

The `prep-[trace|harpoon].sh` script automatically prepares a folder in
`/INET/inet-slipstrem/work/data`. For example, calling
```
./prep-trace.sh test-run
```
creates the folder `/INET/inet-slipstream/work/data/test-run` and copies /
creates the folders required for plotting.

Next, you need to get the `*.csv` files from `/INET/quic-storage` to
`/INET/inet-slipstream`. The easiest is to use rsync, e.g.:
```
rsync -rv --include=*/ --include=*.csv --exclude=* /INET-quic-storage/work/data/test-run/meas/ /INET/inet-slipstream/work/data/test-run/meas/
```

Now you can head over to `/INET/inet-slipstream`. First, create the required
stat files by using the Makefile in the `stats` folder, then plot the data by
using the Makefile in the `plots` folder. Your plots should land in the `figs`
folder.
