#!/usr/bin/python3
import os
import sys
from shared_functions import calculate_video_metrics, generate_buffer_lines, get_experiments, write_buffer_to_file, write_summary_to_file

EXPERIMENT_SUFFIX = 'br-switches'


def process_run(csv_file: str):
    with open(csv_file, 'r') as f:
        for line in f:
            line_split = line.split(',')
            if len(line_split) == 2 and line_split[0] == 'br_switches':
                return float(line_split[1])
    print('Error: No br_switches found in summary file: ' + csv_file, file=sys.stderr)
    return None


def process_video(video_path: str):
    run_scores = list()
    if not os.path.exists(video_path + '/csv'):
        print('Error: Failed to find csv directory in video path: ' + video_path, file=sys.stderr)
        return None
    csv_path = video_path + '/csv/'
    it = os.scandir(csv_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith('-summary.csv'):
            run_score = process_run(csv_path + entry.name)
            if run_score is not None:
                run_scores.append(run_score)
    if len(run_scores) == 0:
        print('No br_switches available for video: ' + video_path)
        return None
    metrics = calculate_video_metrics(run_scores, 1)
    return metrics


def process_buffer(buffer_path: str):
    it = os.scandir(buffer_path)
    video_metrics = dict()
    for entry in it:
        if entry.is_dir():
            metrics = process_video(buffer_path + entry.name + '/')
            if metrics is not None:
                video_metrics[entry.name] = metrics
    return video_metrics


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment> <path/to/output>')
        exit(1)
    experiment_path = sys.argv[1]
    if not experiment_path.endswith('/'):
        experiment_path += '/'
    if os.path.exists(experiment_path + 'meas/'):
        experiment_path += 'meas/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    experiments = get_experiments(experiment_path)
    for experiment_name in experiments:
        buffer_list = experiments[experiment_name]
        experiment_summary = dict()
        buffer_sizes = list()
        for buffer_size, buffer_path in buffer_list:
            video_metrics = process_buffer(buffer_path)
            buffer_lines = generate_buffer_lines(experiment_name, buffer_size, video_metrics)
            if buffer_lines is not None and len(buffer_lines) > 0:
                write_buffer_to_file(experiment_name, buffer_size, buffer_lines, output_path, EXPERIMENT_SUFFIX)
                buffer_sizes.append(buffer_size)
                experiment_summary[buffer_size] = buffer_lines
        buffer_sizes.sort()
        experiment_summary_lines = list()
        for buffer_size in buffer_sizes:
            experiment_summary_lines += experiment_summary[buffer_size]
        if len(experiment_summary_lines) > 0:
            write_summary_to_file(experiment_name, experiment_summary_lines, output_path, EXPERIMENT_SUFFIX)
    exit(0)
