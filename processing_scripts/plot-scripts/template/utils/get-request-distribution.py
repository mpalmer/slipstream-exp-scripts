#!/usr/bin/python3
import os
import sys
import numpy as np
from shared_functions import calculate_video_metrics, generate_buffer_lines, get_experiments, write_buffer_to_file, write_summary_to_file

EXPERIMENT_SUFFIX = 'request-distribution'


def process_run(csv_file: str):
    reliable = None
    unreliable = None
    optional = None
    with open(csv_file, 'r') as f:
        for line in f:
            line_split = line.split(',')
            if len(line_split) == 2:
                if line_split[0] == 'reliable(percent)':
                    reliable = float(line_split[1])
                elif line_split[0] == 'unreliable(percent)':
                    unreliable = float(line_split[1])
                elif line_split[0] == 'optional(percent)':
                    optional = float(line_split[1])
    if reliable is None or unreliable is None or optional is None:
        print('Error: Reliable, unreliable or optional percent missing in summary file: ' + csv_file, file=sys.stderr)
        return None,None,None
    return reliable, unreliable, optional


def process_video(video_path: str):
    run_scores = list()
    if not os.path.exists(video_path + '/csv'):
        print('Error: Failed to find csv directory in video path: ' + video_path, file=sys.stderr)
        return None
    csv_path = video_path + '/csv/'
    it = os.scandir(csv_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith('-request-size-stats.csv'):
            run_score = process_run(csv_path + entry.name)
            if run_score is not None:
                run_scores.append(run_score)
    if len(run_scores) == 0:
        print('No request size stats available for video: ' + video_path)
        return None
    metrics = calculate_stats(run_scores)
    return metrics


def calculate_stats(stats: list):
    if len(stats) == 0:
        print('Error: Empty stat list passed.')
        return None
    runs = len(stats)
    values = len(stats[0])
    stat_array = np.zeros((values, runs))
    for r in range(runs):
        for v in range(values):
            stat_array[v][r] = stats[r][v]
    return np.mean(stat_array, axis=1).tolist()


def process_buffer(buffer_path: str):
    it = os.scandir(buffer_path)
    video_metrics = dict()
    for entry in it:
        if entry.is_dir():
            metrics = process_video(buffer_path + entry.name + '/')
            if metrics is not None:
                video_metrics[entry.name] = metrics
    return video_metrics


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment> <path/to/output>')
        exit(1)
    experiment_path = sys.argv[1]
    if not experiment_path.endswith('/'):
        experiment_path += '/'
    if os.path.exists(experiment_path + 'meas/'):
        experiment_path += 'meas/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    experiments = get_experiments(experiment_path)
    for experiment_name in experiments:
        output_lines = list()
        buffer_list = experiments[experiment_name]
        experiment_summary = dict()
        buffer_sizes = list()
        buffer_list.sort()
        for buffer_size, buffer_path in buffer_list:
            video_metrics = process_buffer(buffer_path)
            if len(output_lines) == 0:
                header_line = 'experiment buffer'
                for video in sorted(video_metrics.keys()):
                    header_line += ' ' + video + '(reliable)'
                    header_line += ' ' + video + '(unreliable)'
                    header_line += ' ' + video + '(optional)'
                header_line += '\n'
                output_lines.append(header_line)
            data_line = experiment_name + ' ' + str(buffer_size)
            for video, metric in sorted(video_metrics.items()):
                data_line += ' ' + ' '.join(map(str,metric))
            data_line += '\n'
            output_lines.append(data_line)
        with open(output_path + experiment_name + '-' + EXPERIMENT_SUFFIX + '-stats.txt', 'w') as f:
            f.writelines(output_lines)
    exit(0)
