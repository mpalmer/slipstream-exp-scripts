import numpy as np
import sys
import os

HEADER_LINE = 'proto video netw abr bufsz p50 p90 p95 sdev avg mx mn n'


def get_experiment_name_from_folder(folder: str):
    folder_split = folder.rsplit('-', maxsplit=1)
    if len(folder_split) != 2:
        return None
    return folder_split[0], folder_split[1]


def get_experiments(experiment_path: str):
    ret = dict()
    it = os.scandir(experiment_path)
    for entry in it:
        if entry.is_dir():
            experiment = get_experiment_name_from_folder(entry.name)
            if experiment is None:
                continue
            experiment_name = experiment[0]
            buffer_size = int(experiment[1].rstrip('s'))
            if experiment_name not in ret:
                ret[experiment_name] = list()
            ret[experiment_name].append((buffer_size, experiment_path + entry.name + '/'))
    return ret


def calculate_video_metrics(run_scores: list, normalization_factor: float):
    run_score_array = np.array(run_scores)
    run_score_array = np.divide(run_score_array, normalization_factor)
    p50 = np.percentile(run_score_array, 50)
    p90 = np.percentile(run_score_array, 90)
    p95 = np.percentile(run_score_array, 95)
    sdev = np.std(run_score_array)
    avg = np.mean(run_score_array)
    mx = np.amax(run_score_array)
    mn = np.amin(run_score_array)
    n = len(run_scores)
    return p50, p90, p95, sdev, avg, mx, mn, n


def generate_buffer_lines(experiment_name: str, buffer_size: int, video_metrics: dict):
    experiment_name_split = experiment_name.split('-')
    if len(experiment_name_split) != 2:
        print('Error: Experiment name does not split into two parts: ' + experiment_name, file=sys.stderr)
        return None
    lines = list()
    netw = experiment_name_split[0]
    abr = experiment_name_split[1]
    bufsz = str(buffer_size * 1000)
    for video in video_metrics:
        video_split = video.split('-')
        if len(video_split) != 2:
            print('Error: Video name does not split into two parts: ' + video, file=sys.stderr)
            return None
        p50, p90, p95, sdev, avg, mx, mn, n = video_metrics[video]
        proto = video_split[0]
        video_name = video_split[1]
        line = [proto]
        line.append(video_name)
        line.append(netw)
        line.append(abr)
        line.append(bufsz)
        line.append(str(p50))
        line.append(str(p90))
        line.append(str(p95))
        line.append(str(sdev))
        line.append(str(avg))
        line.append(str(mx))
        line.append(str(mn))
        line.append(str(n))
        lines.append(' '.join(line) + '\n')
    lines.sort()
    return lines


def write_buffer_to_file(experiment_name: str, buffer_size: int, lines: list, outpath: str, suffix: str):
    outfile = experiment_name + '-' + str(buffer_size) + 's-' + suffix + '-stats.txt'
    if not os.path.exists(outpath):
        os.makedirs(outpath)
    with open(outpath + outfile, 'w') as f:
        f.write(HEADER_LINE + '\n')
        f.writelines(lines)


def write_summary_to_file(experiment_name: str, lines: list, outpath: str, suffix: str):
    outfile = experiment_name + '-' + suffix + '-stats.txt'
    if not os.path.exists(outpath):
        os.makedirs(outpath)
    with open(outpath + outfile, 'w') as f:
        f.write(HEADER_LINE + '\n')
        f.writelines(lines)
