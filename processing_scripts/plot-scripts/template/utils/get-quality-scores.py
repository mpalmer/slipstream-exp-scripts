#!/usr/bin/python3
import os
import sys
from shared_functions import calculate_video_metrics, generate_buffer_lines, get_experiments, write_buffer_to_file, write_summary_to_file

SCORE_NORMALIZATION_FACTOR = 750000
EXPERIMENT_SUFFIX = 'quality-scores-balanced'
BR_SWITCH_WEIGHT = 1
# MPC uses a weight of 3000, but we measure in ms.
REBUF_WEIGHT = 3


def calculate_score(lines: list):
    if len(lines) == 0:
        return 0
    score = lines[0][0] - (REBUF_WEIGHT * lines[0][1])
    prev_bitrate = lines[0][0]
    for i in range(1, len(lines)):
        curr_bitrate, curr_rebuf_time = lines[i]
        score += curr_bitrate
        if prev_bitrate != curr_bitrate:
            score -= BR_SWITCH_WEIGHT * abs(curr_bitrate - prev_bitrate)
        if curr_rebuf_time > 0:
            score -= REBUF_WEIGHT * curr_rebuf_time
        prev_bitrate = curr_bitrate
    return score


def process_run(csv_file: str):
    lines = list()
    with open(csv_file, 'r') as f:
        f.readline()
        for line in f:
            line_split = line.split(',')
            if len(line_split) < 17:
                print('Error: Failed to get bitrate/rebuffering time from line: ' + line, file=sys.stderr)
                return None
            lines.append((int(line_split[1]), int(line_split[16])))
    run_score = calculate_score(lines)
    return run_score


def process_video(video_path: str):
    run_scores = list()
    if not os.path.exists(video_path + '/csv'):
        print('Error: Failed to find csv directory in video path: ' + video_path, file=sys.stderr)
        return None
    csv_path = video_path + '/csv/'
    it = os.scandir(csv_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith('out.csv'):
            run_score = process_run(csv_path + entry.name)
            if run_score is not None:
                run_scores.append(run_score)
    if len(run_scores) == 0:
        print('No quality scores available for video: ' + video_path)
        return None
    metrics = calculate_video_metrics(run_scores, SCORE_NORMALIZATION_FACTOR)
    return metrics


def process_buffer(buffer_path: str):
    it = os.scandir(buffer_path)
    video_metrics = dict()
    for entry in it:
        if entry.is_dir():
            metrics = process_video(buffer_path + entry.name + '/')
            if metrics is not None:
                video_metrics[entry.name] = metrics
    return video_metrics


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment> <path/to/output> [<br-switch-weight> <rebuf-weight> <suffix>]')
        exit(1)
    experiment_path = sys.argv[1]
    if not experiment_path.endswith('/'):
        experiment_path += '/'
    if os.path.exists(experiment_path + 'meas/'):
        experiment_path += 'meas/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    if len(sys.argv) == 6:
        BR_SWITCH_WEIGHT = float(sys.argv[3])
        REBUF_WEIGHT = float(sys.argv[4])
        EXPERIMENT_SUFFIX = sys.argv[5]
    experiments = get_experiments(experiment_path)
    for experiment_name in experiments:
        buffer_list = experiments[experiment_name]
        experiment_summary = dict()
        buffer_sizes = list()
        for buffer_size, buffer_path in buffer_list:
            video_metrics = process_buffer(buffer_path)
            buffer_lines = generate_buffer_lines(experiment_name, buffer_size, video_metrics)
            if buffer_lines is not None and len(buffer_lines) > 0:
                write_buffer_to_file(experiment_name, buffer_size, buffer_lines, output_path, EXPERIMENT_SUFFIX)
                buffer_sizes.append(buffer_size)
                experiment_summary[buffer_size] = buffer_lines
        buffer_sizes.sort()
        experiment_summary_lines = list()
        for buffer_size in buffer_sizes:
            experiment_summary_lines += experiment_summary[buffer_size]
        if len(experiment_summary_lines) > 0:
            write_summary_to_file(experiment_name, experiment_summary_lines, output_path, EXPERIMENT_SUFFIX)
    exit(0)
