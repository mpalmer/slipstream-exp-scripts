#!/usr/bin/python3
import os
import sys
import numpy as np
import math
from shared_functions import calculate_video_metrics, generate_buffer_lines, get_experiments, write_buffer_to_file, write_summary_to_file

EXPERIMENT_SUFFIX = 'ssim-cdf-ci95'

# For 95% confidence interval
Z_VALUE = 1.96

def process_run(csv_file: str):
    ssims = list()
    with open(csv_file, 'r') as f:
        f.readline()
        for line in f:
            line_split = line.split(',')
            if len(line_split) < 19:
                print('Error: Failed to get SSIM from line: ' + line, file=sys.stderr)
                return None
            ssim = float(line_split[18])
            # First segment is always lowest quality so there is no target SSIM.
            if ssim == 0:
                continue
            ssims.append(ssim)
    ssim_array = np.array(ssims)
    return ssim_array

def calc_confinterval(ssims: list):
    ssim_array = np.vstack(ssims)
    means = np.mean(ssim_array, axis=0)
    # Standard deviation for sample
    stdevs = np.std(ssim_array, axis=0, ddof=1)
    n = np.size(ssim_array, axis=0)
    conf_intervals = Z_VALUE * stdevs / math.sqrt(n)
    plot_data_sbs = np.vstack((means, conf_intervals)).tolist()
    segment_count = len(plot_data_sbs[0])
    plot_data = list()
    for idx in range(segment_count):
        plot_data.append((plot_data_sbs[0][idx], plot_data_sbs[1][idx]))
    plot_data.sort()
    probabilities = np.arange(segment_count) / (segment_count - 1)
    plot_data_with_probs = list()
    for idx, (mean, conf) in enumerate(plot_data):
        plot_data_with_probs.append((probabilities[idx], mean, conf))
    return plot_data_with_probs


def process_video(video_path: str):
    run_scores = list()
    if not os.path.exists(video_path + '/csv'):
        print('Error: Failed to find csv directory in video path: ' + video_path, file=sys.stderr)
        return None
    csv_path = video_path + '/csv/'
    ref_len = 0
    it = os.scandir(csv_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith('out.csv'):
            run_score = process_run(csv_path + entry.name)
            if run_score is not None:
                run_scores.append(run_score)
                if ref_len != 0:
                    if len(run_score) != ref_len:
                        print('Error: Runs have different segment count in video path: ' + video_path, file=sys.stderr)
                        return None
                else:
                    ref_len = len(run_score)
    if len(run_scores) == 0:
        print('No SSIMs available for video: ' + video_path)
        return None
    return calc_confinterval(run_scores)


def process_buffer(buffer_path: str):
    it = os.scandir(buffer_path)
    video_metrics = dict()
    for entry in it:
        if entry.is_dir():
            metrics = process_video(buffer_path + entry.name + '/')
            if metrics is not None:
                video_metrics[entry.name] = metrics
    return video_metrics


def write_cdf_to_file(experiment_name: str, buffer_size: int, videos: dict, outpath: str, suffix: str):
    for video in videos:
        outfile = experiment_name + '-' + str(buffer_size) + 's-' + video + '-' + suffix + '-stats.txt'
        if not os.path.exists(outpath):
            os.makedirs(outpath)
        cdf_values = videos[video]
        columns = len(cdf_values[0])
        rows = len(cdf_values)
        with open(outpath + outfile, 'w') as f:
            f.write('prob avg ci95\n')
            for r in range(rows):
                line = [cdf_values[r][0]]
                for c in range(1, columns):
                    line.append(cdf_values[r][c])
                f.write(' '.join(map(str, line)) + '\n')


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment> <path/to/output>')
        exit(1)
    experiment_path = sys.argv[1]
    if not experiment_path.endswith('/'):
        experiment_path += '/'
    if os.path.exists(experiment_path + 'meas/'):
        experiment_path += 'meas/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    experiments = get_experiments(experiment_path)
    for experiment_name in experiments:
        buffer_list = experiments[experiment_name]
        experiment_summary = dict()
        buffer_sizes = list()
        for buffer_size, buffer_path in buffer_list:
            video_metrics = process_buffer(buffer_path)
            if len(video_metrics) > 0:
                write_cdf_to_file(experiment_name, buffer_size, video_metrics, output_path, EXPERIMENT_SUFFIX)
    exit(0)
