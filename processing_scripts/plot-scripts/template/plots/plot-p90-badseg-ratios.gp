set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 1 rounded solid

set style line 80 lc rgb "#202020" lt 1 lw 1
set border 2 back ls 80

set style line 81 lc rgb "#505050" lt 0 lw 0.7
set style line 82 lc rgb "#707070" lt 0 lw 0.5
set grid y,my back ls 81, ls 82

set xtics border in scale 1,0.5 nomirror norotate autojustify
set ytics border in scale 1,0.5 nomirror norotate autojustify

set tics in
set xtics scale 0

set style line 2 lt 1 lc rgb "#377eb8" lw 2
set style line 3 lt 1 lc rgb "#4daf4a" lw 2
set style line 4 lt 1 lc rgb "#984ea3" lw 2
set style line 5 lt 1 lc rgb "#ff7f00" lw 2

set style data histograms
set style histogram cluster gap 1 errorbars linewidth 1
set boxwidth 0.8
set style fill pattern border


set key outside center top horizontal Left reverse maxrows 1


set xlabel "Videos" offset 0,0.5

set yrange [0:]
set ytics 0,10
set ylabel "Pct. of 'bad' segments" offset 1,0


select(b)=sprintf("<awk '$5 == \"%s\" {$5/=1000; print $0}' %s", b, IN_FILE)

set output OUT_FILE
plot \
     select('8000') u 7:($9/sqrt($13)):xtic(2) t '8s' w hist ls 2 fill pat 1, \
     select('12000') u 7:($9/sqrt($13)):xtic(2) t '12s' w hist ls 3 fill pat 4, \
     select('16000') u 7:($9/sqrt($13)):xtic(2) t '16s' w hist ls 4 fill pat 2, \
     select('20000') u 7:($9/sqrt($13)):xtic(2) t '20s' w hist ls 5 fill pat 5
unset output
