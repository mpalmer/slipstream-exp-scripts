#!/usr/bin/python3
import sys
import os
import subprocess
import re

def get_experiment_name_from_file(filename: str):
    buffer_match = re.compile('\\d+s')
    filename_split = filename.split('-')
    if len(filename_split) < 3 or buffer_match.match(filename_split[2]) is not None:
        # Either a wrong filename or a stat file for a single buffer size
        return None
    return filename_split[0] + '-' + filename_split[1]


def get_stat_files(stat_path: str, stat_suffix: str):
    ret = dict()
    it = os.scandir(stat_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(stat_suffix + '-stats.txt'):
            experiment_name = get_experiment_name_from_file(entry.name)
            if experiment_name is not None:
                ret[experiment_name] = stat_path + entry.name
    return ret


def plot_stat_file(experiment_name: str, stat_file: str, output_path: str, plot_script: str, suffix: str):
    out_file = output_path + experiment_name + '-' + suffix + '.pdf'
    print('Plotting ' + stat_file + ' with ' + plot_script + ' to ' + out_file)
    subprocess.run(['gnuplot', '-e', "IN_FILE='" + stat_file + "'", '-e', "OUT_FILE='" + out_file + "'", plot_script])


if __name__ == "__main__":
    if len(sys.argv) != 6:
        print('usage: ' + sys.argv[0] + ' <path/to/stats> <path/to/output> <plot-script> <stat-suffix> <plot-suffix>')
        exit(1)
    stat_path = sys.argv[1]
    if not stat_path.endswith('/'):
        stat_path += '/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    plot_script = sys.argv[3]
    stat_suffix = sys.argv[4]
    plot_suffix = sys.argv[5]
    stat_files = get_stat_files(stat_path, stat_suffix)
    for experiment_name in stat_files:
        plot_stat_file(experiment_name, stat_files[experiment_name], output_path, plot_script, plot_suffix)
