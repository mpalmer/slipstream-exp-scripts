set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 1 rounded solid

set style line 80 lc rgb "#202020" lt 1 lw 1
set border 2 back ls 80

set style line 81 lc rgb "#505050" lt 0 lw 0.7
set style line 82 lc rgb "#707070" lt 0 lw 0.5
set grid y,my back ls 81, ls 82

set xtics border in scale 1,0.5 nomirror norotate autojustify
set ytics border in scale 1,0.5 nomirror norotate autojustify

set tics in
set xtics scale 0

set style line 10 lc rgb "#e41a1c" lw 0.1
set style line 11 lc rgb "#e41a1c"
set style line 20 lc rgb "#377eb8" lw 0.1
set style line 21 lc rgb "#377eb8"
set style line 30 lc rgb "#4daf4a" lw 0.1
set style line 31 lc rgb "#4daf4a"
set style line 40 lc rgb "#984ea3" lw 0.1
set style line 41 lc rgb "#984ea3"

set style data histograms
set style histogram cluster gap 1 errorbars linewidth 1
set bars 0.5
set boxwidth 0.8
set style fill pattern border

set key outside center top horizontal Left reverse maxrows 2 font ',13'

set xlabel "Buffer size (in seconds)" offset 0,0.5

set yrange [0:]
#set ytics 5
set ylabel "Amount discarded (percent)" offset 1,0

select(p,v)=sprintf("<awk '$1== \"%s\" && $2 == \"%s\" {$5/=1000; print $0}' %s", p, v, IN_FILE)

set output OUT_FILE
plot \
    select('s', 'bbb') u 10:($9/sqrt($13)):xtic(5) t 'BBB/Q*' w hist ls 11 fill pat 3, \
    select('s', 'ed') u 10:($9/sqrt($13)):xtic(5) t 'ED/Q*' w hist ls 21 fill pat 3, \
    select('s', 'sintel') u 10:($9/sqrt($13)):xtic(5) t 'Sintel/Q*' w hist ls 31 fill pat 3, \
    select('s', 'tos') u 10:($9/sqrt($13)):xtic(5) t 'ToS/Q*' w hist ls 41 fill pat 3
unset output
