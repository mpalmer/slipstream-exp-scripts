#!/bin/bash

INPUT_PATTERN="$1"
OUTPUT_FOLDER="${2%/}"
PLOT_SCRIPT="$3"

for FILE in $INPUT_PATTERN
do
    OUT_NAME=$(basename -s ".txt" $FILE)
    OUT_FILE="$OUTPUT_FOLDER"/"$OUT_NAME".pdf
    echo "Plotting $OUT_FILE"
    gnuplot -e "IN_FILE='$FILE'" -e "OUT_FILE='$OUT_FILE'" "$PLOT_SCRIPT"
done
