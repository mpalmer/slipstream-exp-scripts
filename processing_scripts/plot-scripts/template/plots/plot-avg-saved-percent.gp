set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 1 rounded solid

set style line 80 lc rgb "#202020" lt 1 lw 1
set border 2 back ls 80

set style line 81 lc rgb "#505050" lt 0 lw 0.7
set style line 82 lc rgb "#707070" lt 0 lw 0.5
set grid y,my back ls 81, ls 82

set xtics border in scale 1,0.5 nomirror norotate autojustify
set ytics border in scale 1,0.5 nomirror norotate autojustify

set tics in
set xtics scale 0

set style line 10 lc rgb "#e41a1c" lw 2 pt 5
set style line 20 lc rgb "#377eb8" lw 2 pt 9
set style line 30 lc rgb "#4daf4a" lw 2 pt 13
set style line 40 lc rgb "#984ea3" lw 2 pt 3

set style data linespoints

set key outside center top horizontal Left reverse maxrows 1 font ',19'
set key samplen 2

set xlabel "Buffer size (in segments)" offset 0,0.5

set yrange [0:]
set ylabel "Data saved (in percent)" offset 1,0

select(p,v)=sprintf("<awk '$1== \"%s\" && $2 == \"%s\" {$5 = ($5/4000)-1; print $0}' %s", p, v, IN_FILE)

set output OUT_FILE
plot \
    select('s', 'bbb') u 10:xtic(5) t 'BBB' ls 10, \
    select('s', 'ed') u 10:xtic(5) t 'ED' ls 20, \
    select('s', 'sintel') u 10:xtic(5) t 'Sintel' ls 30, \
    select('s', 'tos') u 10:xtic(5) t 'ToS' ls 40
unset output
