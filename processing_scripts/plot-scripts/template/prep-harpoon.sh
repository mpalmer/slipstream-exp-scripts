#!/bin/bash
if [ ! $# -eq 1 ]
then
    echo "usage: $0 <folder-name>"
    exit
fi
DIR=${1%/}
ROOT=/INET/inet-slipstream/work/data/"$DIR"
mkdir -p "$ROOT"/meas "$ROOT"/figs
cp -r plots utils stats "$ROOT"
cp figs-harpoon/* "$ROOT"/figs
