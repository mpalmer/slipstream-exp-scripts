#!/usr/bin/python3
import sys
from collections import namedtuple

SEGMENT_COUNT = 75
SEGMENT_LENGTH_IN_S = 4
VIDEO_LENGTH = SEGMENT_COUNT * SEGMENT_LENGTH_IN_S

Switch = namedtuple('Switch', 'segment_no magnitude')

def parse_line(line: str, prev_ssim: float):
    line_split = line.split(',')
    if len(line_split) < 19:
        print('Error: Failed to find SSIM in line: ' + line.strip(), file=sys.stderr)
        return None
    curr_ssim = float(line_split[18])
    print(curr_ssim)
    return curr_ssim, Switch(int(line_split[0]), curr_ssim)


def get_switches(csv_file: str):
    # First segment has no SSIM
    ret = {1:0}
    prev_ssim = 0
    with open(csv_file, 'r') as f:
        # Consume header
        f.readline()
        # Skip first segment
        f.readline()
        for line in f:
            prev_ssim, switch = parse_line(line, prev_ssim)
            if switch is None:
                return
            ret[switch.segment_no] = switch.magnitude
    return ret


def get_convergence_data(switch_data: dict):
    ret = list()
    for i in range(1, SEGMENT_COUNT + 1):
        if i not in switch_data:
            ret.append(0)
        else:
            ret.append(switch_data[i])
    return ret


def convert_to_percent_played(data: list):
    ret = list()
    switch_acc = 0
    played_acc = 4
    segment_acc = 0
    for magnitude in data[1:]:
        switch_acc += magnitude
        played_acc += 4
        segment_acc += 1
        percent_played = (100 / VIDEO_LENGTH) * played_acc
        switch_rate = switch_acc / segment_acc
        ret.append((percent_played, switch_rate))
    return ret


def write_output(output: str, data: list):
    with open(output, 'w') as f:
        f.write('percent_played avg_acc_ssim\n')
        for entry in data:
            f.write(' '.join(map(str, entry)) + '\n')


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <csv-file> <output>')
        exit(1)
    csv_file = sys.argv[1]
    output = sys.argv[2]
    switches = get_switches(csv_file)
    convergence_data = get_convergence_data(switches)
    plot_data = convert_to_percent_played(convergence_data)
    write_output(output, plot_data)

