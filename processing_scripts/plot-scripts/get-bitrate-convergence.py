#!/usr/bin/python3
import sys
from collections import namedtuple

SEGMENT_COUNT = 75
SEGMENT_LENGTH_IN_S = 4
VIDEO_LENGTH = SEGMENT_COUNT * SEGMENT_LENGTH_IN_S

Switch = namedtuple('Switch', 'segment_no magnitude')

def parse_line(line: str):
    line_split = line.split(',')
    if len(line_split) < 5:
        print('Error: Invalid bitrate switch line: ' + line.strip(), file=sys.stderr)
        return None
    return Switch(int(line_split[0]), abs(int(line_split[4])))


def get_switches(br_switches: str):
    ret = dict()
    with open(br_switches, 'r') as f:
        # Consume header
        f.readline()
        for line in f:
            switch = parse_line(line)
            if switch is None:
                continue
            ret[switch.segment_no] = switch.magnitude
    return ret


def get_convergence_data(switch_data: dict):
    ret = list()
    for i in range(1, SEGMENT_COUNT + 1):
        if i not in switch_data:
            ret.append(0)
        else:
            ret.append(switch_data[i])
    return ret


def convert_to_percent_played(data: list):
    ret = list()
    switch_acc = 0
    played_acc = 4
    for magnitude in data[1:]:
        switch_acc += (magnitude / 1000)
        played_acc += 4
        percent_played = (100 / VIDEO_LENGTH) * played_acc
        switch_rate = switch_acc / played_acc
        ret.append((percent_played, switch_rate))
    return ret


def write_output(output: str, data: list):
    with open(output, 'w') as f:
        f.write('percent_played acc_bitrate_change(Mbps)\n')
        for entry in data:
            f.write(' '.join(map(str, entry)) + '\n')


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <br-switches> <output>')
        exit(1)
    br_switches = sys.argv[1]
    output = sys.argv[2]
    switches = get_switches(br_switches)
    convergence_data = get_convergence_data(switches)
    plot_data = convert_to_percent_played(convergence_data)
    write_output(output, plot_data)

