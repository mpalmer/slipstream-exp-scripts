#!/usr/bin/python3
import sys
from collections import namedtuple

Throughputs = namedtuple('Throughputs', 'mavg rel unrel')
DownloadTimings = namedtuple('DownloadTimings', 'rel_start rel_len unrel_start unrel_len')
ReferenceResults = namedtuple('ReferenceResults', 'time_in_interval bandwidth rem_dl_len interval')

def read_bw_file(bw_switch_file: str):
    lines = list()
    with open(bw_switch_file, 'r') as f:
        for line in f:
            line_split = line.split()
            if len(line_split) != 3:
                print('Warning: Skipping line with invalid format in bw-switch file: ' + line.strip(), file=sys.stderr)
                continue
            lines.append((float(line_split[0]), float(line_split[1])))
    if len(lines) == 0:
        print('Error: No valid bw-switch lines found.', file=sys.stderr)
        return None
    start = lines[0][0]
    print('First bw-switch at: ' + str(start))
    ret = list()
    for time, rate in lines:
        ret.append((time - start, rate))
    return ret


def read_csv_file(csv_file: str):
    segment_count = 1
    tp = list()
    dl = list()
    prev_tp_rel = 0
    prev_tp_unrel = 0
    with open(csv_file, 'r') as f:
        # Consume header line
        f.readline()
        for line in f:
            line_split = line.split(',')
            if len(line_split) < 18:
                raise ValueError('Invalid line in csv file: ' + line.strip())
            tp_mavg = float(line_split[3])
            tp_rel = float(line_split[9])
            tp_unrel = float(line_split[10])
            start = int(line_split[11])
            total_rel = int(line_split[12])
            total_unrel = int(line_split[13])
            dl_rel = int(line_split[14])
            dl_unrel = int(line_split[15])
            if segment_count > 1:
                tp.append(Throughputs(tp_mavg, prev_tp_rel, prev_tp_unrel))
            dl_rel_start = (start + (total_rel - dl_rel)) / 1000
            dl_unrel_start = (start + total_rel + (total_unrel - dl_unrel)) / 1000
            dl.append(DownloadTimings(dl_rel_start, dl_rel / 1000, dl_unrel_start, dl_unrel / 1000))
            prev_tp_rel = tp_rel
            prev_tp_unrel = tp_unrel
            segment_count += 1
    # We do not get a mavg value for the 75th segment, so add at least the raw throughputs.
    tp.append(Throughputs(0, prev_tp_rel, prev_tp_unrel))
    return tp, dl


def calculate_single_reference_throughput(bandwidths: list, dl_start: float, dl_len: float, interval_hint: int = 0):
    bw_interval = interval_hint
    while bw_interval < len(bandwidths) and dl_start >= bandwidths[bw_interval][0]:
        bw_interval += 1
    if bw_interval == len(bandwidths):
        print('Warning: dl_start ' + str(dl_start) + ' is after last bw-switch.', file=sys.stderr)
        return ReferenceResults(dl_len, bandwidths[-1][1], 0, bw_interval - 1)
    bw_interval -= 1
    next_interval = bandwidths[bw_interval + 1][0]
    if dl_start + dl_len < next_interval:
        # Download is completely in current interval
        return ReferenceResults(dl_len, bandwidths[bw_interval][1], 0, bw_interval)
    time_in_interval = next_interval - dl_start
    remaining_dl_len = dl_len - time_in_interval
    if remaining_dl_len <= 0:
        remaining_dl_len = 0
        time_in_interval = dl_len
    return ReferenceResults(time_in_interval, bandwidths[bw_interval][1], remaining_dl_len, bw_interval)


def calculate_reference_throughput_for_dl(bandwidths: list, dl_start: float, dl_len: float):
    results = list()
    next_start = dl_start
    next_len = dl_len
    next_interval = 0
    while next_len > 0:
        res = calculate_single_reference_throughput(bandwidths, next_start, next_len, next_interval)
        results.append(res)
        next_start += res.time_in_interval
        next_interval = res.interval
        next_len = res.rem_dl_len
    interval_throughput = 0
    for result in results:
        weight = result.time_in_interval / dl_len
        interval_throughput += weight * result.bandwidth * 1000  # Mbps to kbps
    return interval_throughput


def calculate_reference_throughput(bandwidths: list, dl: DownloadTimings):
    tp_rel = calculate_reference_throughput_for_dl(bandwidths, dl.rel_start, dl.rel_len)
    tp_unrel = calculate_reference_throughput_for_dl(bandwidths, dl.unrel_start, dl.unrel_len)
    return Throughputs(0, tp_rel, tp_unrel)


def calculate_throughput_differences(ref_throughputs: list, measured_throughputs: list):
    ret_ref = list()
    ret_diff = list()
    if len(ref_throughputs) != len(measured_throughputs):
        print('Error:  Lengths of throughput lists do not match: ref:' + str(len(reference_throughputs)) + ' measured:'
              + str(len(measured_throughputs)), file=sys.stderr)
        return
    for idx, ref in enumerate(ref_throughputs):
        diff_rel = measured_throughputs[idx].rel - ref.rel
        diff_unrel = measured_throughputs[idx].unrel - ref.unrel
        ret_ref.append((ref.rel, ref.unrel))
        ret_diff.append((diff_rel, diff_unrel))
    return ret_ref, ret_diff


def write_throughputs_to_file(path: str, path_summary: str, ref_throughputs: list, diff_throughputs: list):
    acc_ref_rel = 0
    acc_ref_unrel = 0
    acc_diff_rel = 0
    acc_diff_unrel = 0
    with open(path, 'w') as f:
        f.write('act_tp_rel,act_tp_unrel,diff_tp_rel,diff_tp_unrel\n')
        for idx, (ref_rel, ref_unrel) in enumerate(ref_throughputs):
            diff_rel, diff_unrel = diff_throughputs[idx]
            acc_ref_rel += ref_rel
            acc_ref_unrel += ref_unrel
            acc_diff_rel += diff_rel
            acc_diff_unrel += diff_unrel
            line = [str(ref_rel)]
            line.append(str(ref_unrel))
            line.append(str(diff_rel))
            line.append(str(diff_unrel))
            f.write(','.join(line) + '\n')
    with open(path_summary, 'w') as f:
        f.write('avg_act_tp_rel,avg_act_tp_unrel,avg_diff_tp_rel,avg_diff_tp_unrel\n')
        tp_count = len(ref_throughputs)
        line = [str(acc_ref_rel / tp_count)]
        line.append(str(acc_ref_unrel / tp_count))
        line.append(str(acc_diff_rel / tp_count))
        line.append(str(acc_diff_unrel / tp_count))
        f.write(','.join(line) + '\n')


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print('usage: ' + sys.argv[0] + ' <bw-switch> <cli-csv> [<output-detail> <output-summary>]')
        print('run after log parser. use generate-tput-accuracy-data.sh for batch processing.')
        exit(1)
    bw_switch_file = sys.argv[1]
    csv_file = sys.argv[2]
    out = ''
    out_summary = ''
    if len(sys.argv) == 5:
        out = sys.argv[3]
        out_summary = sys.argv[4]
    bandwidths = read_bw_file(bw_switch_file)
    throughputs, downloads = read_csv_file(csv_file)
    reference_throughputs = list()
    print('Calculating reference throughputs...')
    for dl in downloads:
        reference_throughputs.append(calculate_reference_throughput(bandwidths, dl))
    ref, diff = calculate_throughput_differences(reference_throughputs, throughputs)
    if out != '':
        print('Writing to files: ' + out + ' ' + out_summary)
        write_throughputs_to_file(out, out_summary, ref, diff)
