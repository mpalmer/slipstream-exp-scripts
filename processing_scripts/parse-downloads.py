#!/usr/bin/python3
import os
import re
import sys

LOG_SUFFIX = '.cli.out.log'
FLOAT_FORMAT = '{:.4f}'


def get_configurations(path: str):
    ret = list()
    it = os.scandir(path)
    for entry in it:
        if entry.is_dir():
            subdir = path + entry.name + '/'
            it_subdir = os.scandir(subdir)
            for entry_subdir in it_subdir:
                if entry_subdir.is_dir():
                    print(entry_subdir.name)
                    ret.append(subdir + entry_subdir.name)
    return ret


def get_runs(path: str):
    ret = list()
    if not path.endswith('/'):
        path += '/'
    it = os.scandir(path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_SUFFIX):
            ret.append(path + entry.name)
    return ret


def parse_key_value_line(line: str):
    ret = dict()
    key_value_list = line.split()[1:]
    for pair in key_value_list:
        split_pair = pair.split(':')
        if len(split_pair) != 2:
            print('Error: Key-value pair is not actually a pair: ' + pair, file=sys.stderr)
            continue
        ret[split_pair[0]] = split_pair[1]
    return ret


def process_run(run_file: str):
    path, filename = os.path.split(run_file)
    outname = os.path.splitext(filename)[0]
    outpath = path + '/csv/'
    if not os.path.exists(outpath):
        os.mkdir(outpath)
    ret = ['dl_no segment_no try_no part_request complete_segment aborted\n']
    last_dl_finished = True
    dl_count = 0
    segment_no = None
    try_count = 0
    part_request = False
    part_request_active = False
    part_request_finished = False

    marker_match = re.compile('\\[[\\w-]+\\]')
    with open(run_file, 'r') as f:
        for line in f:
            match = marker_match.match(line)
            if match is None:
                continue
            key = match.group(0).strip('[]')
            if key == 'trying-segment':
                if dl_count > 0 and not last_dl_finished:
                    ret_line = [dl_count]
                    ret_line.append(segment_no)
                    ret_line.append(try_count)
                    if part_request:
                        ret_line.append(1)
                    else:
                        ret_line.append(0)
                    # If the optional request is still active (i.e., we never
                    # saw a [hole-fill] with loss == 0) we were not able to
                    # complete the optional download.
                    if (part_request and not part_request_finished) or not part_request:
                        ret_line.append(0)
                    else:
                        ret_line.append(1)
                    # We only reach this point if we aborted.
                    ret_line.append(1)
                    ret.append(' '.join(map(str, ret_line)) + '\n')
                last_dl_finished = False
                dl_count += 1
                values = parse_key_value_line(line)
                segment_no = values['#']
                try_count = int(values['re']) + 1
            elif key == 'bpp-request-sizes':
                values = parse_key_value_line(line)
                if int(values['sso']) > 0:
                    part_request = True
                else:
                    part_request = False
                part_request_active = False
                part_request_finished = False
            elif key == 'loading-optional':
                part_request_active = True
            elif key == 'hole-fill':
                # Hole fill in the required unreliable part.
                if not part_request_active:
                    continue
                values = parse_key_value_line(line)
                if values['loss'] == '0':
                    part_request_active = False
                    part_request_finished = True
            elif key == 'segment':
                # Init segment.
                if segment_no is None:
                    continue
                ret_line = [dl_count]
                ret_line.append(segment_no)
                ret_line.append(try_count)
                if part_request:
                    ret_line.append(1)
                else:
                    ret_line.append(0)
                # If the optional request is still active (i.e., we never
                # saw a [hole-fill] with loss == 0) we were not able to
                # complete the optional download.
                if (part_request and not part_request_finished) or not part_request:
                    ret_line.append(0)
                else:
                    ret_line.append(1)
                # We only reach this point if we did not abort.
                ret_line.append(0)
                ret.append(' '.join(map(str, ret_line)) + '\n')
                last_dl_finished = True
    with open(outpath + outname + '-downloads.csv', 'w') as f:
        f.writelines(ret)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment>')
        print('use only with bpp logs')
        sys.exit(1)
    experiment_path = sys.argv[1]
    if not experiment_path.endswith('/'):
        experiment_path += '/'
    if os.path.exists(experiment_path + 'meas/'):
        experiment_path += 'meas/'
    configurations = get_configurations(experiment_path)
    heartbeat = 0
    for config in configurations:
        runs = get_runs(config)
        for run in runs:
            process_run(run)
        heartbeat += 1
        if heartbeat % 10 == 0:
            print('Processed', heartbeat, 'configs...')

