#!/usr/bin/python3
import sys
import os
import sqlite3
from collections import namedtuple
import numpy as np

QUALITY_MAP = {'bbb'   :{161:0, 236:1, 375:2, 560:3, 749:4, 1048:5, 1745:6, 2347:7, 2995:8, 4290:9, 5785:10, 7378:11, 9978:12},
               'ed'    :{161:0, 236:1, 376:2, 561:3, 751:4, 1051:5, 1753:6, 2354:7, 3005:8, 4311:9, 5817:10, 7421:11, 9980:12},
               'sintel':{161:0, 237:1, 378:2, 561:3, 750:4, 1052:5, 1750:6, 2343:7, 2990:8, 4290:9, 5786:10, 7265:11, 9753:12},
               'tos'   :{161:0, 237:1, 376:2, 561:3, 751:4, 1049:5, 1747:6, 2350:7, 2990:8, 4284:9, 5795:10, 7392:11, 10003:12}}
CSV_SUFFIX = '.cli.out.csv'

Bracket = namedtuple('Bracket', 'p min median max')


def get_videos(buffer_path: str):
    ret = dict()
    it = os.scandir(buffer_path)
    for entry in it:
        if entry.is_dir() and entry.name.startswith('s-'):
            video_folder = entry.name
            folder_split = video_folder.split('-', maxsplit=2)
            if len(folder_split) != 2:
                print('Error: Failed to get video name from folder: ' + buffer_path + video_folder, file=sys.stderr)
                continue
            video_name = folder_split[1]
            ret[video_name] = buffer_path + video_folder + '/'
    return ret


def get_runs(video_path: str):
    ret = list()
    csv_path = video_path + 'csv/'
    if not os.path.exists(csv_path):
        print('Error: Failed to find csv folder in path: ' + video_path, file=sys.stderr)
        return ret
    it = os.scandir(csv_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(CSV_SUFFIX):
            ret.append(csv_path + entry.name)
    return ret


def get_ssims(run_file: str, video_name: str, db_cursor: sqlite3.Cursor):
    ssim_stmt = 'SELECT ssim FROM data WHERE frame_order = "original" AND video = ? AND quality = ? AND segment_no = ? and frames_dropped = 0'
    ret = list()
    with open(run_file, 'r') as f:
        # Consume header
        f.readline()
        f.readline()
        segment_count = 1
        for line in f:
            segment_count += 1
            line_split = line.split(',')
            if len(line_split) < 19:
                print('Error: Failed to get SSIM from line: ' + line.strip(), file=sys.stderr)
                return None
            bitrate = int(line_split[1])
            pause = int(line_split[17])
            ssim = float(line_split[18])
            if pause <= 70:
                ret.append(ssim)
                continue
            quality = QUALITY_MAP[video_name][bitrate]
            db_cursor.execute(ssim_stmt, (video_name,  quality, segment_count))
            query_ret = db_cursor.fetchone()
            if query_ret is None:
                print('Error: Could not find SSIM entry for video: ' + video_name + ' q: ' + str(quality) + ' segment_no: ' + str(segment_count), file=sys.stderr)
                return None
            ssim = query_ret[0]
            ret.append(ssim)
    ret.sort()
    return ret


def process_video(video_name: str, video_path: str, db_cursor: sqlite3.Cursor):
    ret = list()
    runs = get_runs(video_path)
    if len(runs) == 0:
        return None
    for run in runs:
        run_ssims = get_ssims(run, video_name, db_cursor)
        if run_ssims is None:
            continue
        ret.append(run_ssims)
    return ret


def calculate_cdf(video_ssims: list):
    ret = list()
    if len(video_ssims) == 0:
        return None
    ref_len = len(video_ssims[0])
    for v in range(len(video_ssims)):
        if len(video_ssims[v]) != ref_len:
            print('Error: Runs do not have the same segment count.', file=sys.stderr)
            return None
    p_values = np.arange(ref_len) / (ref_len - 1)
    for i in range(ref_len):
        line = list()
        for v in range(len(video_ssims)):
           line.append(video_ssims[v][i])
        line_array = np.array(line)
        ret.append(Bracket(p_values[i], np.min(line_array), np.median(line_array), np.max(line_array)))
    return ret


def write_output(output_path: str, video_name: str, cdf_data: list):
    os.makedirs(output_path, exist_ok=True)
    with open(output_path + video_name + '-ssim-cdf.dat', 'w') as f:
        f.write('prob mn p50 mx\n')
        for entry in cdf_data:
            f.write(' '.join(map(str, entry)) + '\n')


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('usage: ' + sys.argv[0] + ' <database> <path/to/buffer> <path/to/output>')
        exit(1)
    database = sys.argv[1]
    buffer_path = sys.argv[2]
    if not buffer_path.endswith('/'):
        buffer_path += '/'
    output_path = sys.argv[3]
    if not output_path.endswith('/'):
        output_path += '/'
    db_connection = sqlite3.connect(database)
    db_cursor = db_connection.cursor()
    videos = get_videos(buffer_path)
    for video in videos:
        video_ssims = process_video(video, videos[video], db_cursor)
        video_cdf = calculate_cdf(video_ssims)
        write_output(output_path, video, video_cdf)

