#!/usr/bin/python3
import sys
import os
import re

LOG_SUFFIX = '.cli.out.log'
FLOAT_FORMAT = '{:.4f}'


def get_configurations(path: str):
    ret = list()
    if not path.endswith('/'):
        path += '/'
    if os.path.exists(path + 'meas/'):
        path += 'meas/'
    it = os.scandir(path)
    for entry in it:
        if entry.is_dir():
            subdir = path + entry.name + '/'
            it_subdir = os.scandir(subdir)
            for entry_subdir in it_subdir:
                if entry_subdir.is_dir():
                    print(entry_subdir.name)
                    ret.append(subdir + entry_subdir.name)
    return ret


def get_runs(path: str):
    ret = list()
    if not path.endswith('/'):
        path += '/'
    it = os.scandir(path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_SUFFIX):
            ret.append(path + entry.name)
    return ret


def process_run(file):
    path, filename = os.path.split(file)
    outname = os.path.splitext(filename)[0]
    outpath = path + '/csv/'
    if not os.path.exists(outpath):
        os.mkdir(outpath)
    lines = dict()
    marker_match = re.compile('\\[[\\w-]+\\]')
    segment_count = -1
    curr_request_size_line = ''
    used_request_size_lines = list()
    with open(file, 'r') as f:
        for line in f:
            match = marker_match.match(line)
            if match is None:
                continue
            key = match.group(0).strip('[]')
            if key not in lines:
                lines[key] = [line]
            else:
                lines[key].append(line)
            if key == 'segment':
                segment_count += 1
                if segment_count > 0:
                    used_request_size_lines.append(curr_request_size_line)
            elif key == 'bpp-request-sizes':
                curr_request_size_line = line
    if not check_line_exists('bpp-request-sizes', lines):
        print('No bpp-request-sizes lines found. Is this a ABR* log?', file=sys.stderr)
        print('in file:' + file, file=sys.stderr)
        return
    if len(lines['bpp-request-sizes']) < segment_count:
        print('Error: Got fewer bpp-request-sizes lines than segments. (' + str(len(lines['bpp-request-sizes'])) + ' < ' + str(segment_count) + ')', file=sys.stderr)
        return
    request_sizes = parse_key_value_lines(used_request_size_lines)
    request_stats = {'total':0, 'reliable':0, 'unreliable':0, 'optional':0}
    with open(outpath + outname + '-request-sizes.csv', 'w') as f:
        f.write('segment_no,reliable(bytes),unreliable(bytes),optional(bytes),total(bytes),reliable(percent),unreliable(percent),optional(percent)\n')
        for idx in range(segment_count):
            request_info = calculate_stats(request_sizes[idx], request_stats)
            line = [str(idx + 1)]
            line.append(request_sizes[idx]['ssr'])
            line.append(request_sizes[idx]['ssu'])
            line.append(request_sizes[idx]['sso'])
            line += list(map(str, request_info))
            f.write(','.join(line) + '\n')
    with open(outpath + outname + '-request-size-stats.csv', 'w') as f:
        f.write('total(bytes),' + str(request_stats['total']) + '\n')
        f.write('reliable(bytes),' + str(request_stats['reliable']) + '\n')
        f.write('unreliable(bytes),' + str(request_stats['unreliable']) + '\n')
        f.write('optional(bytes),' + str(request_stats['optional']) + '\n')
        reliable_percent = (100 / request_stats['total']) * request_stats['reliable']
        unreliable_percent = (100 / request_stats['total']) * request_stats['unreliable']
        optional_percent = (100 / request_stats['total']) * request_stats['optional']
        f.write('reliable(percent),' + str(reliable_percent) + '\n')
        f.write('unreliable(percent),' + str(unreliable_percent) + '\n')
        f.write('optional(percent),' + str(optional_percent) + '\n')


def calculate_stats(request: dict, request_stats: dict):
    reliable = int(request['ssr'])
    unreliable = int(request['ssu'])
    optional = int(request['sso'])
    total = reliable + unreliable + optional
    percent_reliable = (100 / total) * reliable
    percent_unreliable = (100 / total) * unreliable
    percent_optional = (100 / total) * optional
    request_stats['total'] += total
    request_stats['reliable'] += reliable
    request_stats['unreliable'] += unreliable
    request_stats['optional'] += optional
    return total, percent_reliable, percent_unreliable, percent_optional


def check_single_line_exists(key: str, dictionary: dict):
    if key not in dictionary or len(dictionary[key]) != 1:
        print('Error: Either no or more than one [' + key + '] line was found.', file=sys.stderr)
        return False
    return True


def check_line_exists(key: str, dictionary: dict):
    if key not in dictionary:
        print('Error: No [' + key + '] lines found.', file=sys.stderr)
        return False
    return True


def parse_singleton_line(line: str):
    return line.split()[1]


def parse_singleton_lines(lines: list):
    ret = list()
    for line in lines:
        ret.append(parse_singleton_line(line))
    return ret


def parse_key_value_line(line: str):
    ret = dict()
    key_value_list = line.split()[1:]
    for pair in key_value_list:
        split_pair = pair.split(':')
        if len(split_pair) != 2:
            print('Error: Key-value pair is not actually a pair: ' + pair, file=sys.stderr)
            continue
        ret[split_pair[0]] = split_pair[1]
    return ret


def parse_key_value_lines(lines: list):
    ret = list()
    for line in lines:
        ret.append(parse_key_value_line(line))
    return ret


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment>')
        print('use only with bpp logs')
        sys.exit(1)
    configurations = get_configurations(sys.argv[1])
    heartbeat = 0
    for config in configurations:
        runs = get_runs(config)
        for run in runs:
            process_run(run)
        heartbeat += 1
        if heartbeat % 10 == 0:
            print('Processed ' + str(heartbeat) + ' configs...')
