#!/usr/bin/python3
import sys
import os
import re

FRAMES_PER_SEGMENT = 96
TOTAL_SEGMENTS = 75

def create_perfect_log(log_number: int, log_path: str):
    out_file = log_path + str(log_number) + '_ssim.log'
    with open(out_file, 'w') as f:
        for i in range(1,FRAMES_PER_SEGMENT + 1):
            f.write(str(i) + ' 1.0 1.0 1.0 1.0\n')
    return out_file

def get_segment_log_list(path: str):
    log_it = os.scandir(path)
    log_files = list()
    # Retreive existing logs (with errors)
    for entry in log_it:
        if entry.is_file() and entry.name.endswith('.log'):
            log_number = int(entry.name.split('_', maxsplit=2)[0])
            if log_number == 0:
                # Skip log of init segment
                continue
            log_files.append((log_number, path + entry.name))
    log_files.sort()
    perfect_files = list()
    log_number_exp = 1
    # Fill gaps for perfect segments.
    for log_number, file in log_files:
        while log_number > log_number_exp:
            perfect_file = create_perfect_log(log_number_exp, path)
            perfect_files.append((log_number_exp, perfect_file))
            log_number_exp += 1
        log_number_exp += 1
    # If the tail segments are perfect, fill them up as well.
    for i in range(log_number_exp, TOTAL_SEGMENTS + 1):
        perfect_file = create_perfect_log(i, path)
        perfect_files.append((i, perfect_file))
    log_files += perfect_files
    log_files.sort()
    if len(log_files) != TOTAL_SEGMENTS:
        print('Error: Did not produce the correct amount of ssim log files. Expected ' + str(TOTAL_SEGMENTS) + ' got ' + str(len(log_files)), file=sys.stderr)
    return log_files


def parse_line(line: str):
    decimal_match = re.compile('\\d\\.\\d+')
    line_match = decimal_match.findall(line)
    if len(line_match) < 4:
        print('Error: Failed to match line: ' + line, file=sys.stderr)
        return None
    return line_match[0:4]


def calculate_averages(value_list: list):
    y_acc = 0
    u_acc = 0
    v_acc = 0
    all_acc = 0
    value_no = len(value_list)
    for entry in value_list:
        y_acc += float(entry[0])
        u_acc += float(entry[1])
        v_acc += float(entry[2])
        all_acc += float(entry[3])
    y_avg = str(y_acc / value_no)
    u_avg = str(u_acc / value_no)
    v_avg = str(v_acc / value_no)
    all_avg = str(all_acc / value_no)
    return y_avg, u_avg, v_avg, all_avg


def parse_segment_log(path: str):
    segment_ssim_values = list()
    with open(path, 'r') as f:
        for line in f:
            parsed_line = parse_line(line)
            if parsed_line is not None:
                segment_ssim_values.append(parsed_line)
    if len(segment_ssim_values) == 0:
        return None
    averages = calculate_averages(segment_ssim_values)
    return segment_ssim_values, averages


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <path/to/ssim> <total-score-file>')
        exit(1)
    path = sys.argv[1]
    total_score_file = sys.argv[2]
    if not path.endswith('/'):
        path += '/'
    run_name = path.split('/')
    run_name = run_name[len(run_name) - 2]
    logfile_list = get_segment_log_list(path)
    total_values = list()
    segment_averages = list()
    for logfile in logfile_list:
        segment = parse_segment_log(logfile[1])
        if segment is None:
            continue
        total_values += segment[0]
        segment_averages.append(segment[1])
    total_averages = calculate_averages(total_values)
    with open(path + 'total_score.csv', 'w') as f:
        f.write('run_name,y,u,v,all\n')
        f.write(run_name + ',' + ','.join(total_averages) + '\n')
    if not os.path.exists(total_score_file):
        with open(total_score_file, 'w') as f:
            f.write('run_name,y,u,v,all\n')
    with open(total_score_file, 'a') as f:
        f.write(run_name + ',' + ','.join(total_averages) + '\n')
    with open(path + 'frame_scores.csv', 'w') as f:
        f.write('frame_no,y,u,v,all\n')
        for i in range(0, len(total_values)):
            f.write(str(i+1) + ',' + ','.join(total_values[i]) + '\n')
    with open(path + 'segment_scores.csv', 'w') as f:
        f.write('segment_no,y,u,v,all\n')
        for i in range(0, len(segment_averages)):
            f.write(str(i+1) + ',' + ','.join(segment_averages[i]) + '\n')
