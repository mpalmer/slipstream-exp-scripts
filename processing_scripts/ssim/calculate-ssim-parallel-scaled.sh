#!/bin/bash
if [ ! $# -eq 3 ]
then
	echo "usage: $0 <path/to/corrupt> <path/to/ref> <path/to/logs>"
	echo "Iterates over the corrupt files and tries to find the corresponding reference file in the given document tree. Creates individual logs in the specified path and two summary files 'total.log' containing per-frame information and 'total_score.log' containing the overall SSIM score."
	exit
fi

TEST_DIR=${1%/}
REF_DIR=${2%/}
LOG_DIR=${3%/}
FFMPEG_BIN=ffmpeg
if $FFMPEG_BIN -version | grep -q 4.1.3
then
    echo "Found valid FFmpeg executable"
else
    echo "Trying Rechenknecht FFmpeg executable /pool/data/mappel/FFmpeg-n4.1.3/bin/ffmpeg"
    FFMPEG_BIN=/pool/data/mappel/FFmpeg-n4.1.3/bin/ffmpeg
    if $FFMPEG_BIN -version | grep -q 4.1.3
    then
        echo "Found valid FFmpeg executable"
    else
        echo "Could not find valid FFmpeg executable."
        exit
    fi
fi
if [ ! -d "$TEST_DIR" ] || [ ! -d "$REF_DIR" ]
then
	echo "Corrupt or reference file directory does not exist."
	exit
fi

mkdir -p "$LOG_DIR"
mkdir -p "$LOG_DIR"/ffmpeg
echo "Looking for video files..."
VIDEO_FILES=()
FILENAMES=()
REF_FILES=()
REF_SUFFIX=""
SCALE_BOTH=0
for FILE in "$TEST_DIR"/*
do
    FILE_NAME=$(basename "$FILE")
    if [[ "$FILE_NAME" == *bbb*96key* ]]
    then
        REF_SUFFIX="bbb_2160p_10k_24fps_96key_300s_dash"
    elif [[ "$FILE_NAME" == *ed*96key* ]]
    then
        REF_SUFFIX="ed_1080p_10k_24fps_96key_300s_dash"
        SCALE_BOTH=1
    elif [[ "$FILE_NAME" == *sintel*96key* ]]
    then
        REF_SUFFIX="sintel_2160p_10k_24fps_96key_300s_dash"
    elif [[ "$FILE_NAME" == *tos*96key* ]]
    then
        REF_SUFFIX="tos_2160p_10k_24fps_96key_300s_dash"
    fi
    break
done
for FILE in "${TEST_DIR}"/*
do
  VIDEO_FILES+=("$FILE")
	# Strip the path
	FILENAME=$(basename -- "$FILE")
    SEGMENT_NO=${FILENAME%%_*}
	# Strip the trailing 'init_with_http_header' thingy
	FILENAME="$(echo "$FILENAME" | grep -o -e '.*dash')"
	FILENAMES+=("$FILENAME")
	# Look for the corresponding reference file
    REF=$(find "$REF_DIR" -name "${SEGMENT_NO}_${REF_SUFFIX}_glued.mp4")
	REF_FILES+=("$REF")
done
echo "Running FFmpeg..."
if [ "$SCALE_BOTH" -eq 0 ]
then
    nice -n 1 parallel --bar "($FFMPEG_BIN -hide_banner -i {1} -i {2} -lavfi \"[1]scale=3840x2160[c];[0][c]ssim=${LOG_DIR}/{3}.log\" -f null - 2>> ${LOG_DIR}/ffmpeg/{3}_ffmpeg.log)" ::: ${REF_FILES[*]} :::+ ${VIDEO_FILES[*]} :::+ ${FILENAMES[*]}
else
    nice -n 1 parallel --bar "($FFMPEG_BIN -hide_banner -i {1} -i {2} -lavfi \"[0]scale=3840x2160[r];[1]scale=3840x2160[c];[r][c]ssim=${LOG_DIR}/{3}.log\" -f null - 2>> ${LOG_DIR}/ffmpeg/{3}_ffmpeg.log)" ::: ${REF_FILES[*]} :::+ ${VIDEO_FILES[*]} :::+ ${FILENAMES[*]}
fi
