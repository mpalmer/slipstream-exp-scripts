#!/bin/bash
#set -eou pipefail
# Set this to 0 if you want to keep the generated/glued video files.
CLEANUP=1
LOG_FILE_ENDING="*.cli.out.log"
SSIM_ROOT=/dev/shm/ssim

if [ ! $# -eq 1 ]
then
  echo "usage: $0 <path/to/result>"
  exit
fi
RESULT_DIR=${1%/}
REF_DIR="$SSIM_ROOT"/ref
if [ ! -d "$RESULT_DIR" ] || [ ! -d "$SSIM_ROOT" ] || [ ! -d "$REF_DIR" ]
then
  echo "Result, ssim or reference directory does not exist."
  exit
fi
if [ -d "$RESULT_DIR"/meas ]
then
	RESULT_DIR="$RESULT_DIR"/meas
fi
PROC_DIR="$SSIM_ROOT"/proc
mkdir -p "$PROC_DIR"
echo "Cleaning up proc dir..."
rm -r "$PROC_DIR"/* 2> /dev/null
echo "Copying log files to proc dir..."
PRE_COPY=$(date +%s.%N)
rsync -r --prune-empty-dirs --include=*/ --include="$LOG_FILE_ENDING" --exclude=* "$RESULT_DIR"/* "$PROC_DIR"
POST_COPY=$(date +%s.%N)
COPY_DUR=$(echo "$POST_COPY-$PRE_COPY" | bc)
printf "Copy took %.4f s\n" $COPY_DUR
for EXPERIMENT in "$PROC_DIR"/*/s-*
do
	echo "Experiment: $EXPERIMENT"
	for RUN in "$EXPERIMENT"/$LOG_FILE_ENDING
	do
		echo "Run: $RUN"
		RUNNO=$(basename "$RUN" .cli.out.log)
		VIDEO_FILE_DIR="$EXPERIMENT"/"$RUNNO"
		LOG_DIR="$EXPERIMENT"/"$RUNNO"-ssim-logs
		echo "Generating video files..."
		PRE_GEN=$(date +%s.%N)
		./reconstruct-video-complete.py "$RUN" "$VIDEO_FILE_DIR" "$REF_DIR"
		POST_GEN=$(date +%s.%N)
		echo "Glueing..."
		./glue-init.sh "$VIDEO_FILE_DIR"
		POST_GLUE=$(date +%s.%N)
		echo "Calculating SSIM..."
		./calculate-ssim-parallel-scaled.sh "$VIDEO_FILE_DIR"/glued/ "$REF_DIR" "$LOG_DIR"
		POST_SSIM=$(date +%s.%N)
		echo "Processing SSIM scores..."
		./concat-ssims.py "$LOG_DIR" "$EXPERIMENT"/total_scores.csv
		POST_CONCAT=$(date +%s.%N)
		if [ $CLEANUP -eq 1 ]
		then
		  echo "Cleaning up..."
		  rm -r "$VIDEO_FILE_DIR"
		fi
		GEN_DUR=$(echo "$POST_GEN-$PRE_GEN" | bc)
		GLUE_DUR=$(echo "$POST_GLUE-$POST_GEN" | bc)
		SSIM_DUR=$(echo "$POST_SSIM-$POST_GLUE" | bc)
		CONCAT_DUR=$(echo "$POST_CONCAT-$POST_SSIM" | bc)
		printf "stats: gen: %.4f s glue: %.4f s ssim: %.4f s concat: %.4f s\n" $GEN_DUR $GLUE_DUR $SSIM_DUR $CONCAT_DUR
	done
done
echo "Copying results back to experiment root..."
rsync -r --prune-empty-dirs --include=*/ --include=*.csv --include=*.log --exclude=* "$PROC_DIR"/* "$RESULT_DIR"
echo "Cleaning up proc dir..."
rm -r "$PROC_DIR"/* 2> /dev/null
echo "Finished."
