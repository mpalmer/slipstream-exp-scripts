#!/usr/bin/python3
import sys
import os
import subprocess
import tempfile

# Set this to true if you want to generate a single video file in addition to
# the segment files.
GENERATE_SINGLE_FILE = False


def get_lines_from_file(path: str):
    segment_lines = list()
    loss_lines = list()
    skip_lines = list()
    segment_count = 0
    with open(path, 'r') as f:
        for line in f:
            if line.startswith('[loss]'):
                loss_lines.append((segment_count, line.strip()))
            elif line.startswith('[skipping-optional]'):
                skip_lines.append((segment_count, line.strip()))
            elif line.startswith('[segment]'):
                segment_lines.append(line.strip())
                segment_count += 1
    return segment_lines, loss_lines, skip_lines


def parse_key_value_line(line: str):
    ret = dict()
    key_value_list = line.split()[1:]
    for pair in key_value_list:
        split_pair = pair.split(':')
        if len(split_pair) != 2:
            print('Error: Key-value pair is not actually a pair: ' + pair, file=sys.stderr)
            continue
        ret[split_pair[0]] = split_pair[1]
    return ret


def get_offset_from_segment(segment: dict):
    if '@' not in segment:
        raise ValueError('Could not find range in line: ' + str(segment))
    media_range = segment['@']
    media_range_split = media_range.split('-')
    if len(media_range_split) != 2:
        raise ValueError('Invalid media range format: ' + media_range)
    return int(media_range_split[0])


def parse_key_value_lines(lines: list):
    ret = list()
    for line in lines:
        ret.append(parse_key_value_line(line))
    return ret


def parse_skip_line(line: str, offset: int):
    ret = list()
    ranges = line.split()[1]
    ranges_split = ranges.split(',')
    for pair in ranges_split:
        split_pair = pair.split('-')
        if len(split_pair) != 2:
            print('Error: Invalid skip entry format: ' + pair, file=sys.stderr)
            continue
        range_start = int(split_pair[0])
        range_end = int(split_pair[1])
        size = range_end - range_start + 1
        ret.append((str(range_start - offset), str(size)))
    return ret


def parse_skip_lines(lines: list, segments: list):
    ret = list()
    for segment_no, skip_line in lines:
        segment_offset = get_offset_from_segment(segments[segment_no])
        ret.append((segment_no, parse_skip_line(skip_line, segment_offset)))
    return ret


def parse_loss_line(line: str, offset: int):
    ret = list()
    line_split = line.split()[1:]
    for pair in line_split:
        split_pair = pair.split(',')
        if len(split_pair) != 2:
            print('Error: Invalid loss entry format: ' + pair, file=sys.stderr)
            continue
        ret.append((str(int(split_pair[0]) - offset), split_pair[1]))
    return ret


def parse_loss_lines(lines: list, segments: list):
    ret = list()
    for segment_no, loss_line in lines:
        segment_offset = get_offset_from_segment(segments[segment_no])
        ret.append((segment_no, parse_loss_line(loss_line, segment_offset)))
    return ret


def get_reference_file_name(segment: dict):
    if '#' not in segment:
        raise ValueError('Could not find segment number in line: ' + str(segment))
    no = segment['#']
    if 'n' not in segment:
        raise ValueError('Could not find file name in line: ' + str(segment))
    file = segment['n']
    dash_index = file.find('_dash')
    if dash_index == -1:
        raise ValueError('Could not find \'_dash\' substring in name: ' + file)
    file = file[: dash_index + 5]
    ref_segment_name = no + '_' + file + '.mp4'
    return int(no), ref_segment_name


def find_reference_file(ref_path: str, file_name: str):
    ref_file = subprocess.check_output(['find', ref_path, '-name', file_name]).strip().decode('utf-8')
    if ref_file == '':
        raise ValueError('Could not find reference file: ' + file_name)
    if len(ref_file.split()) > 1:
        raise ValueError('Multiple file locations found for reference file: ' + file_name)
    return str(ref_file)


def copy_reference_files(reference_files: list, out_path: str):
    if not os.path.exists(out_path):
        os.mkdir(out_path)
    print('Copying reference files...')
    subprocess.run('parallel cp {} ' + out_path + ' ::: ' + ' '.join(reference_files), check=True, shell=True)


def generate_corrupt_files(loss_queue: list):
    range_files = list()
    inputs = list()
    outputs = list()
    try:
        print('Generating range files...')
        for reference_file, losses, outfile in loss_queue:
            inputs.append(reference_file)
            outputs.append(outfile)
            rangefile, rangefile_path = tempfile.mkstemp()
            range_files.append(rangefile_path)
            with os.fdopen(rangefile, 'w') as tmp:
                for offset, size in losses:
                    start = int(offset)
                    end = start + int(size) - 1
                    tmp.write(str(start) + ' ' + str(end) + '\n')
        print('Applying losses...')
        subprocess.run('parallel "./bitkiller {1} {2} {3} -q" ::: '
                + ' '.join(range_files) + ' :::+ ' + ' '.join(inputs) + ' :::+ ' + ' '.join(outputs), check=True, shell=True)
    finally:
        print('Deleting range files...')
        subprocess.run('parallel rm ::: ' + ' '.join(range_files), check=True, shell=True)


def generate_video(segments: list, losses: list, out_path: str, ref_path: str):
    reference_files = dict()
    out_files = dict()
    for segment in segments:
        segment_no, reference_file_name = get_reference_file_name(segment)
        reference_files[segment_no] = find_reference_file(ref_path, reference_file_name)
        out_files[segment_no] = reference_file_name
    print('out_path: ' + out_path)
    if not os.path.exists(out_path):
        print('created.')
        os.mkdir(out_path)
    subprocess.run('cp ' + reference_files[0] + ' ' + out_path + out_files[0], check=True, shell=True)
    apply_loss_queue = list()
    for segment_no, loss in losses:
        file = out_path + out_files[segment_no]
        apply_loss_queue.append((reference_files[segment_no], loss, file))
    generate_corrupt_files(apply_loss_queue)
    if GENERATE_SINGLE_FILE:
        out_files_prefixed = list()
        for out_file in out_files:
            out_files_prefixed.append(out_path + out_file)
        print('Concatenating segments to single file')
        subprocess.run('cat ' + ' '.join(out_files_prefixed) + ' >> ' + out_path + 'out.mp4', check=True, shell=True)


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print('usage: ' + sys.argv[0] + ' <logfile> <path/to/video/output> <path/to/ref>')
        exit(1)
    logfile = sys.argv[1]
    out_path = sys.argv[2]
    if not out_path.endswith('/'):
        out_path += '/'
    ref_path = sys.argv[3]
    if not ref_path.endswith('/'):
        ref_path += '/'
    segment_lines, loss_lines, skip_lines = get_lines_from_file(logfile)
    segments = parse_key_value_lines(segment_lines)
    losses = parse_loss_lines(loss_lines, segments)
    losses += parse_skip_lines(skip_lines, segments)
    generate_video(segments, losses, out_path, ref_path)
