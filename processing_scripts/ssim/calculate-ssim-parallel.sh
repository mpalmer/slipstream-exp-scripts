#!/bin/bash
if [ ! $# -eq 3 ]
then
	echo "usage: $0 <path/to/corrupt> <path/to/ref> <path/to/logs>"
	echo "Iterates over the corrupt files and tries to find the corresponding reference file in the given document tree. Creates individual logs in the specified path and two summary files 'total.log' containing per-frame information and 'total_score.log' containing the overall SSIM score."
	exit
fi

TEST_DIR=${1%/}
REF_DIR=${2%/}
LOG_DIR=${3%/}
FFMPEG_BIN=ffmpeg
if $FFMPEG_BIN -version | grep -q 4.1.3
then
    echo "Found valid FFmpeg executable"
else
    echo "Trying Rechenknecht FFmpeg executable /pool/data/mappel/FFmpeg-n4.1.3/bin/ffmpeg"
    FFMPEG_BIN=/pool/data/mappel/FFmpeg-n4.1.3/bin/ffmpeg
    if $FFMPEG_BIN -version | grep -q 4.1.3
    then
        echo "Found valid FFmpeg executable"
    else
        echo "Could not find valid FFmpeg executable."
        exit
    fi
fi
if [ ! -d "$TEST_DIR" ] || [ ! -d "$REF_DIR" ]
then
	echo "Corrupt or reference file directory does not exist."
	exit
fi

mkdir -p "$LOG_DIR"
mkdir -p "$LOG_DIR"/ffmpeg
echo "Looking for video files..."
VIDEO_FILES=()
FILENAMES=()
REF_FILES=()
for FILE in "${TEST_DIR}"/*
do
  VIDEO_FILES+=("$FILE")
	# Strip the path
	FILENAME=$(basename -- "$FILE")
	# Strip the run number: Removes everything up to (including) the first underscore.
	#FILENAME=${FILENAME#*_}
	# Strip the trailing 'init_with_http_header' thingy
	FILENAME="$(echo "$FILENAME" | grep -o -e '.*dash')"
	FILENAMES+=("$FILENAME")
	# Look for the corresponding reference file
	REF=$(find "$REF_DIR" -name "$FILENAME*_glued.mp4")
	REF_FILES+=("$REF")
done
echo "Running FFmpeg..."
nice -n 1 parallel --bar "($FFMPEG_BIN -hide_banner -i {1} -i {2} -lavfi \"ssim=${LOG_DIR}/{3}.log\" -f null - 2>> ${LOG_DIR}/ffmpeg/{3}_ffmpeg.log)" ::: ${REF_FILES[*]} :::+ ${VIDEO_FILES[*]} :::+ ${FILENAMES[*]}
