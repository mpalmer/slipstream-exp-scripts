#!/bin/bash

if [ -z "$1" ]
then
	echo "usage: $0 <path/to/chunks>"
	echo "Expects the initialization segment to match either the '*_0_*.mp4' or '0_*.mp4' pattern. Glued files are created in a subfolder called 'glued'."
	exit
fi

DIR=$1
if [ ! -d "$DIR" ]
then
	echo "Specified directory does not exist. Aborting."
	exit
fi

cd $DIR
INIT_FILE=$(find -name "*_0_*.mp4")
if [ -z "$INIT_FILE" ]
then
	INIT_FILE=$(find -name "0_*.mp4")
fi
if [ -z "$INIT_FILE" ]
then
	echo "Could not find initialization segment. Aborting."
	exit
fi

echo "Found initialization segment $INIT_FILE"
mkdir -p glued
COUNT=0
for FILE in *.mp4
do
	NAME="${FILE:0:-4}_glued.mp4"
	cat "$INIT_FILE" "$FILE" > "glued/$NAME"
	COUNT=$((COUNT+1))
done
echo "Finished creating $COUNT files."
