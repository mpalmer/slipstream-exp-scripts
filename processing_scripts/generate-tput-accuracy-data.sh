#!/bin/bash
if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/experiment>"
    exit
fi
BW_SWITCH_SUFFIX=".bw-switch.log"
CSV_SUFFIX=".cli.out.csv"
DETAIL_OUT_SUFFIX=".cli.out-throughputs.csv"
SUMMARY_OUT_SUFFIX=".cli.out-tp_summary.csv"

EXPERIMENT_DIR=${1%/}
if [ -d "$EXPERIMENT_DIR"/meas ]
then
    EXPERIMENT_DIR="$EXPERIMENT_DIR"/meas
fi
for VIDEO in "$EXPERIMENT_DIR"/*/*/
do
    echo $VIDEO
    for BW_SWITCH in "$VIDEO"*"$BW_SWITCH_SUFFIX"
    do
        CSV_FOLDER="$VIDEO"csv
        RUN=$(basename "$BW_SWITCH" "$BW_SWITCH_SUFFIX")
        CSV="$RUN""$CSV_SUFFIX"
        OUT_DETAIL="$RUN""$DETAIL_OUT_SUFFIX"
        OUT_SUMMARY="$RUN""$SUMMARY_OUT_SUFFIX"
        ./measure-tput-accuracy.py "$BW_SWITCH" "$CSV_FOLDER"/"$CSV" "$CSV_FOLDER"/"$OUT_DETAIL" "$CSV_FOLDER"/"$OUT_SUMMARY"
    done
done
