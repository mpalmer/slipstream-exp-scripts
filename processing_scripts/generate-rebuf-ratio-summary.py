#!/usr/bin/python3
import sys
import os

video_list = list()
protocol_list = list()
buffer_list = list()


def get_rebuf_ratio_from_file(file: str):
    with open(file, 'r') as f:
        for line in f:
            if line.startswith('rebuf_ratio'):
                return line.strip().split(',')[1]
    print('Error: rebuf_ratio not found in file ' + file, file=sys.stderr)
    return str()


def get_rebuf_ratio_column(path: str, buffer_size: int):
    column = [',' + str(buffer_size) + 's']
    path += 'csv/'
    it = os.scandir(path)
    for entry in it:
        if entry.is_file() and entry.name.endswith('-summary.csv'):
            run_number = entry.name.split('.', maxsplit=1)[0]
            rebuf_ratio = get_rebuf_ratio_from_file(path + entry.name)
            column.append(run_number + ',' + rebuf_ratio)
    return column


def write_video_to_file(path: str, experiment_name: str, video: str, buffers: dict):
    file_name = experiment_name + '-' + video + '.csv'
    print('Writing file ' + file_name)
    columns = list()
    for protocol in protocol_list:
        for buffer_size in buffer_list:
            columns.append(get_rebuf_ratio_column(buffers[buffer_size][video][protocol], buffer_size))
    row_num = len(columns[0])
    reliable_column = ['']*row_num
    slipstream_column = ['']*row_num
    reliable_column[0] = 'reliable'
    slipstream_column[0] = 'slipstream'
    columns.insert(len(buffer_list), slipstream_column)
    columns.insert(0, reliable_column)
    for column in columns:
        if len(column) != row_num:
            print('Error: column has different size than first column. First: ' + str(row_num) + ' This: '
                  + str(len(column)), file=sys.stderr)
            print(column, file=sys.stderr)
            return
    with open(path + '/csv/' + file_name, 'w') as f:
        for i in range(0, row_num):
            line = str()
            for column in columns:
                line += column[i] + ','
            f.write(line[:-1] + '\n')


def get_experiment_name_from_folder(folder: str):
    global buffer_list
    if folder == 'csv':
        return None
    folder_split = folder.rsplit('-', maxsplit=1)
    if len(folder_split) != 2:
        print('Error: Failed to get experiment name from folder: ' + folder, file=sys.stderr)
        return None
    experiment_name = folder_split[0]
    buffer_size = int(folder_split[1].rstrip('s'))
    if buffer_size not in buffer_list:
        buffer_list.append(buffer_size)
    return experiment_name, buffer_size


def get_experiments(path: str):
    ret = dict(list())
    it = os.scandir(path)
    for entry in it:
        if entry.is_dir():
            experiment = get_experiment_name_from_folder(entry.name)
            if experiment is None:
                continue
            experiment_name = experiment[0]
            buffer_size = experiment[1]
            print('Found experiment: ' + experiment_name + ' buffer: ' + str(buffer_size) + 's')
            if experiment_name not in ret:
                ret[experiment_name] = [(buffer_size, path + entry.name + '/')]
            else:
                ret[experiment_name].append((buffer_size, path + entry.name + '/'))
    return ret


def parse_video_name(video: str):
    global video_list
    global protocol_list
    name_split = video.split('-')
    if len(name_split) != 2:
        print('Error: Failed to parse video name: ' + video, file=sys.stderr)
        return None
    video_name = name_split[1]
    protocol = name_split[0]
    if video_name not in video_list:
        video_list.append(video_name)
    if protocol not in protocol_list:
        protocol_list.append(protocol)
    return video_name, protocol


def get_videos(buffer: str):
    ret = dict(dict())
    it = os.scandir(buffer)
    for entry in it:
        if entry.is_dir():
            video_protocol = parse_video_name(entry.name)
            if video_protocol is None:
                continue
            if video_protocol[0] not in ret:
                ret[video_protocol[0]] = dict()
            ret[video_protocol[0]][video_protocol[1]] = buffer + entry.name + '/'
    return ret


def process_experiment(path: str, experiment_name: str, buffer_dict: dict):
    global protocol_list
    global buffer_list
    buffers = dict()
    for buffer_size, buffer_folder in buffer_dict:
        buffers[buffer_size] = get_videos(buffer_folder)
    protocol_list.sort()
    buffer_list.sort()
    for video in video_list:
        write_video_to_file(path, experiment_name, video, buffers)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment>')
        exit(1)
    path = sys.argv[1]
    if not os.path.exists(path):
        print('Error: Folder not found: ' + path, file=sys.stderr)
        exit(1)
    if not path.endswith('/'):
        path += '/'
    if os.path.exists(path + 'meas/'):
        path += 'meas/'
    if not os.path.exists(path + 'csv'):
        os.mkdir(path + 'csv')
    experiments = get_experiments(path)
    for experiment_name in experiments:
        process_experiment(path, experiment_name, experiments[experiment_name])
