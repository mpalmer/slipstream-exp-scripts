#!/bin/bash
set -euo pipefail

if [ ! $# -eq 1 ]
then
    echo "usage: $0 <path/to/experiment>"
    exit 1
fi

EXPERIMENT_PATH="${1%/}"
[ -d "$EXPERIMENT_PATH"/meas ] && EXPERIMENT_PATH="$EXPERIMENT_PATH"/meas
for RUN in "$EXPERIMENT_PATH"/*/*/*.cli.out.log
do
    ./get-losses.py "$RUN"
done
