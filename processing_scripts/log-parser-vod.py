#!/usr/bin/python3
import sys
import os
import re

LOG_SUFFIX = '.cli.out.log'
FLOAT_FORMAT = '{:.4f}'


def get_configurations(path: str):
    ret = list()
    if not path.endswith('/'):
        path += '/'
    if os.path.exists(path + 'meas/'):
        path += 'meas/'
    it = os.scandir(path)
    for entry in it:
        if entry.is_dir():
            subdir = path + entry.name + '/'
            it_subdir = os.scandir(subdir)
            for entry_subdir in it_subdir:
                if entry_subdir.is_dir():
                    print(entry_subdir.name)
                    ret.append(subdir + entry_subdir.name)
    return ret


def get_runs(path: str):
    ret = list()
    if not path.endswith('/'):
        path += '/'
    it = os.scandir(path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_SUFFIX):
            ret.append(path + entry.name)
    return ret


def process_run(file):
    path, filename = os.path.split(file)
    outname = os.path.splitext(filename)[0]
    outpath = path + '/csv/'
    if not os.path.exists(outpath):
        os.mkdir(outpath)
    lines = dict()
    marker_match = re.compile('\\[[\\w-]+\\]')
    segment_count = -1
    rebuffer_locations = list()
    pause_locations = list()
    ping_locations = list()
    idle_locations = list()
    with open(file, 'r') as f:
        for line in f:
            match = marker_match.match(line)
            if match is None:
                continue
            key = match.group(0).strip('[]')
            if key not in lines:
                lines[key] = [line]
            else:
                lines[key].append(line)
            if key == 'segment':
                segment_count += 1
            elif key == 'pause':
                pause_locations.append(segment_count)
            elif key == 'rebuffer':
                rebuffer_locations.append(segment_count)
            elif key == 'ping':
                ping_locations.append(segment_count)
            elif key == 'idle':
                idle_locations.append(segment_count)
    if not check_single_line_exists('bitrates', lines):
        print('in file: ' + file, file=sys.stderr)
        return
    bitrates = parse_bitrates(lines['bitrates'][0])
    if not check_single_line_exists('segments', lines):
        return
    segment_summary = parse_key_value_line(lines['segments'][0])
    if not check_single_line_exists('startup', lines):
        return
    startup = parse_singleton_line(lines['startup'][0])
    if not check_line_exists('buffer', lines):
        return
    buffers = parse_singleton_lines(lines['buffer'])
    if not check_line_exists('segment', lines):
        return
    segments = parse_key_value_lines(lines['segment'])
    if not check_line_exists('throughput', lines):
        return
    throughputs = parse_key_value_lines(lines['throughput'])
    if not check_line_exists('time', lines):
        return
    times = parse_key_value_lines(lines['time'])
    # Sanity checks
    if len(segments) != int(segment_summary['num']) + 1 or len(segments) != len(throughputs) or len(segments) != len(buffers) + 1 or len(segments) != len(times):
        print('Error: Line numbers do not add up. (segments == specified segments + 1 == throughputs == buffers + 1 == times)', file=sys.stderr)
        print('          segments: ' + str(len(segments)), file=sys.stderr)
        print('         specified: ' + segment_summary['num'], file=sys.stderr)
        print('           buffers: ' + str(len(buffers)), file=sys.stderr)
        print('       throughputs: ' + str(len(throughputs)), file=sys.stderr)
        print('             times: ' + str(len(times)), file=sys.stderr)
        print('in file: ' + file, file=sys.stderr)
        return

    # Optional lines
    if 'pause' in lines:
        pauses = parse_singleton_lines(lines['pause'])
    if 'rebuffer' in lines:
        rebuffers = parse_singleton_lines(lines['rebuffer'])

    loss_percentages = calculate_loss_percentages(segments)
    with open(outpath + outname + '.csv', 'w') as f:
        f.write('segment_no,bitrate(kbits),buffer(ms),throughput_mavg(kbits),loss(percent),loss(bytes),'
                'size_reliable(bytes),size_unreliable(bytes),size_total(bytes),throughput_reliable(kbits),'
                'throughput_unreliable(kbits),time_segment_request(ms),duration_reliable_request(ms),'
                'duration_unreliable_request(ms),download_duration_reliable(ms),download_duration_unreliable(ms),rebuffering_time(ms),pause(ms),target_ssim\n')
        for segment_no in range(1, len(segments)):
            line = [str(segment_no)]
            line.append(segments[segment_no]['br'])
            line.append(buffers[segment_no - 1])
            line.append(throughputs[segment_no]['mavg'])
            line.append(FLOAT_FORMAT.format(loss_percentages[segment_no]))
            line.append(segments[segment_no]['loss'])
            line.append(segments[segment_no]['ssr'])
            line.append(segments[segment_no]['ssu'])
            line.append(segments[segment_no]['ss'])
            line.append(throughputs[segment_no]['r'])
            line.append(throughputs[segment_no]['u'])
            line.append(times[segment_no]['s'])
            line.append(times[segment_no]['r'])
            line.append(times[segment_no]['u'])
            line.append(times[segment_no]['dlr'])
            line.append(times[segment_no]['dlu'])
            rebuffer_time = '0'
            if len(rebuffer_locations) > 0:
                try:
                    rebuffer_time = rebuffers[rebuffer_locations.index(segment_no)]
                except ValueError:
                    rebuffer_time = '0'
            line.append(rebuffer_time)
            pause = '0'
            if len(pause_locations) > 0:
                try:
                    pause = pauses[pause_locations.index(segment_no)]
                except ValueError:
                    pause = '0'
            line.append(pause)
            ssim = '0'
            if 'ssim' in segments[segment_no]:
                ssim = segments[segment_no]['ssim']
            line.append(ssim)
            f.write(','.join(line) + '\n')
    rebuf_total = 0
    with open(outpath + outname + '-rebuf.csv', 'w') as f:
        f.write('segment_no,bitrate,rebuf_time\n')
        # Write startup time as rebuffering instance to be consistent with old format.
        f.write('1,' + segments[1]['br'] + ',' + startup + '\n')
        if len(rebuffer_locations) > 0:
            for i in range(0, len(rebuffers)):
                segment_no = rebuffer_locations[i]
                line = [str(segment_no)]
                line.append(segments[segment_no]['br'])
                line.append(rebuffers[i])
                f.write(','.join(line) + '\n')
                rebuf_total += int(rebuffers[i])
    br_switches = calculate_quality_switches(segments, bitrates)
    with open(outpath + outname + '-br_switches.csv', 'w') as f:
        f.write('segment_no(to),bitrate_from(kbits),bitrate_to(kbits),level_diff,bitrate_diff(kbit)\n')
        for br_switch in br_switches:
            f.write(','.join(br_switch) + '\n')
    video_length = int(segment_summary['num']) * int(segment_summary['len'])
    with open(outpath + outname + '-summary.csv', 'w') as f:
        f.write('total_rebuffering_time,' + str(rebuf_total) + '\n')
        rebuf_ratio = (rebuf_total * 100) / video_length
        ratebuf = len(rebuffer_locations) * 100 / int(segment_summary['num'])
        f.write('rebuf_ratio,' + FLOAT_FORMAT.format(rebuf_ratio) + '\n')
        f.write('rebuffering_events,' + str(len(rebuffer_locations)) + '\n')
        f.write('rate_buf,' + FLOAT_FORMAT.format(ratebuf) + '\n')
        f.write('br_switches,' + str(len(br_switches)) + '\n')
    if len(ping_locations) > 0 or len(idle_locations) > 0:
        with open(outpath + outname + '-idle.csv', 'w') as f:
            for segment_no in ping_locations:
                f.write('ping ' + str(segment_no) + '\n')
            for segment_no in idle_locations:
                f.write('idle ' + str(segment_no) + '\n')


def calculate_quality_switches(segments: list, bitrates: list):
    ret = list()
    for segment_no in range(2, len(segments)):
        bitrate_from = segments[segment_no - 1]['br']
        bitrate_to = segments[segment_no]['br']
        if bitrate_from != bitrate_to:
            level_from = bitrates.index(bitrate_from) + 1
            level_to = bitrates.index(bitrate_to) + 1
            ret.append((str(segment_no), bitrate_from, bitrate_to, str(level_to - level_from), str(int(bitrate_to) - int(bitrate_from))))
    return ret


def calculate_loss_percentages(segments: list):
    # TODO Percentage is with regard to the unreliable part. Do we want to change this?
    ret = list()
    for segment in segments:
        if segment['ssu'] == '0' or segment['loss'] == '0':
            ret.append(0)
            continue
        loss_percentage = (100 / int(segment['ssu'])) * int(segment['loss'])
        ret.append(loss_percentage)
    return ret


def check_single_line_exists(key: str, dictionary: dict):
    if key not in dictionary or len(dictionary[key]) != 1:
        print('Error: Either no or more than one [' + key + '] line was found.', file=sys.stderr)
        return False
    return True


def check_line_exists(key: str, dictionary: dict):
    if key not in dictionary:
        print('Error: No [' + key + '] lines found.', file=sys.stderr)
        return False
    return True


def parse_bitrates(line: str):
    # Remove key from line
    ret = line.split()[1:]
    return ret


def parse_singleton_line(line: str):
    return line.split()[1]


def parse_singleton_lines(lines: list):
    ret = list()
    for line in lines:
        ret.append(parse_singleton_line(line))
    return ret


def parse_key_value_line(line: str):
    ret = dict()
    key_value_list = line.split()[1:]
    for pair in key_value_list:
        split_pair = pair.split(':')
        if len(split_pair) != 2:
            print('Error: Key-value pair is not actually a pair: ' + pair, file=sys.stderr)
            continue
        ret[split_pair[0]] = split_pair[1]
    return ret


def parse_key_value_lines(lines: list):
    ret = list()
    for line in lines:
        ret.append(parse_key_value_line(line))
    return ret


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment>')
        sys.exit(1)
    configurations = get_configurations(sys.argv[1])
    heartbeat = 0
    for config in configurations:
        runs = get_runs(config)
        for run in runs:
            process_run(run)
        heartbeat += 1
        if heartbeat % 10 == 0:
            print('Processed ' + str(heartbeat) + ' configs...')
