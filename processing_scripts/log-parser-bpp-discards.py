#!/usr/bin/python3
import sys
import os
import re

LOG_SUFFIX = '.cli.out.log'


def get_configurations(path: str):
    ret = list()
    if not path.endswith('/'):
        path += '/'
    if os.path.exists(path + 'meas'):
        path += 'meas/'
    it = os.scandir(path)
    for entry in it:
        if entry.is_dir():
            subdir = path + entry.name + '/'
            it_subdir = os.scandir(subdir)
            for entry_subdir in it_subdir:
                if entry_subdir.is_dir():
                    print(entry_subdir.name)
                    ret.append(subdir + entry_subdir.name)
    return ret


def get_runs(path: str):
    ret = list()
    if not path.endswith('/'):
        path += '/'
    it = os.scandir(path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(LOG_SUFFIX):
            ret.append(path + entry.name)
    return ret


def process_run(file):
    path, filename = os.path.split(file)
    outname = os.path.splitext(filename)[0]
    outpath = path + '/csv/'
    if not os.path.exists(outpath):
        os.mkdir(outpath)
    segment_lines = list()
    request_size_lines = list()
    hole_fill_lines = list()
    marker_match = re.compile('\\[[\\w-]+\\]')
    segment_count = -1
    current_request = None
    current_required_hole_line = None
    current_optional_hole_line = None
    is_optional = False
    with open(file, 'r') as f:
        for line in f:
            match = marker_match.match(line)
            if match is None:
                continue
            key = match.group(0).strip('[]')
            if key == 'bpp-request-sizes':
                current_request = line
                is_optional = False
                current_required_hole_line = None
                current_optional_hole_line = None
            elif key == 'loading-optional' or key == 'skipping-optional':
                is_optional = True
            elif key == 'hole-fill':
                if is_optional:
                    current_optional_hole_line = line
                else:
                    current_required_hole_line = line
            elif key == 'segment':
                segment_count += 1
                if segment_count == 0:
                    continue
                segment_lines.append(line)
                request_size_lines.append(current_request)
                hole_fill_lines.append((current_required_hole_line, current_optional_hole_line))
    if len(segment_lines) == 0:
        print('Error: No [segment] lines found.', file=sys.stderr)
        return
    segments = parse_key_value_lines(segment_lines)
    request_sizes = parse_key_value_lines(request_size_lines)
    savings = list()
    for required, optional in hole_fill_lines:
        saved_required = 0
        saved_optional = None
        if required is not None:
            required_line = parse_key_value_line(required)
            saved_required = int(required_line['loss'])
        if optional is not None:
            optional_line = parse_key_value_line(optional)
            saved_optional = int(optional_line['loss'])
        savings.append((saved_required, saved_optional))
    with open(outpath + outname + '-discards.csv', 'w') as f:
        f.write('segment_no,size_total,size_reliable(bytes),size_unreliable_required(bytes),size_unreliable_optional(bytes),size_discarded(bytes),size_discarded(percent)\n')
        for idx, sizes in enumerate(request_sizes):
            requests = request_sizes[idx]
            total_size = int(requests['ssr']) + int(requests['ssu']) + int(requests['sso'])
            total_savings, savings_optional = savings[idx]
            if savings_optional is None:
                # We skipped the optional part entirely
                total_savings += int(requests['sso'])
            else:
                total_savings += savings_optional
            savings_percent = (100 / total_size) * total_savings
            line = [str(idx + 1)]
            line.append(str(total_size))
            line.append(requests['ssr'])
            line.append(requests['ssu'])
            line.append(requests['sso'])
            line.append(str(total_savings))
            line.append(str(savings_percent))
            f.write(','.join(line) + '\n')


def parse_singleton_line(line: str):
    return line.split()[1]


def parse_singleton_lines(lines: list):
    ret = list()
    for line in lines:
        ret.append(parse_singleton_line(line))
    return ret


def parse_key_value_line(line: str):
    ret = dict()
    key_value_list = line.split()[1:]
    for pair in key_value_list:
        split_pair = pair.split(':')
        if len(split_pair) != 2:
            print('Error: Key-value pair is not actually a pair: ' + pair, file=sys.stderr)
            continue
        ret[split_pair[0]] = split_pair[1]
    return ret


def parse_key_value_lines(lines: list):
    ret = list()
    for line in lines:
        ret.append(parse_key_value_line(line))
    return ret


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <path/to/experiment>')
        sys.exit(1)
    configurations = get_configurations(sys.argv[1])
    heartbeat = 0
    for config in configurations:
        runs = get_runs(config)
        for run in runs:
            process_run(run)
        heartbeat += 1
        if heartbeat % 10 == 0:
            print('Processed ' + str(heartbeat) + ' configs...')
