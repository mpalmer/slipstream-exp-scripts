#!/usr/bin/python3
import sys
import os
import sqlite3
from collections import namedtuple
import numpy as np

QUALITY_MAP = {'bbb'   :{161:0, 236:1, 375:2, 560:3, 749:4, 1048:5, 1745:6, 2347:7, 2995:8, 4290:9, 5785:10, 7378:11, 9978:12},
               'ed'    :{161:0, 236:1, 376:2, 561:3, 751:4, 1051:5, 1753:6, 2354:7, 3005:8, 4311:9, 5817:10, 7421:11, 9980:12},
               'sintel':{161:0, 237:1, 378:2, 561:3, 750:4, 1052:5, 1750:6, 2343:7, 2990:8, 4290:9, 5786:10, 7265:11, 9753:12},
               'tos'   :{161:0, 237:1, 376:2, 561:3, 751:4, 1049:5, 1747:6, 2350:7, 2990:8, 4284:9, 5795:10, 7392:11, 10003:12}}
CSV_SUFFIX = '.cli.out.csv'

Bracket = namedtuple('Bracket', 'p min median max')

def get_experiment_name_from_folder(folder: str):
    folder_split = folder.rsplit('-', maxsplit=1)
    if len(folder_split) != 2:
        return None
    return folder_split[0], folder_split[1]

def get_experiments(experiment_path: str):
    ret = dict()
    it = os.scandir(experiment_path)
    for entry in it:
        if entry.is_dir():
            experiment = get_experiment_name_from_folder(entry.name)
            if experiment is None:
                continue
            experiment_name = experiment[0]
            buffer_size = int(experiment[1].rstrip('s'))
            if experiment_name not in ret:
                ret[experiment_name] = list()
            ret[experiment_name].append((buffer_size, experiment_path + entry.name + '/'))
    return ret

def get_videos(buffer_path: str):
    ret = dict()
    it = os.scandir(buffer_path)
    for entry in it:
        if entry.is_dir() and entry.name.startswith('r-'):
            video_folder = entry.name
            folder_split = video_folder.split('-', maxsplit=2)
            if len(folder_split) != 2:
                print('Error: Failed to get video name from folder: ' + buffer_path + video_folder, file=sys.stderr)
                continue
            video_name = folder_split[1]
            ret[video_name] = buffer_path + video_folder + '/'
    return ret


def get_runs(video_path: str):
    ret = list()
    csv_path = video_path + 'csv/'
    if not os.path.exists(csv_path):
        print('Error: Failed to find csv folder in path: ' + video_path, file=sys.stderr)
        return ret
    it = os.scandir(csv_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(CSV_SUFFIX):
            ret.append(csv_path + entry.name)
    return ret


def get_ssims(run_file: str, video_name: str, db_cursor: sqlite3.Cursor):
    ssim_stmt = 'SELECT ssim FROM data WHERE frame_order = "original" AND video = ? AND quality = ? AND segment_no = ? and frames_dropped = 0'
    ret = list()
    with open(run_file, 'r') as f:
        # Consume header
        f.readline()
        segment_count = 1
        for line in f:
            line_split = line.split(',')
            if len(line_split) < 2:
                print('Error: Failed to get bitrate from line: ' + line.strip(), file=sys.stderr)
                return None
            bitrate = int(line_split[1])
            quality = QUALITY_MAP[video_name][bitrate]
            db_cursor.execute(ssim_stmt, (video_name,  quality, segment_count))
            query_ret = db_cursor.fetchone()
            if query_ret is None:
                print('Error: Could not find SSIM entry for video: ' + video_name + ' q: ' + str(quality) + ' segment_no: ' + str(segment_count), file=sys.stderr)
                return None
            ssim = query_ret[0]
            ret.append(ssim)
            segment_count += 1
    return ret


def process_video(video_name: str, video_path: str, db_cursor: sqlite3.Cursor):
    ret = list()
    runs = get_runs(video_path)
    if len(runs) == 0:
        return None
    for run in runs:
        run_ssims = get_ssims(run, video_name, db_cursor)
        if run_ssims is None:
            continue
        ret.append(run_ssims)
    return ret


def write_output(output_path: str, prefix: str, cdf_data: list):
    os.makedirs(output_path, exist_ok=True)
    columns = len(cdf_data[0])
    rows = len(cdf_data)
    with open(output_path + prefix + '-ssim-stats.txt', 'w') as f:
        for r in range(rows):
            row_line = list()
            for c in range(columns):
                row_line.append(cdf_data[r][c])
            f.write(' '.join(map(str, row_line)) + '\n')


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('usage: ' + sys.argv[0] + ' <database> <path/to/experiment> <path/to/output>')
        exit(1)
    database = sys.argv[1]
    experiment_path = sys.argv[2]
    if not experiment_path.endswith('/'):
        experiment_path += '/'
    if os.path.exists(experiment_path + 'meas/'):
        experiment_path += 'meas/'
    output_path = sys.argv[3]
    if not output_path.endswith('/'):
        output_path += '/'
    db_connection = sqlite3.connect(database)
    db_cursor = db_connection.cursor()
    experiments = get_experiments(experiment_path)
    for experiment_name in experiments:
        buffer_list = experiments[experiment_name]
        experiment_summary = dict()
        buffer_sizes = list()
        for buffer_size, buffer_path in buffer_list:
            videos = get_videos(buffer_path)
            for video in videos:
                prefix = experiment_name + '-' + str(buffer_size) + 's-r-' + video
                video_ssims = process_video(video, videos[video], db_cursor)
                if video_ssims is None:
                    continue
                write_output(output_path, prefix, video_ssims)

