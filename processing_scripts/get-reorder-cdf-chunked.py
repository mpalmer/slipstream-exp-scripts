#!/usr/bin/python3
from collections import namedtuple
import numpy as np
import os
import sys

DAT_SUFFIX = '.dat'
LineTuple = namedtuple('LineTuple', 'weight pos')

def get_input_files(input_path: str):
    ret = list()
    it = os.scandir(input_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(DAT_SUFFIX):
            ret.append(input_path + entry.name)
    return ret


def get_chunk_offsets(lines: list):
    ret = list()
    line_tuples = list()
    for idx, line in enumerate(lines):
        line_split = line.split()
        if len(line_split) != 2:
            print('Error: Invalid line:', line, file=sys.stderr)
            continue
        weight = int(line_split[1])
        line_tuples.append(LineTuple(weight, idx))
    # Sort by descending weight. For equal weights use original order.
    sorted_tuples = sorted(line_tuples, key=lambda x: (-x[0], x[1]))
    for idx, entry in enumerate(sorted_tuples):
        ret.append(entry.pos - idx)
    return ret


def get_segment_offsets(input_file: str, chunk_frames: int):
    ret = list()
    lines = list()
    with open(input_file, 'r') as f:
        lines = f.readlines()
    chunk_cnt = 0
    # WARNING: If the number of frames in a segment is not divisible by
    # chunk_frames, the last (smaller) chunk is ignored.
    while chunk_cnt * chunk_frames < len(lines):
        chunk_start_idx = chunk_cnt * chunk_frames
        chunk_end_idx = chunk_start_idx + chunk_frames - 1
        # Range is exclusive.
        chunk_lines = lines[chunk_start_idx:chunk_end_idx + 1]
        ret += get_chunk_offsets(chunk_lines)
        chunk_cnt += 1
    return ret


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('usage:', sys.argv[0], '<path/to/input> <output-file> <frames in chunk>')
        exit(1)
    input_path = sys.argv[1]
    if not input_path.endswith('/'):
        input_path += '/'
    output_file = sys.argv[2]
    chunk_frames = int(sys.argv[3])
    input_files = get_input_files(input_path)
    offsets = list()
    for input_file in input_files:
        offsets += get_segment_offsets(input_file, chunk_frames)
    offsets.sort()
    p_values = np.arange(len(offsets)) / (len(offsets) - 1)
    with open(output_file, 'w') as f:
        f.write('p offset\n')
        for idx, value in enumerate(offsets):
            f.write(str(p_values[idx]) + ' ' + str(value) + '\n')

