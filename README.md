# Slipstream experiment scripts

This repository contains the scripts that are used to run the experiments for
the Slipstream project.

## Preparation

To run the experiments, a few things need to be gathered that are not committed
to this repository. Maybe we will add nice scripts for them at sometime.

1. Add folders `bin` containing the following files (names are important):
```
libbase_i18n.so
libbase.so
libboringssl.so
libcrcrypto.so
libc++.so
libicui18n.so
libicuuc.so
libnet.so
libprotobuf_lite.so
liburl.so
quic_client
quic_server
```
Take a look at
[the Slipstream repo](https://gitlab.mpi-klsb.mpg.de/mpalmer/quic-chrome)
([github.com mirror)](https://github.com/balakrishnanc/slipstream/) on how to
generate these files.

2. Get the actual video files to populate the `extra-data/cache-*` folders. One
place to get them at the moment would be
`rechenknecht:/pool/data/mappel/slipstream-exp-scripts`. They should hopefully
be available somewhere different in the future.

3. Set the access rights for the SSH key
```
chmod 600 ssh/slipstream
```

4. Set the `BINARY_DIR` variable in `Defines.py`. This is the directory on the
lab machines that will be used to store the binaries, videos, etc.

## Running experiments

To run experiments, you need two scripts: `generate-runs.py` and `ExperimentRunner.py`. The generate-runs script generates a run queue file from a given config file.
```
./generate-runs.py example-trace.cfg run_queue
```

would generate a queue file called `run_queue` from the config `example-trace.cfg`.
Necessary parameters for **all** experiment are:
* `timestamp`: Specifies the output folder name. If left blank, the current timestamp is used. An existing folder name can be used to add additional results to an existing experiment.
* `traffic-simulation`: Must be *one* of `trace,harpoon,parallel`
* `runs`: An integer greater 0. Specifies how many iterations per video are queued.
* `buffers`: A comma-separated list of buffer sizes (in ms).
* `videos`: A comma-separated list of videos. The format is `<protocol>-<video>`.
    * Valid protocols: `reliable, slipstream`
    * Valid videos: `bbb,ed,sintel,tos,bbb_1s,ed_1s,sintel_1s,tos_1s`
    * Example: `slipstream-bbb`
* `fine`: Specifies if the ABRs should get a rough (`false`) or fine (`true`) throughput estimate. For slipstream runs set this to `true`, else to `false`.
* `abrs`: A comma-separated list of ABRs. Valid values are `bola,mpc,tput,bpp`.
* `qlen`: The network queue size that is set on the bridge. Default for 10Mbps throughput 30ms one-way delay should be `32`
* `delay`: The one-way network delay (in ms).
* `default-bandwidth`: The initial bandwidth (in Mbps) that is set on the shaper. This value makes most sense for Harpoon runs, since the trace runs overwrite it immediately.

Optional parameters:
* `feature`: A hash-separated (`#`) list of key-value pairs separated by `:`. For example: `key1:value1#key2:value2`. Currently only used to activate enhanced BOLA features with `enhanced_bola:1`.

Additional parameters for **trace** experiments:
* `traces`: A comma-separated list of network traces. Valid values are `att,tmobile,verizon,constant,fcc,3g`. Note that `3g` requires a different queue size.
* `shift`: Can be `true` or `false`. If `runs` is greater than 1 and `shift` is true, the script will choose a different start time in each trace for each run.

Additional parameters for **harpoon** experiments:
* `harpoon-bandwidths`: A comma-separated list of cross-traffic bandwidths (in Mbps).

The generate-runs script will unroll this config file into the different permutations.

When the runs are finished, they are automatically backed up to
```
/INET/quic-storage/work/data/<timestamp>
```
You can see the folder structure of some existing results there.

After the queue file was generated, it can be executed with:
```
./ExperimentRunner active-configs run_queue
```
The script looks in the `active-configs` folder for signatures it will use to execute the runs on. To reduce the number of active signatures, move them to a folder, e.g., `inactive-configs`.

The script should then automatically execute all the runs and also restart them if they should fail for some reason.
Note that the script deletes the run_queue file after consumption, so you might want to keep a backup in case you want to rerun the same stuff.

## Log parsing

When the experiment is finished, you can use the `log_parser_new.py` script (found in the `processing_scripts` folder), to generate the csv files.
```
./log_parser_new.py /INET/quic-storage/work/data/<timestamp>/meas/
```

creates a `csv` subfolder in each video folder (e.g., `meas/tmobile-bola-8s/s-ed/csv/`).

## Adding additional traces

1. Put your trace file in the `extra-data/traces/` folder.
2. Edit the `Configs.py` file and add the filename and an alias to the `__translate_trace` function. If the script should apply an offset (in Mbit) to the trace, you can also specify it here (if not you still need to set it to 0).
3. Edit the `generate-runs.py` file and add the trace alias and the length of the trace in line `13` to the `trace_lengths` dict. This is needed to automatically generate the shift value for different runs.

Now you can use your trace alias in the config files and generate run queues with them. 
