import threading
from queue import Queue
from queue import Empty
from paramiko import SSHClient
from paramiko import AutoAddPolicy
from paramiko import BadHostKeyException
from paramiko import AuthenticationException
from paramiko import SSHException
import SSHHelper as ssh
import socket
import sys
import time


class ServerThread(threading.Thread):
    def __init__(self, host: str, port: str, video: str, pipe: Queue, log=sys.stderr):
        threading.Thread.__init__(self)
        self.kill_wait = 10
        self.host = host
        self.port = port
        video_split = video.split('-')
        self.video = video_split[len(video_split) - 1]
        self.pipe = pipe
        self.connection = SSHClient()
        self.connection.set_missing_host_key_policy(AutoAddPolicy)
        self.connection.load_system_host_keys()
        self.binary_dir = None
        self.working_dir = None
        self.storage_dir = None
        self.log = log
        self.pid = -1

    def __del__(self):
        self.connection.close()

    def connect(self):
        try:
            self.connection.connect(hostname=self.host, port=222, username='root', key_filename='ssh/slipstream')
        except (BadHostKeyException, AuthenticationException, SSHException, socket.error) as e:
            print('Failed to connect to server: ' + str(e), file=self.log)
            return False
        return True

    def set_binary_dir(self, path: str):
        self.binary_dir = path.rstrip('/')

    def set_working_dir(self, path: str):
        self.working_dir = path.rstrip('/')

    def set_storage_dir(self, path: str):
        self.storage_dir = path.rstrip('/')

    def stop(self):
        if self.pid == -1:
            print('Error: QUIC server pid was never set. Can not stop!', file=self.log)
            return False
        if not self.is_alive():
            print('Error: QUIC server was already stopped. Trying to kill PID just in case.', file=self.log)
            ssh.execute(self.connection, 'kill -9 ' + str(self.pid), self.log)
            return False
        print('Stopping QUIC server', file=self.log)
        ssh.execute(self.connection, 'kill ' + str(self.pid), self.log)
        tries = 0
        while self.is_alive():
            time.sleep(1)
            tries += 1
            if tries == self.kill_wait:
                print('CriticalError: QUIC server does not respond to kill command. Trying to force.', file=self.log)
                ssh.execute(self.connection, 'kill -9 ' + str(self.pid), self.log)
                return False
        return True

    def teardown(self, target_name: str):
        if self.working_dir is None or self.storage_dir is None:
            print('Working dir or storage dir not set!', file=self.log)
            return False
        mv_server_err = 'mv --backup=numbered ' + self.working_dir + '/srv.err.log ' + self.storage_dir + '/' + target_name + '.srv.err.log'
        mv_server_out = 'mv --backup=numbered ' + self.working_dir + '/srv.out.log ' + self.storage_dir + '/' + target_name + '.srv.out.log'
        no_error = True
        print(mv_server_err, file=self.log)
        if ssh.execute(self.connection, mv_server_err, self.log) != 0:
            print('Error: Failed to move srv.err.log.', file=self.log)
            no_error = False
        print(mv_server_out, file=self.log)
        if ssh.execute(self.connection, mv_server_out, self.log) != 0:
            print('Error: Failed to move srv.out.log.', file=self.log)
            no_error = False
        return no_error

    def run(self):
        if self.binary_dir is None or self.working_dir is None:
            print('Binary dir or working dir not set!', file=self.log)
            self.pipe.put(-1)
            return
        print('Starting QUIC server', file=self.log)
        command = 'ulimit -c unlimited; cd ' + self.working_dir + '; ' + self.binary_dir + '/bin/quic_server --certificate_file='\
                  + self.binary_dir + '/leaf_cert.pem --key_file=' + self.binary_dir + '/leaf_cert.pkcs8 --port='\
                  + self.port + ' --quic_response_cache_dir=' + self.binary_dir + '/cache-' + self.video
        print(command, file=self.log)
        try:
            stdin, stdout, stderr = self.connection.exec_command(command)
        except SSHException as e:
            print('Failed to start server: ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        try:
            self.pid = int(stdout.readline())
        except ValueError as e:
            print('Got something from server that was not the PID: ' + str(e), file=self.log)
            self.pipe.put(-1)
            return
        print('QUIC server started with pid: ' + str(self.pid), file=self.log)
        self.pipe.put(self.pid)
        version = stdout.readline()
        print('QUIC server version: ' + version.strip(), file=self.log)
        self.pipe.put(version.strip())
        ready = stdout.readline()
        self.pipe.put(ready.strip())
        exit_code = stdout.channel.recv_exit_status()
        print('QUIC server stopped with exit code: ' + str(exit_code), file=self.log)
        self.pipe.put(exit_code)
